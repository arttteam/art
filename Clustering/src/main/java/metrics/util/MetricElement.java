/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package metrics.util;

import util.Element;

/**
 * @author mihai
 */
public class MetricElement {

    private Element elem;
    private int tag;

    public MetricElement(Element elem) {
        this.elem = elem;
    }

    public MetricElement(dataFormat.Element elem) {
        this.elem = new Element(elem.getClassName(), elem.index);
    }

    public MetricElement(Element elem, int tag) {
        this.elem = elem;
        this.tag = tag;
    }

    public Element getElement() {
        return this.elem;
    }

    public int getElementIndex() {
        return elem.index;
    }

    public String getClassName() {
        return this.elem.getClassName();
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public int getTag() {
        return this.tag;
    }

}
