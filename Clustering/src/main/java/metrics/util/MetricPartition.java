package metrics.util;

import util.Partition;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MetricPartition {

    private List<MetricCluster> clusters;
    public String algorithmPerformed = "";

    public MetricPartition(Partition p) {
        clusters = p
                .getClusters()
                .stream()
                .map(MetricCluster::new)
                .collect(Collectors.toList());
    }

    public MetricPartition(dataFormat.Partition p) {
        clusters = p
                .getClusters()
                .stream()
                .map(MetricCluster::new)
                .collect(Collectors.toList());
    }

    public MetricPartition(List<MetricCluster> clust) {
        this.clusters = clust
                .stream()
                .map(c -> new MetricCluster(c.getElements(), c.getTag()))
                .collect(Collectors.toList());
    }

    public void addCluster(MetricCluster cluster) {
        clusters.add(cluster);
    }

    public List<MetricCluster> getClusters() {
        return this.clusters;
    }

    public int getClusterNumber() {
        return this.clusters.size();
    }

    public int findElementTag(MetricElement e) {
        for (MetricCluster c : clusters)
            for (MetricElement elem : c.getElements())
                if (elem.getClassName().equals(e.getClassName()))
                    return c.getTag();
        return 0;
    }

    public void joinClusters(MetricCluster x, MetricCluster y) {
        MetricCluster lx = null, ly = null;
        for (MetricCluster c : this.clusters)
            if (c.getTag() == x.getTag())
                lx = c;
            else if (c.getTag() == y.getTag())
                ly = c;
        for (MetricElement e : y.getElements()) {
            lx.appendElement(new MetricElement(e.getElement(), e.getTag()));
        }
        this.clusters.remove(ly);
    }

    public List<Set<Integer>> toListSet() {
        return clusters
                .stream()
                .map(MetricCluster::toSet)
                .collect(Collectors.toList());
    }

    public String toString() {
        String temp = "";
        for (MetricCluster c : clusters)
            temp += c.toString();
        return temp;
    }

}
