package metrics.util;

import util.Cluster;

import java.util.*;
import java.util.stream.Collectors;

public class MetricCluster {

    private List<MetricElement> cluster;
    private int tag;
    private MetricCluster centerCluster;
    public MetricCluster rel = null;
    public boolean removed = false;

    public MetricCluster(int tag) {
        this.tag = tag;
        this.cluster = new ArrayList<>();
    }

    public MetricCluster(Cluster c) {
        this.tag = c.getTag();
        this.cluster = c.getElements()
                        .stream()
                        .map(MetricElement::new)
                        .collect(Collectors.toList());
    }

    public MetricCluster(dataFormat.Cluster c) {
        this.tag = c.getTag();
        this.cluster = c.getElements()
                        .stream()
                        .map(MetricElement::new)
                        .collect(Collectors.toList());
    }

    public MetricCluster(List<MetricElement> cluster, int tag) {
        this.tag = tag;
        this.cluster = cluster
                        .stream()
                        .map(e -> new MetricElement(e.getElement(), e.getTag()))
                        .collect(Collectors.toList());
    }

    public void appendElement(MetricElement e) {
        this.cluster.add(e);
    }

    public void removeElement(MetricElement e) {
        this.cluster.remove(e);
    }

    public List<MetricElement> getElements() {
        return this.cluster;
    }

    public void setCenterCluster(MetricCluster a) {
        this.centerCluster = a;
    }

    public int getNumberOfElementsWithTag(int tag) {
        return cluster
                .stream()
                .map(MetricElement::getTag)
                .filter(metricTag -> metricTag == tag)
                .reduce(0, Integer::sum);
    }

    public boolean containsElement(MetricElement elem) {
        final String elementClassName = elem.getClassName();

        return cluster
                .stream()
                .map(MetricElement::getClassName)
                .anyMatch(className -> className.equals(elementClassName));
    }

    public MetricCluster getCenterCluster() {
        return this.centerCluster;
    }

    public int getTag() {
        return this.tag;
    }

    public Set<Integer> toSet() {
        return cluster
                .stream()
                .map(MetricElement::getElementIndex)
                .collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        String sir = "";
        if (this.cluster != null)
            for (MetricElement e : this.cluster)
                sir += "Tag: " + e.getTag();
        if (this.centerCluster != null)
            return "\nCluster " + this.tag + " : centerCluster " + this.centerCluster.getTag() + " elements:" + sir;
        else
            return "\nCluster " + this.tag + " elements:" + sir;
    }
}
