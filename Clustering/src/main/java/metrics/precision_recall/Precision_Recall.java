/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package metrics.precision_recall;

import metrics.gui.MetricGUI;
import metrics.gui.MetricInterface;
import metrics.util.MetricPartition;
import util.OpenClusters;
import util.Partition;
import util.XMLParser;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;

/**
 * @author mihai
 */
public class Precision_Recall implements MetricInterface {

    private ArrayList<MetricPartition> partitions;
    private MetricGUI gui;

    public Precision_Recall() {

    }

    public static void main(String[] args) {
        MetricGUI.start(new Precision_Recall());
    }

    @Override
    public void register(MetricGUI gui) {
        partitions = new ArrayList<>();
        this.gui = gui;
    }

    public void addResult(File[] selectedFiles) {
        for (File file : selectedFiles) {
            Partition p = OpenClusters.open(file);
            MetricPartition mp = new MetricPartition(p);
            mp.algorithmPerformed = XMLParser.getHeader().algorithmPerformed;
            partitions.add(mp);
            this.gui.addGroupElement(mp.algorithmPerformed);
        }
    }

    public void displayResults(int main) {
        MetricPartition refer = partitions.get(main);

        for (MetricPartition mp : partitions) {
            try {
                Precision_RecallAlgorithm pralg = new Precision_RecallAlgorithm(refer, mp);
                float precision = pralg.calculatePrecision(refer, mp);
                float recall = pralg.calculateRecall(mp, refer);
                float rez = (precision + recall) / 2;
                this.gui.setPercent(rez, mp.algorithmPerformed + " vs " + refer.algorithmPerformed, pralg.getStats());
                System.gc();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(new JFrame(), "Selected clusterings does not match!", "Dialog",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
    }

    @Override
    public void reset() {
        partitions = new ArrayList<>();
    }

    public String getType() {
        return " Modified Precision/Recall ";
    }


}
