/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package metrics.precision_recall;

import metrics.gui.ClusteringsMatchException;
import metrics.util.MetricCluster;
import metrics.util.MetricElement;
import metrics.util.MetricPartition;

/**
 * @author mihai
 */
public class Precision_RecallAlgorithm {
    private MetricPartition A, B;
    private float precision = 0, recall = 0;

    public Precision_RecallAlgorithm(MetricPartition A, MetricPartition B) {

        this.A = new MetricPartition(A.getClusters());
        this.B = new MetricPartition(B.getClusters());

    }

    public float calculatePrecision(MetricPartition p1, MetricPartition p2) throws ClusteringsMatchException {
        try {
            this.A = new MetricPartition(p1.getClusters());
            this.B = new MetricPartition(p2.getClusters());
            this.setTags(A, B);

            this.precision = calculateIntraPairs(A);
            this.setTags(A, A);
            long n = calculateIntraPairs(A);
            this.precision = precision / n * 100;
            return this.precision;
        } catch (Exception e) {
            throw new ClusteringsMatchException();
        }


    }

    public float calculateRecall(MetricPartition p1, MetricPartition p2) throws ClusteringsMatchException {
        try {
            this.A = new MetricPartition(p1.getClusters());
            this.B = new MetricPartition(p2.getClusters());
            this.setTags(A, B);
            this.recall = calculateIntraPairs(A);
            this.setTags(A, A);
            long n = calculateIntraPairs(A);
            this.recall = recall / n * 100;
            return this.recall;
        } catch (Exception e) {
            throw new ClusteringsMatchException();
        }


    }

    private int calculateIntraPairs(MetricPartition p1) {
        int count = 0;
        for (MetricCluster c : p1.getClusters())
            count += calculateIntraPairsInCluster(c);
        return count;
    }

    private int calculateIntraPairsInCluster(MetricCluster c) {
        int count = 0;

        for (int i = 0; i < c.getElements().size() - 1; i++) {
            MetricElement e1 = c.getElements().get(i);
            for (int j = i + 1; j < c.getElements().size(); j++) {
                MetricElement e2 = c.getElements().get(j);
                if (isIntraPair(e1, e2))
                    count++;
            }
        }
        return count;
    }

    private boolean isIntraPair(MetricElement e1, MetricElement e2) {
        if (e1.getTag() == e2.getTag())
            return true;
        return false;
    }

    private void setTags(MetricPartition p1, MetricPartition p2) {
        for (MetricCluster c1 : p1.getClusters())
            for (MetricElement e : c1.getElements())
                for (int i = 0; i < p2.getClusters().size(); i++)
                    if (p2.getClusters().get(i).containsElement(e))
                        e.setTag(i);

    }

    public int calculateRecall() throws ClusteringsMatchException {
        try {
            return 1;
        } catch (Exception e) {
            throw new ClusteringsMatchException();
        }
    }


    public String getStats() {
        return "Precision: " + this.precision + " \nRecall: " + this.recall + "\n";
    }
}
