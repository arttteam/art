/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package metrics.mojo;

import metrics.gui.ClusteringsMatchException;
import metrics.gui.MetricGUI;
import metrics.gui.MetricInterface;
import metrics.util.MetricPartition;
import util.OpenClusters;
import util.Partition;
import util.XMLParser;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;

/**
 * @author mihai
 */
public class Mojo implements MetricInterface {

    private ArrayList<MetricPartition> partitions;
    private MetricGUI gui;

    public Mojo() {

    }

    public static void main(String[] args) {
        MetricGUI.start(new Mojo());
    }

    @Override
    public void register(MetricGUI gui) {
        partitions = new ArrayList<>();
        this.gui = gui;
    }

    public void addResult(File[] selectedFiles) {
        for (File file : selectedFiles) {
            Partition p = OpenClusters.open(file);
            MetricPartition mp = new MetricPartition(p);
            mp.algorithmPerformed = XMLParser.getHeader().algorithmPerformed;
            partitions.add(mp);
            this.gui.addGroupElement(mp.algorithmPerformed);
        }
    }

    public void displayResults(int main) {

        //float avg = 0;
        //int cnt = 0;

        MetricPartition refer = partitions.get(main);
        for (MetricPartition mp : partitions) {
            try {
                MojoAlgorithm mojo = new MojoAlgorithm(refer, mp);

                int mojoAB = mojo.calculate();
                int n = mojo.getPartitionBSize();
                float f = (float) mojoAB / n;
                f = ((float) 1 - f) * 100;
                //avg+=f;
                //cnt++;
                this.gui.setPercent(f, mp.algorithmPerformed + " vs " + refer.algorithmPerformed, mojo.stats);

            } catch (ClusteringsMatchException e) {
                JOptionPane.showMessageDialog(new JFrame(), "Selected clusterings does not match!", "Dialog",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        //System.err.println((float)(avg/cnt));
    }

    @Override
    public void reset() {
        partitions = new ArrayList<>();
    }

    public String getType() {
        return " MoJo ";
    }

}


