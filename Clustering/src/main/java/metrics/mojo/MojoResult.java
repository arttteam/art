package metrics.mojo;

public class MojoResult {
    private int joins, moves, splits;

    public MojoResult(int joins, int moves, int splits) {
        this.joins = joins;
        this.moves = moves;
        this.splits = splits;
    }

    public boolean hasValues(int joins, int moves, int splits) {
        if ((this.joins == joins) && (this.moves == moves) &&
                (this.splits == splits)) {
            return true;
        }

        return false;
    }
}
