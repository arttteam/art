package metrics.mojo;

import metrics.gui.ClusteringsMatchException;
import metrics.util.MetricCluster;
import metrics.util.MetricElement;
import metrics.util.MetricPartition;

import java.util.ArrayList;

public class MojoAlgorithm {

    private MetricPartition pA, pB, A, B;
    private int nrObjects = 0;
    public String stats = "";
    public int moves = 0, joins = 0, splits = 0, totalMoves = 0;

    public MojoAlgorithm(MetricPartition p1, MetricPartition p2) {
        this.A = new MetricPartition(p1.getClusters());
        this.B = new MetricPartition(p2.getClusters());
    }

    public MojoResult getMojoResult() {
        return new MojoResult(joins, moves, splits);
    }


    public int getPartitionBSize() {
        return nrObjects;
    }

    public int calculate() throws ClusteringsMatchException {
        //System.out.println("\nA to B:");
        stats = "";
        MetricPartition p1 = new MetricPartition(this.A.getClusters());
        MetricPartition p2 = new MetricPartition(this.B.getClusters());
        //System.out.println("A:"+A.toString());
        //System.out.println("B:"+B.toString());
        int r1 = this.mno(p1, p2);
        String stats1 = stats;

        //System.out.println("\nB to A:");
        stats = "";
        p1 = new MetricPartition(this.A.getClusters());
        p2 = new MetricPartition(this.B.getClusters());
        int r2 = this.mno(p2, p1);
        String stats2 = stats;

        nrObjects = count(new MetricPartition(this.pA.getClusters()));


        if (r1 < r2) {
            stats = stats1 + "objects: " + nrObjects;
            return r1;
        } else {
            stats = stats2 + "objects: " + nrObjects;
            return r2;
        }
    }

    private int count(MetricPartition p) {
        int nr = 0;
        for (MetricCluster c : p.getClusters())
            nr += c.getElements().size();
        return nr;
    }

    public int getSplits(MetricPartition p1, MetricPartition p2) throws ClusteringsMatchException {
        pA = setTags(p1, p2);
        pB = setCenterClusters(pA, p2);

        if (pA.getClusters().size() < pB.getClusters().size())
            splits = calculate_splits();

        return splits;
    }

    public int getJoins(MetricPartition p1, MetricPartition p2) throws ClusteringsMatchException {
        pA = setTags(p1, p2);
        pB = setCenterClusters(pA, p2);

        joins = calculate_joins();

        return joins;
    }

    public int getMoves(MetricPartition p1, MetricPartition p2) throws ClusteringsMatchException {
        pA = setTags(p1, p2);
        pB = setCenterClusters(pA, p2);

        moves = calculate_moves();

        return moves;
    }

    public int mno(MetricPartition p1, MetricPartition p2) throws ClusteringsMatchException {
        pA = setTags(p1, p2);

        pB = setCenterClusters(pA, p2);

        moves = 0;
        joins = 0;
        splits = 0;
        if (pA.getClusters().size() < pB.getClusters().size())
            splits = calculate_splits();

        stats += "splits: " + splits + "\n";
        joins = calculate_joins();
        stats += "joins: " + joins + "\n";
        moves = calculate_moves();
        stats += "moves: " + moves + "\n";
        totalMoves = splits + moves + joins;
        stats += "total moves: " + totalMoves + "\n";

        return totalMoves;
    }

    private MetricPartition setTags(MetricPartition p1, MetricPartition p2) {
        for (MetricCluster c : p1.getClusters()) {
            for (MetricElement e : c.getElements())
                e.setTag(p2.findElementTag(e));
        }
        return p1;
    }

    private MetricPartition setCenterClusters(MetricPartition p1, MetricPartition p2) throws ClusteringsMatchException {
        MetricCluster tempCluster;
        int max = 0;

        for (MetricCluster cp2 : p2.getClusters()) {
            max = 0;
            tempCluster = null;
            for (MetricCluster cp1 : p1.getClusters()) {
                if (cp1.getNumberOfElementsWithTag(cp2.getTag()) > max) {
                    max = cp1.getNumberOfElementsWithTag(cp2.getTag());
                    tempCluster = cp1;
                }
            }
            cp2.setCenterCluster(tempCluster);
        }
        try {
            p2 = checkDistinct(p1, p2);
        } catch (Exception e) {
            throw new ClusteringsMatchException();
        }
        for (MetricCluster c : p2.getClusters())
            c.getCenterCluster().rel = c;
        return p2;
    }

    private int calculate_joins() {
        int joins = 0;
        MetricPartition temp = new MetricPartition(pA.getClusters());
        for (MetricCluster x : pA.getClusters()) {
            if (x.rel != null)
                for (MetricCluster y : pA.getClusters())
                    if (!y.removed && y.rel == null && y.getNumberOfElementsWithTag(x.rel.getTag()) > 1) {
                        temp.joinClusters(x, y);
                        y.removed = true;
                        //System.out.println("Joined: "+x.getTag()+" - "+y.getTag());
                        joins++;
                    }
        }
        pA = temp;
        return joins;
    }

    private int calculate_moves() {
        for (MetricCluster b : pB.getClusters()) {
            MetricCluster cc = b.getCenterCluster();
            for (MetricCluster a : pA.getClusters())
                if (a.getTag() != cc.getTag())
                    for (MetricElement e : a.getElements())
                        if (e.getTag() == b.getTag()) {
                            cc.appendElement(e);
                            //System.out.println("Append: "+e.getTag()+" to cluster "+cc.getTag());
                            moves++;
                        }

        }
        return moves;
    }

    private int calculate_splits() {
        for (MetricCluster ccx : pB.getClusters()) {
            for (MetricCluster ccy : pB.getClusters())
                if (ccx.getTag() != ccy.getTag())
                    if (ccx.getCenterCluster().equals(ccy.getCenterCluster())) {
                        MetricCluster Az = ccx.getCenterCluster();
                        int x1 = Az.getNumberOfElementsWithTag(ccx.getTag());
                        int y1 = Az.getNumberOfElementsWithTag(ccy.getTag());
                        MetricCluster newA = new MetricCluster(null, pA.getClusters().size() + 1);
                        splits++;
                        if (x1 > y1) {
                            ccy.setCenterCluster(newA);
                            newA.rel = ccy;
                        } else {
                            ccx.setCenterCluster(newA);
                            newA.rel = ccx;
                        }
                        pA.addCluster(newA);

                    }
        }
        return splits;
    }

    public MetricPartition checkDistinct(MetricPartition A, MetricPartition B) {
        ArrayList Q[] = new ArrayList[B.getClusters().size() + 1];
        for (MetricCluster c : B.getClusters())
            Q[c.getTag()] = new ArrayList();
        for (MetricCluster ccx : B.getClusters()) {
            for (MetricCluster ccy : B.getClusters())
                if (ccx.getTag() != ccy.getTag())
                    if (ccx.getCenterCluster().equals(ccy.getCenterCluster())) {
                        MetricCluster Az = ccx.getCenterCluster();
                        int x1 = Az.getNumberOfElementsWithTag(ccx.getTag());
                        int y1 = Az.getNumberOfElementsWithTag(ccy.getTag());
                        int x2 = 0;
                        int y2 = 0;
                        MetricCluster Af = null, Ag = null;
                        for (MetricCluster Ai : A.getClusters())
                            if (!Ai.equals(Az)) {
                                if (x2 <= Ai.getNumberOfElementsWithTag(ccx.getTag()) && !Q[ccx.getTag()].contains(Ai.getTag())) {
                                    x2 = Ai.getNumberOfElementsWithTag(ccx.getTag());
                                    Af = Ai;
                                }
                                if (y2 <= Ai.getNumberOfElementsWithTag(ccy.getTag()) && !Q[ccy.getTag()].contains(Ai.getTag())) {
                                    y2 = Ai.getNumberOfElementsWithTag(ccy.getTag());
                                    Ag = Ai;
                                }
                            }
                        if (x2 == 0 && y2 == 0) ;
                        else if (x2 == 0) {
                            ccy.setCenterCluster(Ag);
                            Ag.rel = ccy;
                        } else if (y2 == 0) {
                            ccx.setCenterCluster(Af);
                            Af.rel = ccx;
                        } else if (x1 + y2 > x2 + y1) {
                            ccy.setCenterCluster(Ag);
                            Ag.rel = ccy;
                        } else {
                            ccx.setCenterCluster(Af);
                            Af.rel = ccx;
                        }

                    }
        }
        return B;
    }
}



