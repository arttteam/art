/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package metrics.gui;

import java.io.File;

/**
 * @author mihai
 */
public interface MetricInterface {

    public void reset();

    public void register(MetricGUI gui);

    public void addResult(File[] selectedFiles);

    public void displayResults(int main);

    public String getType();
}
