package metrics.gui;

import javax.swing.*;
import java.awt.*;

public class StatPanel extends JPanel {

    final static boolean MULTICOLORED = false;
    final static int MAX = 10000;

    StatPanel(float percent, String myTitle, String stats) {
        if (MULTICOLORED) {
            setOpaque(true);
            setBackground(new Color(0, 255, 255));
        }
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(myTitle),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        JProgressBar bar = new JProgressBar() {
            @Override
            public Dimension getMinimumSize() {
                return getPreferredSize();
            }

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(350,
                        super.getPreferredSize().height);
            }

            @Override
            public Dimension getMaximumSize() {
                return getPreferredSize();
            }
        };

        bar.setForeground(Color.black);
        bar.setString(percent + "%");
        bar.setStringPainted(true);
        bar.setValue((int) percent);
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

        JTextArea jTextArea = new JTextArea();
        jTextArea.setText(stats);
        jTextArea.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        jTextArea.setColumns(5);
        jTextArea.setFont(new java.awt.Font("Courier New", 1, 11)); // NOI18N
        jTextArea.setRows(5);
        jTextArea.setMargin(new Insets(0, 10, 0, 0));
        add(bar);
        add(jTextArea);
        bar.setAlignmentY(TOP_ALIGNMENT);


    }

    @Override
    public Dimension getMaximumSize() {
        return new Dimension(Integer.MAX_VALUE,
                getPreferredSize().height);
    }

}

