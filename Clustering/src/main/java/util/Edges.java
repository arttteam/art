/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.util.ArrayList;

/**
 * @author mihai
 */
public class Edges {

    private ArrayList<Edge> edges;

    public Edges() {
        this.edges = new ArrayList<>();
    }

    public void addEdge(Edge e) {
        this.edges.add(e);
    }

    public int getSize() {
        return this.edges.size();
    }

    public ArrayList<Edge> getEdges() {
        return this.edges;
    }
}
