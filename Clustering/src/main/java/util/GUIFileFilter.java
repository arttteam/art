/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * @author mihai
 */
public class GUIFileFilter extends FileFilter {

    private AcceptedFiles acceptedf;

    public GUIFileFilter(AcceptedFiles af) {
        this.acceptedf = af;
    }

    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        //String extension = AcceptedFiles.getExtension(f);
        if (acceptedf.isAccepted(f)) {
            // if (extension != null) {
            //     if (extension.equals(Utils.cls) ||
            //          extension.equals(Utils.jar)) {
            return true;
            //    } else {
            //         return false;
            //     }
        }

        return false;
    }

    //The description of this filter
    public String getDescription() {
        return "Java files";
    }
}
