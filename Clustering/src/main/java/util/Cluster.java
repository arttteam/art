package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Cluster {

    private List<Element> elements;
    private int tag;

    public Cluster(int tag) {
        this.tag = tag;
        this.elements = new ArrayList<>();
    }

    public Cluster(int tag, Collection<Element> elements) {
        this(tag);
        this.elements.addAll(elements);
    }

    public void add(Element e) {
        this.elements.add(e);
    }

    public int getSize() {
        return this.elements.size();
    }

    public List<Element> getElements() {
        return this.elements;
    }

    public int getTag() {
        return this.tag;
    }

}
