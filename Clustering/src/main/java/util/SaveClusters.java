package util;

import clusterFinder.ClusterFinder;
import sysmodel.DSM;

import java.util.List;
import java.util.Set;


public class SaveClusters {

    public static List<? extends Set<Integer>> cycles;
    private static CycleIndex[] indexes;
    private int ix, iy;
    private static DSM d;
    private static ClusterFinder f;

    public SaveClusters(DSM d, ClusterFinder f) {
        this.d = d;

        //f=new BinaryCycleFinder();
        init(f);

    }

    private void init(ClusterFinder f) {

        //cycles=f.findClusters(d);
        this.buildCycleIndexArray();
        //drawGrid(0,0);
        //label.addMouseListener(new CycleElements(this));
    }

    private void buildCycleIndexArray() {
        this.indexes = new CycleIndex[cycles.size()];
        int i = 0, len = 0;
        for (Set<Integer> s : cycles) {
            indexes[i] = new CycleIndex(len, s);
            len += s.size();
            i++;
        }
        ix = iy = 0;
    }

    public void writeValues(String filename, String alg) {
        new XMLWriter(filename, alg);
        //System.out.println(cycles.toString());
        int i = 0;
        for (Set<Integer> c : cycles) {
            i++;
            Cluster cluster = new Cluster(i);
            for (Integer elem : c) {
                cluster.add(new Element(d.elementAt(elem), elem));
            }
            XMLWriter.appendCluster(cluster);
        }
        XMLWriter.closeXML();

    }

}

class CycleIndex {
    int index;
    Set<Integer> cycle;

    public CycleIndex(int index, Set<Integer> cycle) {
        this.index = index;
        this.cycle = cycle;
    }
}