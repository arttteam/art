/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.xml.sax.ContentHandler;
import org.xml.sax.helpers.AttributesImpl;

import java.io.FileOutputStream;

/**
 * @author mihai
 */
public class XMLWriter {

    private static AttributesImpl atts;
    private static ContentHandler hd;
    private static FileOutputStream fos;

    public XMLWriter(String filename, String alg) {
        try {
            fos = new FileOutputStream(filename);
            OutputFormat of = new OutputFormat("XML", "ISO-8859-1", true);
            of.setIndent(1);
            of.setIndenting(true);
            XMLSerializer serializer = new XMLSerializer(fos, of);
            hd = serializer.asContentHandler();
            hd.startDocument();

            atts = new AttributesImpl();
            hd.startElement("", "", "clusters", atts);
            atts = new AttributesImpl();
            atts.addAttribute("", "", "algorithm", "CDATA", alg);
            hd.startElement("", "", "header", atts);
            hd.endElement("", "", "header");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeXML() {
        try {
            hd.endElement("", "", "clusters");
            hd.endDocument();
            fos.close();
        } catch (Exception e) {
        }
    }

    public static void appendCluster(Cluster c) {
        atts = new AttributesImpl();
        // USERS tag.
        try {
            atts.clear();
            atts.addAttribute("", "", "tag", "CDATA", String.valueOf(c.getTag()));
            hd.startElement("", "", "cluster", atts);
            for (Element e : c.getElements()) {
                atts.clear();
                atts.addAttribute("", "", "classname", "CDATA", e.classname);
                atts.addAttribute("", "", "index", "CDATA", String.valueOf(e.index));
                hd.startElement("", "", "element", atts);
                hd.endElement("", "", "element");
            }
            hd.endElement("", "", "cluster");
        } catch (Exception exc) {
        }
    }

    public static void appendEdges(Edges c) {
        atts = new AttributesImpl();
        // USERS tag.
        try {
            hd.startElement("", "", "edges", atts);
            for (Edge e : c.getEdges()) {
                atts.clear();
                atts.addAttribute("", "", "e1", "CDATA", String.valueOf(e.e1));
                atts.addAttribute("", "", "e2", "CDATA", String.valueOf(e.e2));
                atts.addAttribute("", "", "value", "CDATA", String.valueOf(e.val));
                hd.startElement("", "", "edge", atts);

                hd.endElement("", "", "edge");
            }
            hd.endElement("", "", "edges");
        } catch (Exception exc) {
        }
    }
}
