/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import clusterFinder.ClusterFinder;
import sysmodel.DSM;

import java.io.File;

/**
 * @author mihai
 */
public class SaveDSM {

    private SaveClusters saveClusters;

    public SaveDSM(File selectedFile, DSM d, ClusterFinder f, String AlgorithmPerformed) {
        this.saveClusters = new SaveClusters(d, f);
        this.saveClusters.writeValues(selectedFile.getAbsolutePath(), AlgorithmPerformed);
    }
}
