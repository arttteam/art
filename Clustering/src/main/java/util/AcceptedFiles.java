/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.io.File;
import java.util.ArrayList;

/**
 * @author mihai
 */
public class AcceptedFiles {

    private ArrayList<String> exts;

    public AcceptedFiles() {
        this.exts = new ArrayList();
    }

    public void addExtension(String ext) {
        this.exts.add(ext);
    }

    private String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    public boolean isAccepted(File f) {
        String fExt = this.getExtension(f);
        for (String s : exts) {
            if (fExt != null && s.compareTo(fExt) == 0)
                return true;
        }
        return false;
    }
}
