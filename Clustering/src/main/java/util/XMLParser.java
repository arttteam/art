/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

/**
 * @author mihai
 */
public class XMLParser extends DefaultHandler {

    private static Partition part;
    private Cluster cluster;
    private static Header header;

    public static void init(File file) {
        try {
            DefaultHandler dotsHandler = new XMLParser();
            header = new Header();
            part = new Partition();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            //use default non-validating parser
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(file, dotsHandler);
        } catch (Throwable t) { // much too general exception handling !!
            t.printStackTrace();
        }
    }

    public static Partition getPartition() {
        return part;
    }

    public static Header getHeader() {
        return header;
    }

    @Override
    public void startDocument()
            throws SAXException {

    }

    @Override
    public void endDocument()
            throws SAXException {

    }

    @Override
    public void startElement(String namespaceURI, String localName,
                             String qName, Attributes atts)
            throws SAXException {
        switch (qName) {
            case "element":
                cluster.add(new Element(atts.getValue("classname"), Integer.parseInt(atts.getValue("index"))));
                break;
            case "cluster":
                cluster = new Cluster(Integer.parseInt(atts.getValue("tag")));
                break;
            case "header":
                header.algorithmPerformed = atts.getValue("algorithm");
                break;
        }


    }


    // Called at the end of each element
    public void endElement(java.lang.String uri,
                           java.lang.String localName,
                           java.lang.String qName)
            throws SAXException {
        if (qName.equals("cluster")) {
            part.appendCluster(cluster);
        }
    }

    // Called for characters between nodes.
    public void characters(char buf[], int offset, int len)
            throws SAXException {
        String s = new String(buf, offset, len);
        s = s.trim();
        if (!s.equals("")) {
            // System.out.println("characters:" + s);
        }
    }


}
