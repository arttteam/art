/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.util.ArrayList;
import java.util.List;

public class Partition {

    private final List<Cluster> partition;
    public int classes;

    public Partition() {
        partition = new ArrayList<>();
        this.classes = 0;
    }

    public Partition(int size) {
        partition = new ArrayList<>(size);
        for (int i = 0; i < size; i++)
            partition.add(null);
    }

    public void appendCluster(Cluster c) {
        partition.add(c);
        this.classes += c.getSize();
    }

    public void appendCluster(int index, Cluster c) {
        partition.add(index, c);
    }

    public List<Cluster> getClusters() {
        return this.partition;
    }

    public Cluster getCluster(int i) {
        try {
            this.partition.get(i);
            return this.partition.get(i);
        } catch (Exception e) {
            return null;
        }
    }

    public String toString() {
        String sir = "";
        for (Cluster c : partition) {
            sir += "\n";
            for (Element e : c.getElements()) {
                sir += e.toString();
            }
        }
        return sir;
    }
}
