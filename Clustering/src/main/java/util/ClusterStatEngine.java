package util;

import sysmodel.DSM;
import sysmodel.SparceMatrix;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class ClusterStatEngine implements StatEngine {
    private List<? extends Set<Integer>> clusters;
    private SparceMatrix<Integer> matrix;
    private DSM d;
    private Map<String, Integer> packageFrequency;
    public static int partitionSize = 0;

    public ClusterStatEngine(List<? extends Set<Integer>> list, DSM d) {
        this.clusters = list;
        this.matrix = d.getDependencyMatrix();
        this.d = d;
        this.packageFrequency = new HashMap<>();
    }

    public String start() {
        StringBuilder sb = new StringBuilder();
        int c = clusters.size();
        float n = matrix.getRows();
        sb.append(" -----------\n\n");
        sb.append("Clusters: " + c + "\n");
        int count = 0;
        for (int i = 0; i < d.getDependencyMatrix().getRows(); i++)
            for (int j = 0; j < d.getDependencyMatrix().getColumns(); j++)
                if (d.getDependencyMatrix().getElement(i, j) != null)
                    count++;
        float densitate = (float) count / (d.getDependencyMatrix().getRows() * d.getDependencyMatrix().getColumns()) * 100;
        sb.append("Density: " + densitate + "\n");
        sb.append("Partition Levels: " + partitionSize + "\n");
        if (c != 0)
            sb.append("Average nodes/clusters: " + n / c + "\n");
        else
            sb.append("Average nodes/clusters: 0\n");
        List<FutureTask<String>> workers = new LinkedList<>();
        ClusterWorker[] w = new ClusterWorker[c];
        int i = 0;
        for (Set<Integer> s : clusters) {
            w[i] = new ClusterWorker(s, d);
            FutureTask<String> current = new FutureTask<>(w[i]);
            i++;
            workers.add(current);
            new Thread(current).start();
        }
        i = 0;
        float maxCoupl = 0;
        float minCohe = Integer.MAX_VALUE;
        float maxPacks = 0;
        for (FutureTask<String> ft : workers) {
            try {
                //sb.append(ft.get());
                ft.get();
                if (w[i].cohe < minCohe)
                    minCohe = w[i].cohe;
                if (w[i].coupl > maxCoupl)
                    maxCoupl = w[i].coupl;
                Set<String> packs = w[i].packages.keySet();
                int size = packs.size();
                if (maxPacks < size)
                    maxPacks = size;
                this.addPackSet(packs);
                i++;
            } catch (InterruptedException | ExecutionException e) {
            }
        }
        float adj = adj(minCohe);
        float group = grouping(maxPacks);
        float frag = fragmentation();


        double g = adj * frag + (1 - adj) * group;//Math.sqrt((adj*adj+ frag*frag+2*group*group)/4);
        return sb.append("Estimation value: " + (c * g) / n + "\nMin coupling: " + minCohe + " \nMax cohesion: " + maxCoupl + "\n").toString();
    }

    private float compute() {
        Collection<Integer> c = this.packageFrequency.values();
        int max = 0;
        for (Integer i : c)
            max += i;
        return max / c.size();
    }

    private float fragmentation() {
        float max = compute();
        if (max <= 4)
            return 1f - (max - 1) / 10;
        return 1f / max;
    }

    private float grouping(float num) {
        if (num <= 2)
            return 1f - (num - 1) / 2;
        return 0.1f;
    }

    private float adj(float x) {
        if (x > 20)
            return 1f;
        else
            return x / 20;
    }

    private void addPackSet(Set<String> set) {
        for (String s : set) {
            Integer value = this.packageFrequency.get(s);
            if (value != null)
                value++;
            else
                value = 1;
            this.packageFrequency.put(s, value);
        }
    }
}


class ClusterWorker implements Callable<String> {

    private Set<Integer> cluster;
    private SparceMatrix<Integer> matrix;
    private DSM d;
    float coupl;
    float cohe;
    Map<String, Integer> packages;
    float f;

    public ClusterWorker(Set<Integer> cluster, DSM d) {
        this.cluster = cluster;
        this.matrix = d.getDependencyMatrix();
        this.d = d;
        packages = new HashMap<>();
    }

    public String call() throws Exception {
        StringBuilder sb = new StringBuilder();
        int n = cluster.size();
        sb.append("\n" + n + " elements\n");
        float internal = 0, external = 0;
        int ni = n * (n - 1);
        for (Integer i : cluster) {
            sb.append(writeNodeInfo(i));
            Set<Integer> t = matrix.getConnectedToColumns(i);
            if (t != null) {
                for (Integer j : t) {
                    int val = matrix.getElement(j, i);
                    if (cluster.contains(j)) {
                        internal += val;
                    } else {
                        external += val;
                    }
                }
            }
        }
        computePackages();
        f = maxFreq() / n;
        computeLowest(internal, ni, external);
        sb.append("internal avg " + internal / ni + "\n");
        sb.append("external avg " + external + "\n");
        return sb.toString();
    }

    private void computePackages() {
        for (Integer i : cluster) {
            String x = d.elementAt(i);
            int index = x.lastIndexOf('.');
            if (index >= 0) {
                String s = x.substring(0, index - 1);
                Integer value = this.packages.get(s);
                if (value != null)
                    value++;
                else
                    value = 1;
                this.packages.put(s, value);
            }
        }
    }

    private float maxFreq() {
        float x = 0;
        Set<String> set = this.packages.keySet();
        for (String s : set) {
            Integer i = this.packages.get(s);
            if (i > x)
                x = i;
        }
        return x;
    }

    private void computeLowest(float internal, int ni, float external) {
        if (ni == 0)
            cohe = Integer.MAX_VALUE;
        else
            cohe = internal / ni;
        coupl = external;
    }


    private StringBuffer writeNodeInfo(int node) {
        StringBuffer ret = new StringBuffer().append(node);
        return ret.append("\n");
    }
}


