package util;

import sysmodel.SparceMatrix;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Set;

public class PartitionStatEngine implements StatEngine {
    private int size = 0;
    private SparceMatrix<Integer> matrix;
    private List<? extends Set<Integer>> partitions;
    public List<? extends Set<Integer>> cycles;
    private Stats stats;
    int[] levels;

    public PartitionStatEngine(List<? extends Set<Integer>> list, SparceMatrix<Integer> matrix) {
        this.partitions = list;
        this.matrix = matrix;
        this.buildLevels();
        this.stats = new Stats();
    }

    public String start() {
        StringBuilder sb = new StringBuilder();
        int n = matrix.getColumns();
        double l = 0;
        for (int i = 0; i < n; i++) {
            Set<Integer> s = matrix.getConnectedToColumns(i);
            if (s != null)
                l += s.size();
        }
        int bigger = 0;
        for (Set<Integer> s : this.partitions) {
            int size = s.size();
            if (size > 5)
                bigger++;
            for (Integer i : s)
                writeNodeInfo(i);
        }
        sb.append(" -----------\n");
        sb.append("\nClasses: " + n);
        sb.append("\nLevels: " + size);
        NumberFormat formatter = new DecimalFormat("#0.00");
        sb.append("\nDensity: " + formatter.format((100 * l / n) / n) + "%");
        sb.append("\nCycles with more then 5 elements: " + bigger + "\n");

        if (stats.count != 0) {
            sb.append("\nAverage max distance: " + stats.sumDistMax / stats.count);
            sb.append("\nAverage max distance(%): " + formatter.format(stats.sumDistMax2 / stats.count) + "%");
            sb.append("\nAverage distance: " + stats.sumDistMed / stats.count);
            sb.append("\nAverage distance(%): " + formatter.format(100 * stats.sumDistMed / stats.count / size) + "%");
            sb.append("\nAbsolute avg dist: " + stats.dMaxAbs);
            sb.append("\nAbsolute avg dist(%): " + formatter.format(100 * stats.dMaxAbs / size) + "%");
        } else {
            sb.append("\nAverage max distance: 0");
            sb.append("\nAverage max distance(%): 0%");
            sb.append("\nAverage distance: 0");
            sb.append("\nAverage distance(%): 0%");
            sb.append("\nAbsolute avg dist: 0");
            sb.append("\nAbsolute avg dist(%): 0%");
        }

        sb.append("\n\n");
        return sb.toString();
    }

    private void buildLevels() {
        size = partitions.size();
        levels = new int[matrix.getRows()];
        int j = 0;
        for (Set<Integer> s : partitions) {
            for (Integer i : s) {
                levels[i] = j;
            }
            j++;
        }
    }

    private StringBuffer writeNodeInfo(int node) {
        StringBuffer ret = new StringBuffer().append(node);
        Set<Integer> s = matrix.getConnectedToColumns(node);

        ret.append(" : " + levels[node] + " ");
        if (s != null) {
            // System.out.println("\n "+node);
            int max = -1, nr = 0;
            float sum = 0;
            for (Integer i : s) {
                int current = levels[i];

                // System.out.println(i+" : "+current);
                sum += current - levels[node];
                nr++;
                if (current > max)
                    max = current;
            }
            int x = max - levels[node];
            if (x > stats.dMaxAbs)
                stats.dMaxAbs = x;
            stats.sumDistMax += x;
            stats.sumDistMax2 += 100 * x / size;
            stats.count++;
            stats.sumDistMed += sum / nr;

            ret.append("   " + sum / nr + "   " + max + " max difference " + x + " in % " + 100 * x / size);
        }
        return ret.append("\n");
    }
}
