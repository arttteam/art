/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.io.File;

/**
 * @author mihai
 */
public class OpenClusters {

    public static Partition open(File file) {
        XMLParser.init(file);

        return XMLParser.getPartition();
    }

}
