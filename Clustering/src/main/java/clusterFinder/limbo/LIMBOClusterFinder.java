package clusterFinder.limbo;

import clusterFinder.ClusterFinder;
import clusterFinder.limbo.dcf.DCF;
import clusterFinder.limbo.dcf.DCFUtil;
import clusterFinder.limbo.tree.LimboTree;
import clusterFinder.limbo.util.LogCalculator;
import clusterFinder.postProcess.PostProcessor;
import clusterFinder.preProcess.PreProcessor;
import dataFormat.Partition;
import metrics.mojo.MojoAlgorithm;
import metrics.mojo.MojoResult;
import metrics.util.MetricPartition;
import sysmodel.DSM;
import sysmodel.PackageNames;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;
import sysmodel.edge.EdgeListUtil;
import sysmodel.edge.statistic.EdgeStatistic;
import sysmodel.edge.statistic.MidValueStatistic;

import java.util.*;

public class LIMBOClusterFinder extends ClusterFinder {
    private static int logBase = 2;

    private int branchingFactor = 4;
    private double phi = 0.0;
    private int limboS;
    private boolean usePackageNames;
    private LogCalculator logCalculator;

    public LIMBOClusterFinder(PreProcessor preProcessor,
                              PostProcessor postProcessor, int branchingFactor,
                              int limboS, double phi, double zeroPenalization,
                              boolean usePackageNames) {
        super(preProcessor, postProcessor);
        if ((branchingFactor < 1) ||
                (limboS < 0) ||
                (phi < 0) || (phi > 1) ||
                (zeroPenalization < 0) || (zeroPenalization > 1022)) {
            throw new IllegalArgumentException();
        }
        this.branchingFactor = branchingFactor;
        this.phi = phi;
        this.limboS = limboS;
        this.usePackageNames = usePackageNames;
        logCalculator = new LogCalculator(logBase, zeroPenalization);
    }

    @Override
    protected List<? extends Set<Integer>> computeClusters(DSM initialDSM,
                                                           Collection<Edge> processedEdges) {
        SparceMatrix<Integer> initialMatrix = initialDSM.getDependencyMatrix();
        int nodeNumber = initialMatrix.getRows();

        //if no possible DCFs, do not run
        if (processedEdges.size() == 0) {
            List<Set<Integer>> result = new ArrayList<>();
            Set<Integer> auxCluster;

            for (int i = 0; i < nodeNumber; i++) {
                auxCluster = new HashSet<>();
                auxCluster.add(i);
                result.add(auxCluster);
            }

            return result;
        }

        double nodeP = (double) 1 / nodeNumber;
        EdgeStatistic dependecyStatistic = new MidValueStatistic();
        Map<Integer, Collection<Edge>> edgeMap =
                EdgeListUtil.getEdgeToNodeMap(processedEdges, dependecyStatistic);

        List<DCF> initialDCFs = computeInitialDCFs(edgeMap, nodeP, initialDSM,
                dependecyStatistic.getValue());
        List<String> allAttrs = DCFUtil.getAllAttributes(initialDCFs);

        int maxLeafNodeNr = limboS / (allAttrs.size() * branchingFactor);
        LimboTree limboTree = computeLimboTree(initialDCFs,
                computeRPhi(initialDCFs, allAttrs, nodeP),
                maxLeafNodeNr);
        AIBList aibList = new AIBList(limboTree.getDCFs(), initialDCFs);
        List<Partition> clusterizations = new ArrayList<>();
        Partition currentClusterization = null;

        while (aibList.hasNextClusterization()) {
            currentClusterization = aibList.nextClusterization(initialDSM);
            if (clusterizations.size() > 0) {
                if ((!currentClusterization.equals(clusterizations.get(0))) &&
                        (currentClusterization.getClusterNumber() >= 2))

                {
                    clusterizations.add(0, currentClusterization);
                }
            } else {
                clusterizations.add(currentClusterization);
            }
        }

        if (clusterizations.size() >= 2) {
            for (int i = 0; i < clusterizations.size() - 1; i++) {
                if (mojoCondition(clusterizations.get(i),
                        clusterizations.get(i + 1))) {
                    return addRemovedNodes(clusterizations.get(i).toListSet(), nodeNumber);
                }
            }
        }

        return addRemovedNodes(clusterizations.get(0).toListSet(), nodeNumber);
    }

    @Override
    protected Collection<Edge> computeDependecyEdges(
            SparceMatrix<Integer> matrix) {
        int nodeNumber = matrix.getRows();
        Collection<Edge> edges = new TreeSet<>();

        for (int i = 0; i < nodeNumber; i++) {
            Set<Integer> s = matrix.getConnectedToRow(i);

            if (s != null) {
                for (Integer j : s) {
                    edges.add(new Edge(i, j, matrix.getElement(i, j)));
                }
            }
        }

        return edges;
    }

    private List<DCF> computeInitialDCFs(Map<Integer, Collection<Edge>> edgeMap,
                                         double nodeP, DSM initialDSM, double referenceValue) {
        List<DCF> initialDCFs = new ArrayList<>();
        DCF auxDCF;

        PackageNames packageNames = initialDSM.getPackageNames();
        Set<Map.Entry<Integer, Collection<Edge>>> edgeMapKeys =
                edgeMap.entrySet();
        Iterator<Map.Entry<Integer, Collection<Edge>>> itKeys =
                edgeMapKeys.iterator();
        Map.Entry<Integer, Collection<Edge>> auxEntry;
        Collection<Edge> auxEdgeList;
        String className;
        while (itKeys.hasNext()) {
            auxEntry = itKeys.next();
            auxEdgeList = auxEntry.getValue();
            if (auxEdgeList == null) {
                auxEdgeList = new ArrayList<>();
            }
            if (usePackageNames) {
                className = initialDSM.elementAt(auxEntry.getKey());
                auxDCF = new DCF(logCalculator,
                        auxEntry.getKey().toString(),
                        nodeP,
                        auxEdgeList,
                        packageNames.getMatchValueMap(className, referenceValue));
            } else {
                auxDCF = new DCF(logCalculator,
                        auxEntry.getKey().toString(),
                        nodeP,
                        auxEdgeList);
            }
            initialDCFs.add(auxDCF);
        }

        return initialDCFs;
    }

    private LimboTree computeLimboTree(List<DCF> initialDCFs, double mergeInfoLossThreshold,
                                       int maxLeafNodeNr) {
        LimboTree limboTree = new LimboTree(branchingFactor, maxLeafNodeNr, mergeInfoLossThreshold);

        for (DCF initialDCF : initialDCFs) {
            try {
                limboTree.addDCF(initialDCF.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }

        return limboTree;
    }

    private double computeRPhi(Collection<DCF> initialDCFs, List<String> attrs,
                               double nodeP) {
        double mutualInfo;
        int n = initialDCFs.size();
        mutualInfo = computeEntropy(n, nodeP) -
                computeEntropy(initialDCFs, attrs, nodeP);

        return phi * (mutualInfo / n);
    }

    private double computeEntropy(int n, double nodeP) {
        double entropy = 0;

        for (int i = 0; i < n; i++) {
            entropy += nodeP * logCalculator.log(nodeP);
        }

        return entropy * (-1);
    }

    private double computeEntropy(Collection<DCF> dcfs,
                                  Collection<String> attributes, double nodeP) {
        double entropy = 0,
                currentPBA,
                currentPBASum;
        Iterator<DCF> itDCFs = dcfs.iterator();
        Iterator<String> itAttrs;
        DCF currentDCF;

        while (itDCFs.hasNext()) {
            currentDCF = itDCFs.next();
            itAttrs = attributes.iterator();
            currentPBASum = 0;

            while (itAttrs.hasNext()) {
                currentPBA = currentDCF.getAttrValue(itAttrs.next());
                if (currentPBA != 0) {
                    currentPBASum += currentPBA *
                            logCalculator.log(currentPBA);
                }
            }

            entropy += nodeP * currentPBASum;
        }

        return entropy * (-1);
    }

    private boolean mojoCondition(Partition partitionK, Partition partitionKPLUS1) {

        MojoAlgorithm mojo = new MojoAlgorithm(new MetricPartition(partitionKPLUS1),
                new MetricPartition(partitionK));
        try {
            mojo.mno(new MetricPartition(partitionKPLUS1),
                    new MetricPartition(partitionK));
        } catch (Exception e) {

        }
        MojoResult result = mojo.getMojoResult();

        return result.hasValues(1, 0, 0);
    }

    private List<? extends Set<Integer>> addRemovedNodes(
            List<Set<Integer>> clusterization, int nodeNumber) {
        boolean[] added = new boolean[nodeNumber];
        Iterator<Set<Integer>> itClusters = clusterization.iterator();
        Iterator<Integer> itNodes;

        while (itClusters.hasNext()) {
            itNodes = itClusters.next().iterator();
            while (itNodes.hasNext()) {
                added[itNodes.next()] = true;
            }
        }

        Set<Integer> auxCluster;

        for (int i = 0; i < added.length; i++) {
            if (!added[i]) {
                auxCluster = new HashSet<>();
                auxCluster.add(i);
                clusterization.add(auxCluster);
            }
        }
        //System.out.println(clusterization);
        return clusterization;
    }

    @Override
    public String toString() {
        String result = "Limbo_Phi_" + (float) phi + "_BF_" + (float) branchingFactor +
                "_LS_";

        if (limboS == Integer.MAX_VALUE) {
            result += "MAX";
        } else {
            result += limboS;
        }

        if (usePackageNames) {
            result += "PNY";
        }

        return result + "_ZP_" + (float) logCalculator.getZeroPenalization() + "_" +
                super.toString();
    }
}
