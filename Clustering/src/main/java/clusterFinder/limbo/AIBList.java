package clusterFinder.limbo;

import clusterFinder.limbo.dcf.DCF;
import clusterFinder.limbo.dcf.DCFInfoLossDistance;
import dataFormat.Cluster;
import dataFormat.Element;
import dataFormat.Partition;
import sysmodel.DSM;

import java.util.*;

public class AIBList {
    private List<DCF> limboDCFs,
            initialDCFs;
    private PriorityQueue priorityQueue;
    private Partition currentClusterization;

    public AIBList(List<DCF> limboDCFs, List<DCF> initialDCFs) {
        if ((limboDCFs == null) ||
                (initialDCFs == null)) {
            throw new IllegalArgumentException();
        }

        this.limboDCFs = limboDCFs;
        priorityQueue = new PriorityQueue(limboDCFs);
        this.initialDCFs = initialDCFs;
        currentClusterization = null;
    }

    public boolean hasNextClusterization() {
        if (limboDCFs.size() == 2) {
            return false;
        }

        return true;
    }

    public Partition nextClusterization(DSM initialDSM) {
        if (limboDCFs.size() == 1) {
            throw new NoSuchElementException();
        }

        if (currentClusterization != null) {
            merge();
        }

        Map<DCF, Cluster> resultMap = new HashMap<>();
        Cluster auxCluster;
        int nodeID,
                tagNumber = 1;
        Iterator<DCF> itInitialDCFs = initialDCFs.iterator();
        DCF auxInitialDCF,
                selectedDCF;
        String className;

        while (itInitialDCFs.hasNext()) {
            auxInitialDCF = itInitialDCFs.next();
            selectedDCF = getClosestFromLimboDCFs(auxInitialDCF);

            if (selectedDCF == null) {
                throw new IllegalStateException();
            }

            auxCluster = resultMap.get(selectedDCF);

            if (auxCluster == null) {
                auxCluster = new Cluster(tagNumber);
                tagNumber += 1;
                resultMap.put(selectedDCF, auxCluster);
            }
            nodeID = Integer.valueOf(auxInitialDCF.getId());

            if (initialDSM == null) {
                className = String.valueOf(nodeID);
            } else {
                className = initialDSM.elementAt(nodeID);
            }

            auxCluster.add(new Element(className, nodeID));
        }

        currentClusterization = new Partition(
                new ArrayList<>(resultMap.values()));

        return currentClusterization;
    }

    private void merge() {
        if (limboDCFs.size() >= 2) {
            DCFInfoLossDistance distance = priorityQueue.getFirst();
            if (distance != null) {
                DCF firstDCF = distance.getFirst(),
                        secondDCF = distance.getSecond();
                DCF newDCF = firstDCF.merge(secondDCF);
                limboDCFs.remove(firstDCF);
                limboDCFs.remove(secondDCF);
                priorityQueue.removeAll(firstDCF);
                priorityQueue.removeAll(secondDCF);
                priorityQueue.addAll(newDCF, limboDCFs);
                limboDCFs.add(newDCF);
            }
        }
    }

    private DCF getClosestFromLimboDCFs(DCF referenceDCF) {
        Iterator<DCF> itLimboDCFs = limboDCFs.iterator();
        DCF auxLimboDCF,
                selectedDCF = null;
        double minDistance = Double.MAX_VALUE,
                auxMinDistance;

        while (itLimboDCFs.hasNext()) {
            auxLimboDCF = itLimboDCFs.next();
            auxMinDistance = referenceDCF.getKullbackLeiblerDivergence(auxLimboDCF);

            if (auxMinDistance < minDistance) {
                minDistance = auxMinDistance;
                selectedDCF = auxLimboDCF;
            }
        }

        return selectedDCF;
    }

    @Override
    public String toString() {
        return limboDCFs.toString();
    }
}
