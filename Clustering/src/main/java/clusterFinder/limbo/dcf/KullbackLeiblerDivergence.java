package clusterFinder.limbo.dcf;

import clusterFinder.limbo.util.LogCalculator;

import java.util.Iterator;
import java.util.List;

public class KullbackLeiblerDivergence {
    public double compute(LogCalculator logCalculator, List<String> allAttrs,
                          DCF dcfX, DCF dcfY, double pCStar) {
        Double result = 0d;

        String auxAttr;
        double pValue, qValue;

        for (String allAttr : allAttrs) {
            auxAttr = allAttr;

            pValue = dcfX.getAttrValue(auxAttr);
            qValue = getQValue(pValue, auxAttr, dcfX, dcfY, pCStar);

            if (pValue != 0) {
                result += pValue * (logCalculator.log(pValue)
                        - logCalculator.log(qValue));
            }
        }

        return result;
    }

    protected double getQValue(double pValue, String auxAttr,
                               DCF dcfX, DCF dcfY, double pCStar) {
        return dcfY.getAttrValue(auxAttr);
    }
}
