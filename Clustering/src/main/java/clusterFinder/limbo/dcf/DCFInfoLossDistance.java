package clusterFinder.limbo.dcf;

public class DCFInfoLossDistance implements Comparable<DCFInfoLossDistance> {
    private DCF first, second;
    private double distance;

    public DCFInfoLossDistance(DCF first, DCF second) {
        if ((first == null) || (second == null)) {
            throw new NullPointerException();
        }

        this.first = first;
        this.second = second;
        distance = first.getInformationLoss(second);
    }

    @Override
    public int compareTo(DCFInfoLossDistance o) {
        if (distance < o.distance)
            return -1;
        else if (distance > o.distance)
            return 1;
        return 0;
    }

    @Override
    public int hashCode() {
        int result = 1;

        result = 31 * result + ((first == null) ? 0 : first.hashCode());
        result = 47 * result + ((second == null) ? 0 : second.hashCode());

        return result;
    }

    public DCF getFirst() {
        return first;
    }

    public DCF getSecond() {
        return second;
    }

    public boolean hasDCF(DCF dcf) {
        return first.equals(dcf) || second.equals(dcf);
    }
}
