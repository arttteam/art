package clusterFinder.limbo.dcf;

public class KullbackLeiblerDivergenceWithPBar extends KullbackLeiblerDivergence {
    @Override
    protected double getQValue(double pValue, String auxAttr,
                               DCF dcfX, DCF dcfY, double pCStar) {
        //pBar
        return (dcfX.getP() / pCStar) * pValue +
                (dcfY.getP() / pCStar) * dcfY.getAttrValue(auxAttr);
    }
}
