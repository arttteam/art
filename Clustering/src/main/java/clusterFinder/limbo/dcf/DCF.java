package clusterFinder.limbo.dcf;

import clusterFinder.limbo.util.LogCalculator;
import sysmodel.edge.Edge;

import java.util.*;

public class DCF implements Cloneable {
    private String id;
    private double p;
    private Map<String, Double> attributes;
    private LogCalculator logCalculator;

    public DCF(LogCalculator logCalculator, String id, double p, Map<String, Double> attributes) {
        this(logCalculator, id, p);

        if (attributes == null) {
            throw new IllegalArgumentException();
        }

        this.attributes = new HashMap<>(attributes);
    }

    public DCF(LogCalculator logCalculator, String id, double p,
               Collection<Edge> dependecies) {
        this(logCalculator, id, p);

        if (dependecies == null) {
            throw new IllegalArgumentException();
        }

        initAttributes(dependecies);
    }

    public DCF(LogCalculator logCalculator, String id, double p,
               Collection<Edge> dependecies, Map<String, Double> packageNames) {
        this(logCalculator, id, p);

        if (dependecies == null) {
            throw new IllegalArgumentException();
        }

        initAttributes(dependecies, packageNames);
    }

    private DCF(LogCalculator logCalculator, String id, double p) {
        if ((id == null) || (id.equals("")) || (p <= 0) || (p > 1) ||
                (logCalculator == null)) {
            throw new IllegalArgumentException();
        }

        this.logCalculator = logCalculator;
        this.id = id;
        this.p = p;
    }

    public String getId() {
        return id;
    }

    public double getP() {
        return p;
    }

    public DCF merge(DCF dcf) {
        String newID = id + "," + dcf.id;
        double newP = p + dcf.p;

        List<String> allAttributes =
                DCFUtil.getAllAttributes(attributes.keySet(), dcf.attributes.keySet());

        Map<String, Double> newAttrValues = new HashMap<>();

        Iterator<String> itAttr = allAttributes.iterator();
        Double firstValue, secondValue, newValue;
        String auxAttr;

        while (itAttr.hasNext()) {
            auxAttr = itAttr.next();

            firstValue = this.getAttrValue(auxAttr);
            secondValue = dcf.getAttrValue(auxAttr);

            newValue = (p / newP) * firstValue + (dcf.p / newP) * secondValue;

            newAttrValues.put(auxAttr, newValue);
        }

        return new DCF(logCalculator, newID, newP, newAttrValues);
    }

    public double getInformationLoss(DCF dcf) {
        if (dcf == null) {
            throw new IllegalArgumentException();
        }

        List<String> allAttrs = DCFUtil.getAllAttributes(attributes.keySet(), dcf.attributes.keySet());

        return (this.p + dcf.p) * DCFUtil.jensenShannonDivergence(
                logCalculator, allAttrs, this, dcf);
    }

    public double getKullbackLeiblerDivergence(DCF dcf) {
        if (dcf == null) {
            throw new IllegalArgumentException();
        }

        KullbackLeiblerDivergence dKL = new KullbackLeiblerDivergence();
        List<String> allAttrs =
                DCFUtil.getAllAttributes(attributes.keySet(), dcf.attributes.keySet());
        double pCStar = this.p + dcf.p;

        return dKL.compute(logCalculator, allAttrs, this, dcf, pCStar);
    }

    public Collection<String> getAttrNames() {
        return attributes.keySet();
    }

    public double getAttrValue(String attr) {
        if (attributes == null) {
            throw new IllegalStateException();
        }

        Double result = attributes.get(attr);

        if (result == null) {
            result = 0d;
        }

        return result;
    }

    @Override
    public String toString() {
        String string = "";

        string += id + " p=" + p + " ";
        string += attributes.toString();

        return string;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DCF) {
            return ((DCF) obj).id.equals(id);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 1;

        hash = hash * 31 + id.hashCode();

        return hash;
    }

    @Override
    public DCF clone() throws CloneNotSupportedException {
        DCF clone = (DCF) super.clone();

        clone.id = id;
        clone.p = p;
        clone.attributes = new HashMap<>(attributes);
        clone.logCalculator = logCalculator;

        return clone;
    }

    private void initAttributes(Collection<Edge> dependencies) {
        float sum = 0;
        Edge auxEdge;
        Iterator<Edge> depIt = dependencies.iterator();
        attributes = new HashMap<>();

        while (depIt.hasNext()) {
            sum += depIt.next().getValue();
        }

        depIt = dependencies.iterator();

        while (depIt.hasNext()) {
            auxEdge = depIt.next();
            attributes.put(String.valueOf(auxEdge.getRightNode()),
                    (double) (auxEdge.getValue() / sum));
        }
    }

    private void initAttributes(Collection<Edge> dependencies,
                                Map<String, Double> packageNames) {
        float sum = 0;
        Iterator<Edge> depIt = dependencies.iterator();
        attributes = new HashMap<>();

        while (depIt.hasNext()) {
            sum += depIt.next().getValue();
        }

        Set<Map.Entry<String, Double>> packageNamesKeys =
                packageNames.entrySet();
        Iterator<Map.Entry<String, Double>> itKeys =
                packageNamesKeys.iterator();

        while (itKeys.hasNext()) {
            sum += itKeys.next().getValue();
        }

        Edge auxEdge;
        depIt = dependencies.iterator();

        while (depIt.hasNext()) {
            auxEdge = depIt.next();
            attributes.put(String.valueOf(auxEdge.getRightNode()),
                    (double) (auxEdge.getValue() / sum));
        }

        itKeys = packageNamesKeys.iterator();
        Map.Entry<String, Double> currentEntry;

        while (itKeys.hasNext()) {
            currentEntry = itKeys.next();
            attributes.put(currentEntry.getKey(),
                    currentEntry.getValue() / sum);
        }
    }
}
