package clusterFinder.limbo.dcf;

public class DCFFindIndexInfo {
    private int index;
    private DCF dcf;
    private double infoLoss;

    public DCFFindIndexInfo(int index, DCF dcf, double infoLoss) {
        this.infoLoss = infoLoss;
        this.index = index;
        this.dcf = dcf;
    }

    public int getFirstIndex() {
        return index;
    }

    public DCF getFirstDCF() {
        return dcf;
    }

    public double getInfoLoss() {
        return infoLoss;
    }
}
