package clusterFinder.limbo.dcf;

import clusterFinder.limbo.util.LogCalculator;
import sysmodel.util.DGreaterOrEqualCondition;
import sysmodel.util.DSmallerOrEqualCondition;
import sysmodel.util.DoubleCondition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class DCFUtil {
    public static DCFFindIndexInfo findClosestIndex(List<DCF> dcfList, DCF referenceDCF) {
        if ((dcfList == null) || (dcfList.size() == 0) ||
                (referenceDCF == null)) {
            throw new IllegalArgumentException();
        }

        int index = 0;
        DCF resultDCF = dcfList.get(0);
        double minInfoLoss = referenceDCF.getInformationLoss(resultDCF),
                auxInfoLoss;

        for (int i = 0; i < dcfList.size(); i++) {
            auxInfoLoss = referenceDCF.getInformationLoss(dcfList.get(i));

            if (auxInfoLoss < minInfoLoss) {
                minInfoLoss = auxInfoLoss;
                index = i;
                resultDCF = dcfList.get(i);
            }
        }

        DCFFindIndexInfo result = new DCFFindIndexInfo(index, resultDCF, minInfoLoss);

        return result;
    }

    public static DCFFindIndexesInfo findClosestIndexes(List<DCF> dcfs) {
        return findIndexes(dcfs, Double.MAX_VALUE, new DSmallerOrEqualCondition());
    }

    public static DCFFindIndexesInfo findFarthestIndexes(List<DCF> dcfs) {
        return findIndexes(dcfs, -1, new DGreaterOrEqualCondition());
    }

    private static DCFFindIndexesInfo findIndexes(List<DCF> dcfs,
                                                  double startValue, DoubleCondition condition) {
        if (dcfs.size() < 2) {
            throw new IllegalArgumentException();
        }

        int firstIndex = 0,
                secondIndex = 0,
                i, j;

        DCF firstDCF = null,
                secondDCF = null;

        double minInfoLoss = startValue,
                auxMinInfoLoss;

        for (i = 0; i < (dcfs.size() - 1); i++) {
            for (j = i + 1; j < dcfs.size(); j++) {
                auxMinInfoLoss = dcfs.get(i).getInformationLoss(
                        dcfs.get(j));

                if (condition.meetsCondition(auxMinInfoLoss, minInfoLoss)) {
                    minInfoLoss = auxMinInfoLoss;

                    firstIndex = i;
                    secondIndex = j;

                    firstDCF = dcfs.get(i);
                    secondDCF = dcfs.get(j);
                }
            }
        }

        if ((firstIndex == 0) && (secondIndex == 0)) {
            throw new IllegalStateException();
        }

        DCFFindIndexesInfo result =
                new DCFFindIndexesInfo(firstIndex, firstDCF, minInfoLoss,
                        secondIndex, secondDCF);

        return result;
    }

    public static List<String> getAllAttributes(Collection<String> firstSet,
                                                Collection<String> secondSet) {
        List<String> result = new ArrayList<>(firstSet);

        Iterator<String> it = secondSet.iterator();
        String auxAttr;

        while (it.hasNext()) {
            auxAttr = it.next();

            if (!result.contains(auxAttr)) {
                result.add(auxAttr);
            }
        }

        return result;
    }

    public static List<String> getAllAttributes(Collection<DCF> dcfs) {
        List<String> result = new ArrayList<>();

        if (dcfs.size() >= 1) {
            Iterator<DCF> itDCFs = dcfs.iterator();
            result.addAll(itDCFs.next().getAttrNames());

            while (itDCFs.hasNext()) {
                result = getAllAttributes(result, itDCFs.next().getAttrNames());
            }
        }

        return result;
    }

    public static double jensenShannonDivergence(LogCalculator logCalculator,
                                                 List<String> allAttrs, DCF dcfX, DCF dcfY) {
        double pCStar = dcfX.getP() + dcfY.getP();

        KullbackLeiblerDivergenceWithPBar dKL = new KullbackLeiblerDivergenceWithPBar();

        return (dcfX.getP() / pCStar) * dKL.compute(logCalculator, allAttrs, dcfX, dcfY, pCStar) +
                (dcfY.getP() / pCStar) * dKL.compute(logCalculator, allAttrs, dcfY, dcfX, pCStar);
    }
}
