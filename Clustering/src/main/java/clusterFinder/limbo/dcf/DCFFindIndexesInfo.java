package clusterFinder.limbo.dcf;

public class DCFFindIndexesInfo extends DCFFindIndexInfo {
    private int secondIndex;
    private DCF secondDCF;

    public DCFFindIndexesInfo(int firstIndex, DCF firstDCF, double infoLoss,
                              int secondIndex, DCF secondDCF) {
        super(firstIndex, firstDCF, infoLoss);
        this.secondIndex = secondIndex;
        this.secondDCF = secondDCF;
    }

    public int getSecondIndex() {
        return secondIndex;
    }

    public DCF getSecondDCF() {
        return secondDCF;
    }
}
