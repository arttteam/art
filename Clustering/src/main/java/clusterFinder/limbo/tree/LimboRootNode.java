package clusterFinder.limbo.tree;

import java.util.List;

public class LimboRootNode extends LimboMiddleNode {
    public LimboRootNode(int maxChildNum, List<LimboNode> children) {
        super(maxChildNum, children);
    }

    @Override
    protected void computeOwnDFC() {
        //Do not waste time computing ownDCF for root node, it will be never used
        resetDCF();
    }

    @Override
    public String toString() {
        return "root node " + getChildrenSize() + "\n" + childrenToString();
    }
}
