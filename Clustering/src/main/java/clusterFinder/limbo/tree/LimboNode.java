package clusterFinder.limbo.tree;

import clusterFinder.limbo.dcf.DCF;

import java.util.List;

public interface LimboNode {
    public abstract DCF getDCF();

    public abstract List<DCF> getDCFs();

    public abstract boolean addDCF(DCF dcf);

    public abstract List<LimboNode> split();

    public abstract int getMaxChildNum();
}
