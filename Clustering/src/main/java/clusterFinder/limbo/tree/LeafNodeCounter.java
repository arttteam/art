package clusterFinder.limbo.tree;

public class LeafNodeCounter {
    private int maxNumber;
    private int currentNumber = 0;

    public LeafNodeCounter(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public void increment() {
        if (currentNumber == maxNumber) {
            throw new IndexOutOfBoundsException("Max number of nodes reached");
        }

        currentNumber += 1;
    }

    public boolean maximumNumberReached() {
        if (currentNumber == maxNumber) {
            //System.out.println(currentNumber);

            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return currentNumber + " nodes";
    }
}
