package clusterFinder.limbo.tree;

import clusterFinder.limbo.dcf.DCF;

import java.util.List;

public class LimboTree {
    private LimboNode rootNode;

    public LimboTree(int maxChildNum, int maxLeafNodeNr, double mergeInfoLossThreshold) {
        if ((maxChildNum < 1) || (maxLeafNodeNr < 0)) {
            throw new IllegalArgumentException();
        }

        LeafNodeCounter counter = new LeafNodeCounter(maxLeafNodeNr);

        rootNode = new LimboLeafNode(maxChildNum, mergeInfoLossThreshold, counter);
    }

    public void addDCF(DCF dcf) {
        if (rootNode.addDCF(dcf)) {
            List<LimboNode> children = rootNode.split();

            rootNode = new LimboRootNode(rootNode.getMaxChildNum(), children);
        }
    }

    public List<DCF> getDCFs() {
        return rootNode.getDCFs();
    }

    public String toString() {
        return rootNode.toString();
    }
}
