package clusterFinder.limbo.tree;

import clusterFinder.limbo.dcf.DCF;
import clusterFinder.limbo.dcf.DCFFindIndexInfo;
import clusterFinder.limbo.dcf.DCFFindIndexesInfo;
import clusterFinder.limbo.dcf.DCFUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class LimboLeafNode implements LimboNode {
    private int maxChildNum;
    private List<DCF> ownDCFs;
    private DCF ownDCF;
    private double mergeInfoLossThreshold = 0;
    private LeafNodeCounter counter;

    protected LimboLeafNode(int maxChildNum) {
        if (maxChildNum < 1) {
            throw new IllegalArgumentException();
        }

        this.maxChildNum = maxChildNum;
        ownDCFs = new ArrayList<>();
        counter = null;
    }

    public LimboLeafNode(int maxChildNum,
                         double mergeInfoLossThreshold, LeafNodeCounter counter) {
        this(maxChildNum);
        this.mergeInfoLossThreshold = mergeInfoLossThreshold;
        this.counter = counter;
    }

    public LimboLeafNode(int maxChildNum, double mergeInfoLossThreshold,
                         LeafNodeCounter counter, List<DCF> dcfs) {
        this(maxChildNum, mergeInfoLossThreshold, counter);

        ownDCFs.addAll(dcfs);
        computeOwnDFC();
    }

    @Override
    public DCF getDCF() {
        return ownDCF;
    }

    @Override
    public List<DCF> getDCFs() {
        return ownDCFs;
    }

    @Override
    public List<LimboNode> split() {
        List<List<DCF>> dcfAuxList = splitDCFs();

        final List<LimboNode> result = dcfAuxList
                                    .stream()
                                    .map(aDcfAuxList -> new LimboLeafNode(getMaxChildNum(),
                                                                        mergeInfoLossThreshold,
                                                                        counter,
                                                                        aDcfAuxList))
                                    .collect(Collectors.toList());

        counter.increment();

        return result;
    }

    @Override
    public boolean addDCF(DCF dcf) {
        if (ownDCFs.size() > 0) {


            if ((counter != null) && (counter.maximumNumberReached())) {
                ownDCFs.add(dcf);
                DCFFindIndexesInfo info = DCFUtil.findClosestIndexes(getOwnDcfs());
                DCF firstDCF = ownDCFs.get(info.getFirstIndex()),
                        secondDCF = ownDCFs.get(info.getSecondIndex());

                ownDCFs.remove(firstDCF);
                ownDCFs.remove(secondDCF);
                ownDCFs.add(firstDCF.merge(secondDCF));
            } else {
                DCFFindIndexInfo info = DCFUtil.findClosestIndex(getOwnDcfs(), dcf);

                if (info.getInfoLoss() < mergeInfoLossThreshold) {
                    int index = info.getFirstIndex();
                    ownDCFs.remove(index);
                    ownDCFs.add(index, dcf.merge(info.getFirstDCF()));
                } else {
                    ownDCFs.add(dcf);
                }
            }
        } else {
            ownDCFs.add(dcf);
        }

        computeOwnDFC();

        if (ownDCFs.size() > getMaxChildNum()) {
            return true;
        }

        return false;
    }

    @Override
    public int getMaxChildNum() {
        return maxChildNum;
    }

    protected List<List<DCF>> splitDCFs() {
        List<DCF> firstPart = new ArrayList<>();
        List<DCF> secondPart = new ArrayList<>();

        if (ownDCFs.size() == 0) {
            throw new IllegalStateException();
        }

        DCFFindIndexesInfo info = DCFUtil.findFarthestIndexes(ownDCFs);

        DCF firstSeed = info.getFirstDCF(),
                secondSeed = info.getSecondDCF(),
                auxDCF;
        double firstInfoLoss,
                secondInfoLoss;
        Iterator<DCF> itDCFs = ownDCFs.iterator();

        firstPart.add(firstSeed);
        secondPart.add(secondSeed);

        while (itDCFs.hasNext()) {
            auxDCF = itDCFs.next();

            if ((!auxDCF.equals(firstSeed)) &&
                    (!auxDCF.equals(secondSeed))) {
                firstInfoLoss = auxDCF.getInformationLoss(firstSeed);
                secondInfoLoss = auxDCF.getInformationLoss(secondSeed);

                if (firstInfoLoss <= secondInfoLoss) {
                    firstPart.add(auxDCF);
                } else {
                    secondPart.add(auxDCF);
                }
            }
        }

        List<List<DCF>> result = new ArrayList<>();

        result.add(firstPart);
        result.add(secondPart);

        return result;
    }

    protected List<DCF> getOwnDcfs() {
        return new ArrayList<>(ownDCFs);
    }

    protected void resetDCF() {
        ownDCF = null;
    }

    protected void removeOwnDcfAt(int index) {
        ownDCFs.remove(index);

        computeOwnDFC();
    }

    protected void addOwnDcf(DCF dcf) {
        ownDCFs.add(dcf);

        computeOwnDFC();
    }

    protected void addOwnDcfs(List<DCF> dcfs) {
        ownDCFs.addAll(dcfs);

        computeOwnDFC();
    }

    protected void addOwnDcfAt(int index, DCF dcf) {
        ownDCFs.add(index, dcf);

        computeOwnDFC();
    }

    protected int getOwnDcfIndex(DCF dcf) {
        return ownDCFs.indexOf(dcf);
    }

    protected int getOwnDcfSize() {
        return ownDCFs.size();
    }

    protected void computeOwnDFC() {
        if ((ownDCFs == null) || (ownDCFs.size() == 0)) {
            ownDCF = null;
            return;
        }

        Iterator<DCF> it = ownDCFs.iterator();
        DCF auxDCF;

        auxDCF = it.next();

        while (it.hasNext()) {
            auxDCF = auxDCF.merge(it.next());
        }

        ownDCF = auxDCF;
    }

    @Override
    public String toString() {
        return "leaf node " + getDCF().toString() + "\n" + ownDCFs.toString() + "\n";
    }
}
