package clusterFinder.limbo.tree;

import clusterFinder.limbo.dcf.DCF;
import clusterFinder.limbo.dcf.DCFUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class LimboMiddleNode extends LimboLeafNode {
    private List<LimboNode> childNodes;

    public LimboMiddleNode(int maxChildNum, List<LimboNode> childNodes) {
        super(maxChildNum);

        if (childNodes.size() == 0) {
            throw new IllegalArgumentException();
        }

        this.childNodes = childNodes;

        Iterator<LimboNode> itChild = this.childNodes.iterator();
        List<DCF> dcfs = new ArrayList<>();

        while (itChild.hasNext()) {
            dcfs.add(itChild.next().getDCF());
        }

        addOwnDcfs(dcfs);
    }

    @Override
    public boolean addDCF(DCF dcf) {
        if ((childNodes.size() == 0) || (childNodes.size() != getOwnDcfSize())) {
            throw new IllegalStateException();
        }
        int index = DCFUtil.findClosestIndex(getOwnDcfs(), dcf).getFirstIndex();
        LimboNode nodeToAddTo = childNodes.get(index);
        removeOwnDcfAt(index);

        if (nodeToAddTo.addDCF(dcf)) {
            List<LimboNode> auxList = nodeToAddTo.split();

            ListIterator<LimboNode> it = auxList.listIterator(auxList.size());
            LimboNode nodeToBeAdded;

            childNodes.remove(index);

            while (it.hasPrevious()) {
                nodeToBeAdded = it.previous();

                childNodes.add(index, nodeToBeAdded);
                addOwnDcfAt(index, nodeToBeAdded.getDCF());
            }
        } else {
            addOwnDcfAt(index, nodeToAddTo.getDCF());
        }

        if (childNodes.size() > getMaxChildNum()) {
            return true;
        }

        return false;
    }

    @Override
    public List<DCF> getDCFs() {
        return childNodes
                .stream()
                .map(LimboNode::getDCFs)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Override
    public List<LimboNode> split() {
        List<LimboNode> result = new ArrayList<>();

        List<List<DCF>> splitedDCFs = splitDCFs();
        Iterator<List<DCF>> itSplitedDCFs = splitedDCFs.iterator();
        Iterator<DCF> itDCFs;

        List<LimboNode> auxChildren;

        while (itSplitedDCFs.hasNext()) {
            itDCFs = itSplitedDCFs.next().iterator();
            auxChildren = new ArrayList<>();

            while (itDCFs.hasNext()) {
                auxChildren.add(childNodes.get(getOwnDcfIndex(itDCFs.next())));
            }

            result.add(new LimboMiddleNode(getMaxChildNum(), auxChildren));
        }

        return result;
    }

    protected int getChildrenSize() {
        return childNodes.size();
    }

    protected String childrenToString() {
        return childNodes.toString();
    }

    @Override
    public String toString() {
        return "middle node " + childNodes.size() + " " + getDCF().toString() + "\n" + childNodes.toString();
    }
}