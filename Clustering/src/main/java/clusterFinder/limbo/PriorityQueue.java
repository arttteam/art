package clusterFinder.limbo;

import clusterFinder.limbo.dcf.DCF;
import clusterFinder.limbo.dcf.DCFInfoLossDistance;

import java.util.*;
import java.util.stream.Collectors;

public class PriorityQueue {
    private SortedSet<DCFInfoLossDistance> distances;

    public PriorityQueue(List<DCF> dcfs) {
        if (dcfs == null) {
            throw new NullPointerException();
        }

        int n = dcfs.size(),
                i, j;

        distances = new TreeSet<>();

        for (i = 0; i < (n - 1); i++) {
            for (j = i + 1; j < n; j++) {
                distances.add(new DCFInfoLossDistance(dcfs.get(i), dcfs.get(j)));
            }
        }
    }

    public DCFInfoLossDistance getFirst() {
        Iterator<DCFInfoLossDistance> it = distances.iterator();

        if (it.hasNext())
            return it.next();

        return null;
    }

    public void removeAll(DCF dcf) {
        List<DCFInfoLossDistance> toRemove = new ArrayList<>();
        Iterator<DCFInfoLossDistance> it = distances.iterator();
        DCFInfoLossDistance currentDistance;
        while (it.hasNext()) {
            currentDistance = it.next();
            if (currentDistance.hasDCF(dcf)) {
                toRemove.add(currentDistance);
            }
        }

        distances.removeAll(toRemove);
    }

    public void addAll(DCF dcf, List<DCF> dcfs) {
        distances.addAll(
                dcfs
                    .stream()
                    .map(dcf1 -> new DCFInfoLossDistance(dcf, dcf1))
                    .collect(Collectors.toList()));
    }
}
