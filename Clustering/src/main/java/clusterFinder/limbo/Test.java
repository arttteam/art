package clusterFinder.limbo;

import clusterFinder.limbo.dcf.DCF;
import clusterFinder.limbo.util.LogCalculator;
import sysmodel.edge.Edge;

import java.util.ArrayList;
import java.util.List;

public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Edge e1 = new Edge(1, 2, 1);
        Edge e11 = new Edge(2, 1, 1);
        Edge e2 = new Edge(1, 3, 1);
        Edge e21 = new Edge(3, 1, 1);
        Edge e3 = new Edge(1, 4, 1);
        Edge e31 = new Edge(4, 1, 1);
        Edge e4 = new Edge(2, 4, 1);
        Edge e41 = new Edge(4, 2, 1);
        Edge e5 = new Edge(3, 4, 1);
        Edge e51 = new Edge(4, 3, 1);

        List<Edge> l1 = new ArrayList<>();
        l1.add(e1);
        l1.add(e2);
        l1.add(e3);
        List<Edge> l2 = new ArrayList<>();
        l2.add(e11);
        l2.add(e4);
        List<Edge> l3 = new ArrayList<>();
        l3.add(e21);
        l3.add(e5);
        List<Edge> l4 = new ArrayList<>();
        l4.add(e31);
        l4.add(e41);
        l4.add(e51);

        DCF dcf1 = new DCF(new LogCalculator(2, 1022), "1", 0.25, l1);
        DCF dcf2 = new DCF(new LogCalculator(2, 1022), "2", 0.25, l2);
        DCF dcf3 = new DCF(new LogCalculator(2, 1022), "3", 0.25, l3);
        DCF dcf4 = new DCF(new LogCalculator(2, 1022), "4", 0.25, l4);

        System.out.println(dcf1.getInformationLoss(dcf2));
        System.out.println(dcf1.getInformationLoss(dcf3));
        System.out.println(dcf1.getInformationLoss(dcf4));
        System.out.println(dcf2.getInformationLoss(dcf3));
        System.out.println(dcf2.getInformationLoss(dcf4));
        System.out.println(dcf3.getInformationLoss(dcf4));
    }

}
