package clusterFinder.limbo.util;

public class LogCalculator {
    private double logBase = 2;
    private double zeroPenalization = 6;

    public LogCalculator(double logBase, double zeroPenalization) {
        super();
        this.logBase = logBase;
        this.zeroPenalization = zeroPenalization;
    }

    public double log(double value) {
        if (value != 0) {
            return Math.log(value) / Math.log(logBase);
        } else {
            return (-1) * zeroPenalization;
        }
    }

    public double getZeroPenalization() {
        return zeroPenalization;
    }
}
