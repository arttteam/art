package clusterFinder;

import clusterFinder.postProcess.PostProcessor;
import clusterFinder.preProcess.PreProcessor;
import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public abstract class ClusterFinder {
    private PreProcessor preProcessor;
    private PostProcessor postProcessor;

    public ClusterFinder(PreProcessor preProcessor, PostProcessor postProcessor) {
        this.preProcessor = preProcessor;
        this.postProcessor = postProcessor;
    }

    public List<? extends Set<Integer>> findClusters(DSM dsm) {
        SparceMatrix<Integer> matrix = dsm.getDependencyMatrix();

        Collection<Edge> edges = computeDependecyEdges(matrix);

        Collection<Edge> processedEdges = preProcess(edges, dsm);

        List<? extends Set<Integer>> clusters =
                computeClusters(dsm, new TreeSet<>(processedEdges));

        return postProcess(clusters, dsm, processedEdges);
    }

    public String toString() {
        return preProcessor.toString() + "_" + postProcessor.toString();
    }

    protected abstract List<? extends Set<Integer>> computeClusters(DSM initialDSM,
                                                                    Collection<Edge> processedEdges);

    protected abstract Collection<Edge> computeDependecyEdges(SparceMatrix<Integer> matrix);

    private Collection<Edge> preProcess(Collection<Edge> edges, DSM initialDSM) {
        return preProcessor.process(edges, initialDSM);
    }

    private List<? extends Set<Integer>> postProcess(
            List<? extends Set<Integer>> clusters, DSM initialDSM,
            Collection<Edge> processedEdges) {
        return postProcessor.process(clusters, initialDSM, processedEdges);
    }
}
