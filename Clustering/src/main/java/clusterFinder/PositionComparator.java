package clusterFinder;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

public class PositionComparator implements Comparator<Set<Integer>>, Serializable {
    private static final long serialVersionUID = 4478571059479439442L;

    private int[] levels;

    public PositionComparator(int[] levels) {
        super();
        this.levels = levels;
    }


    private float midLevel(Set<Integer> s) {
        int sum = 0;
        int nr = 0;
        for (Integer i : s) {
            sum += levels[i];
            nr++;
        }
        return ((float) sum) / nr;
    }


    @Override
    public int compare(Set<Integer> o1, Set<Integer> o2) {
        float c = midLevel(o1) - midLevel(o2);
        return (int) Math.signum(c);
    }
}
