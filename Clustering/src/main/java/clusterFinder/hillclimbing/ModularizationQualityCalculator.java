/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clusterFinder.hillclimbing;

import dataFormat.QualityCluster;
import dataFormat.QualityElement;
import dataFormat.QualityPartition;
import sysmodel.SparceMatrix;

import java.util.Set;

/**
 * @author raptor
 */

public class ModularizationQualityCalculator {

    private SparceMatrix<Float> matrix;

    //private static int cnt = 0;
    private double xin = 0, yin = 0, xout = 0, yout = 0;

    public ModularizationQualityCalculator(SparceMatrix<Float> matrix) {

        this.matrix = matrix;
    }

    public double getModularizationQuality(QualityPartition partition) {

        double MQ = 0;
        double CF = 0;

        for (int i = 0; i < partition.getClusterNumber(); i++) {

            CF = this.getNewClusterFactor(partition.getClusterByIndex(i), null, null);
            MQ += CF;
        }

        //System.out.println(cnt + " " + MQ);
        //cnt++;
        return MQ;
    }

    public double getNewClusterFactor(QualityCluster cluster, QualityElement in, QualityElement out) {

        double intraEdges = 0;
        double interEdges = 0;
        double CF = 0;

        this.xin = 0;
        this.yin = 0;
        this.xout = 0;
        this.yout = 0;

        if (in == null && out == null) {

            this.xin = 0;
            this.yin = 0;
            for (int i = 0; i < cluster.getSize(); i++) {

                this.calculateEdgesRow(cluster.getElementByIndex(i), cluster, true);
            }
            intraEdges += this.xin;
            interEdges += this.yin;

            this.xin = 0;
            this.yin = 0;
            for (int i = 0; i < cluster.getSize(); i++) {

                this.calculateEdgesCol(cluster.getElementByIndex(i), cluster, true);
            }
            interEdges += this.yin;
            cluster.setCF(intraEdges, interEdges);
        }

        this.xin = 0;
        this.yin = 0;
        this.calculateEdgesRow(in, cluster, true);
        this.calculateEdgesCol(in, cluster, true);

        this.xout = 0;
        this.yout = 0;
        this.calculateEdgesRow(out, cluster, false);
        this.calculateEdgesCol(out, cluster, false);

        intraEdges = cluster.getIntraEdges();
        interEdges = cluster.getInterEdges();

        intraEdges = intraEdges + this.xin - this.xout;
        interEdges = interEdges - this.xin + this.yin + this.xout - this.yout;

        cluster.setPendingEdges(intraEdges, interEdges);

        CF = ModularizationQualityCalculator.calculateCF(intraEdges, interEdges);

        return CF;
    }

    private void calculateEdgesRow(QualityElement element, QualityCluster cluster, boolean in) {

        if (element != null) {
            Set<Integer> connectedToRow = matrix.getConnectedToRow(Integer.valueOf(element.getClassName()));
            if (connectedToRow != null) {
                for (Integer connectedClass : connectedToRow) {
                    if (this.isClusterMember(cluster, connectedClass)) {
                        if (in == true) {
                            this.xin += matrix.getElement(Integer.valueOf(element.getClassName()), connectedClass);
                        } else {
                            this.xout += matrix.getElement(Integer.valueOf(element.getClassName()), connectedClass);
                        }
                    } else {
                        if (in == true) {
                            this.yin += matrix.getElement(Integer.valueOf(element.getClassName()), connectedClass);
                        } else {
                            this.yout += matrix.getElement(Integer.valueOf(element.getClassName()), connectedClass);
                        }
                    }
                }
            }
        }
    }

    private void calculateEdgesCol(QualityElement element, QualityCluster cluster, boolean in) {

        if (element != null) {
            Set<Integer> connectedToCol = matrix.getConnectedToColumns(Integer.valueOf(element.getClassName()));
            if (connectedToCol != null) {
                for (Integer connectedClass : connectedToCol) {
                    if (this.isClusterMember(cluster, connectedClass)) {
                        if (in == true) {
                            this.xin += matrix.getElement(connectedClass, Integer.valueOf(element.getClassName()));
                        } else {
                            this.xout += matrix.getElement(connectedClass, Integer.valueOf(element.getClassName()));
                        }
                    } else {
                        if (in == true) {
                            this.yin += matrix.getElement(connectedClass, Integer.valueOf(element.getClassName()));
                        } else {
                            this.yout += matrix.getElement(connectedClass, Integer.valueOf(element.getClassName()));
                        }
                    }
                }
            }
        }
    }

    private boolean isClusterMember(QualityCluster cluster, int name) {

        for (int i = 0; i < cluster.getSize(); i++) {
            if (Integer.valueOf(cluster.getElementByIndex(i).getClassName()) == name)
                return true;
        }
        return false;
    }

    public static double calculateCF(double intraEdges, double interEdges) {

        double CF;

        if (intraEdges + interEdges == 0) {
            CF = 0;
        } else {

            CF = (Math.pow(intraEdges, 1.95) / 1000) / (intraEdges + Math.pow(interEdges, 1.5) / 2000);
            //CF = intraEdges/(intraEdges + interEdges/2);
        }

        return CF;
    }

}
