/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clusterFinder.hillclimbing;

/**
 * @author raptor
 */
public interface OptimizationMethod {

    boolean acceptPartition(double initialMQ, double newMQ);

    void init();

}
