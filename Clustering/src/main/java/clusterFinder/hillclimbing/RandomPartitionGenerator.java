/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clusterFinder.hillclimbing;

import dataFormat.Partition;
import sysmodel.SparceMatrix;

/**
 * @author raptor
 */
public interface RandomPartitionGenerator {

    public Partition generateRandomPartition(SparceMatrix<Float> matrix);

}
