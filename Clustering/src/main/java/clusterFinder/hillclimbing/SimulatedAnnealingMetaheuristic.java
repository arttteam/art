/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clusterFinder.hillclimbing;

/**
 * @author raptor
 */
public class SimulatedAnnealingMetaheuristic implements OptimizationMethod {

    private double startTemp = HillClimbingClusterFinder.INIT_START_TEMP;
    private double startTemperature = HillClimbingClusterFinder.INIT_START_TEMP;
    private double coolingFactor = HillClimbingClusterFinder.INIT_COOLING_FACTOR;

    public SimulatedAnnealingMetaheuristic(double startTemp, double coolingFactor) {

        this.startTemp = startTemp;
        this.startTemperature = startTemp;
        this.coolingFactor = coolingFactor;
    }

    public boolean acceptPartition(double initialMQ, double newMQ) {

        boolean accept = false;
        double probability;

        if ((newMQ - initialMQ) >= 0 || this.startTemp == 0) {

            //this.startTemp = this.startTemp * this.coolingFactor;
            probability = 0;
        } else {
            //this.startTemp = this.startTemp * this.coolingFactor;
            probability = Math.pow(Math.E, (newMQ - initialMQ) / this.startTemp);
        }

        //this.startTemp = this.startTemp * this.coolingFactor;

        if (Math.random() < probability) {
            this.startTemp = this.startTemp * this.coolingFactor;
            accept = true;
        }

        return accept;
    }

    public void init() {

        this.startTemp = this.startTemperature;
    }

    @Override
    public String toString() {

        String result = "_CF_" + this.coolingFactor + "_ST_" + this.startTemperature;

        return result;
    }
}
