/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clusterFinder.hillclimbing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author raptor
 */
public class ResultsSaver {

    private BufferedWriter out;
    private int stPart;
    private double neibPer;
    private double temp;
    private double cool;
    private static int counter = 0;

    public ResultsSaver(String filename, int stPart, double neibPer, double temp, double cool) {

        this.stPart = stPart;
        this.neibPer = neibPer;
        this.temp = temp;
        this.cool = cool;

        FileWriter fstream;
        try {
            fstream = new FileWriter(filename, true);
            this.out = new BufferedWriter(fstream);
            if (ResultsSaver.counter == 0) {
                this.out.write("SP\tNP\tTemp\tCool\tMQ\t\t\tevals\tsteps\ttime\n");
                ResultsSaver.counter = 1;
            }
        } catch (IOException ex) {
            Logger.getLogger(ResultsSaver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void writeToFile(double MQ, int MQevals, int steps, double time) {
        try {
            this.out.write(stPart + "\t" + neibPer + "\t" + temp + "\t" + cool + "\t" +
                    MQ + "\t" + MQevals + "\t" + steps + "\t" + time + "\n");
        } catch (IOException ex) {
            Logger.getLogger(ResultsSaver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeFile() {
        try {
            this.out.close();
        } catch (IOException ex) {
            Logger.getLogger(ResultsSaver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
