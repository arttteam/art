/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clusterFinder.hillclimbing;

import dataFormat.Cluster;
import dataFormat.Partition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * @author raptor
 */
public class MovesCreator {

    private ArrayList<ArrayList<Integer>> allMoves;
    private ArrayList<ArrayList<ArrayList<Integer>>> allMovesArray;

    private Iterator<ArrayList<Integer>> iterator;

    public MovesCreator() {

        this.allMoves = new ArrayList<>();
        this.allMovesArray = new ArrayList<>();
    }

    public void createAllMoves(Partition initialPartition) {

        this.createPossibleMoves(initialPartition, 0, 0, true);
    }

    public void createNewMoves(Partition initialPartition, int fromCls, int toCls) {

        this.createPossibleMoves(initialPartition, fromCls, toCls, false);
    }

    private void createPossibleMoves(Partition initialPartition, int fromCls, int toCls, boolean all) {

        this.allMoves = new ArrayList<>();
        int clsIdx = 0;
        ArrayList<Integer> currentMove;
        ArrayList<ArrayList<Integer>> aux;

        if (all == true) {
            this.allMovesArray = new ArrayList<>();
        }

        for (Cluster cls : initialPartition.getClusters()) {
            if (all == true || clsIdx == fromCls || clsIdx == toCls) {
                aux = new ArrayList<>();
                for (int i = 0; i < initialPartition.getClusterNumber() + 1; i++) {
                    for (int j = 0; j < cls.getSize(); j++) {

                        if (clsIdx != i) {
                            currentMove = new ArrayList<>();
                            currentMove.add(clsIdx);
                            currentMove.add(i);
                            currentMove.add(j);
                            aux.add(currentMove);
                        }
                    }
                }
                if (all == true) {

                    this.allMovesArray.add(aux);
                } else {

                    this.allMovesArray.set(clsIdx, aux);
                }
            }
            clsIdx++;
        }

        for (ArrayList<ArrayList<Integer>> lst : this.allMovesArray) {
            this.allMoves.addAll(lst);
        }

        this.iterator = this.allMoves.iterator();
    }

    public void shuffleMoves() {

        Collections.shuffle(this.allMoves);

        this.iterator = this.allMoves.iterator();
    }

    public ArrayList<Integer> getNextMove() {

        if (this.iterator == null) {
            this.iterator = this.allMoves.iterator();
        }

        if (this.iterator.hasNext()) {

            return this.iterator.next();
        } else {

            return null;
        }
    }
}
