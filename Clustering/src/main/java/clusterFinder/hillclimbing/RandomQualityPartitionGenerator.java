/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clusterFinder.hillclimbing;

import dataFormat.Cluster;
import dataFormat.QualityCluster;
import dataFormat.QualityElement;
import dataFormat.QualityPartition;
import sysmodel.SparceMatrix;

import java.util.ArrayList;

/**
 * @author raptor
 */
public class RandomQualityPartitionGenerator implements RandomPartitionGenerator {

    private static RandomQualityPartitionGenerator instance = null;

    private RandomQualityPartitionGenerator() {

    }

    public static RandomQualityPartitionGenerator getInstance() {

        if (RandomQualityPartitionGenerator.instance == null) {
            RandomQualityPartitionGenerator.instance = new RandomQualityPartitionGenerator();
        }

        return RandomQualityPartitionGenerator.instance;
    }

    public QualityPartition generateRandomPartition(SparceMatrix<Float> matrix) {

        int nodeNumber = matrix.getRows();

        int noOfClusters = (int) (Math.random() * nodeNumber) + 1;

        ArrayList<Cluster> clusters = new ArrayList<>(noOfClusters);
        for (int k = 0; k < noOfClusters; k++) {
            clusters.add(new QualityCluster(k));
        }
        QualityPartition result = new QualityPartition(clusters);

        for (int i = 0; i < nodeNumber; i++) {

            int randCluster = (int) (Math.random() * noOfClusters);
            QualityElement currentClass = new QualityElement(String.valueOf(i), i);
            QualityCluster currentCluster = result.getClusterByIndex(randCluster);
            currentCluster.add(currentClass);
        }

        ArrayList<QualityCluster> toRemove = this.getEmptyClusters(result);

        result.getClusters().removeAll(toRemove);

        return result;
    }

    private ArrayList<QualityCluster> getEmptyClusters(QualityPartition partition) {

        ArrayList<QualityCluster> toRemove = new ArrayList<>();

        for (int i = 0; i < partition.getClusterNumber(); i++) {

            QualityCluster cluster = partition.getClusterByIndex(i);
            if (cluster.getSize() == 0)
                toRemove.add(cluster);
        }

        return toRemove;
    }

}
