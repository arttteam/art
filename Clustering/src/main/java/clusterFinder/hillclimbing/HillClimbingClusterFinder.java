/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clusterFinder.hillclimbing;

import clusterFinder.ClusterFinder;
import clusterFinder.postProcess.PostProcessor;
import clusterFinder.preProcess.PreProcessor;
import dataFormat.QualityCluster;
import dataFormat.QualityElement;
import dataFormat.QualityPartition;
import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;

import java.util.*;

/**
 * @author raptor
 */
public class HillClimbingClusterFinder extends ClusterFinder {

    private static boolean PLOT = false;

    public static final int INIT_START_PARTITIONS = 1;
    public static final double INIT_NEIGHBOUR_PERCENT = 0;
    public static final double INIT_COOLING_FACTOR = 0.99;
    public static final double INIT_START_TEMP = 0;

    private int startPartitions = INIT_START_PARTITIONS;
    private double neighboursPercent = INIT_NEIGHBOUR_PERCENT;

    private OptimizationMethod optimMethod;
    private RandomPartitionGenerator partGen;
    private ModularizationQualityCalculator MQCalculator;
    private MovesCreator moveCreator;

    private ResultsSaver saver = null;
    private int MQevals;

    private SparceMatrix<Float> processedMatrix;

    private double finalQuality;


    public HillClimbingClusterFinder(PreProcessor preProcessor, PostProcessor postProcessor,
                                     int startPart, double neighbourPercent, double coolingFactor, double startTemperature) {

        super(preProcessor, postProcessor);
        if ((startPart < 1) ||
                (neighbourPercent < 0) || (neighbourPercent > 100) ||
                (coolingFactor < 0) || (coolingFactor > 1) ||
                (startTemperature < 0)) {

            throw new IllegalArgumentException();
        }

        this.startPartitions = startPart;
        this.neighboursPercent = neighbourPercent;

        this.optimMethod = new SimulatedAnnealingMetaheuristic(startTemperature, coolingFactor);
        this.partGen = RandomQualityPartitionGenerator.getInstance();
        this.moveCreator = new MovesCreator();

        if (HillClimbingClusterFinder.PLOT == true) {
            saver = new ResultsSaver("/home/raptor/Documents/Licenta/SqlAdmin_models/results_noSA.txt",
                    startPart, neighbourPercent, startTemperature, coolingFactor);
        }

        this.finalQuality = 0;
    }

    @Override
    protected List<? extends Set<Integer>> computeClusters(DSM initialDSM, Collection<Edge> processedEdges) {

        SparceMatrix<Integer> matrix = initialDSM.getDependencyMatrix();

        this.processedMatrix = new SparceMatrix(matrix.getRows(), matrix.getColumns());

        for (Edge edge : processedEdges) {
            this.processedMatrix.putElement(edge.getLeftNode(), edge.getRightNode(), edge.getValue());
        }

        this.MQCalculator = new ModularizationQualityCalculator(this.processedMatrix);

        ArrayList<QualityPartition> initialPartitions = this.generateInitialPartitions();

        QualityPartition bestPartition = this.getFinalBestPartition(initialPartitions);

        return bestPartition.toListSet();
    }

    private ArrayList<QualityPartition> generateInitialPartitions() {

        ArrayList<QualityPartition> initialPartitions = new ArrayList<>(this.startPartitions);

        for (int i = 0; i < this.startPartitions; i++) {

            QualityPartition rand = (QualityPartition) this.partGen.generateRandomPartition(this.processedMatrix);
            if (!initialPartitions.contains(rand)) {

                initialPartitions.add(rand);
                initialPartitions.get(i).setMQ(this.MQCalculator.getModularizationQuality(initialPartitions.get(i)));

            } else {
                i--;
            }
        }

        return initialPartitions;
    }

    private QualityPartition getFinalBestPartition(ArrayList<QualityPartition> initialPartitions) {

        QualityPartition bestPartition = null;
        double bestQuality = 0;

        double prevMQ;

        int steps = 0;
        double time = 0;
        if (HillClimbingClusterFinder.PLOT == true) {
            time = System.currentTimeMillis();
            this.MQevals = 0;
        }

        for (QualityPartition p : initialPartitions) {

            int xx = 0;

            this.optimMethod.init();
            int cnt = 0;

            this.moveCreator.createAllMoves(p);

            do {
                prevMQ = p.getMQ();
                xx++;
                this.getNextBestPartition(p);

                if (p.getMQ() - prevMQ > 0 && p.getMQ() - prevMQ < 0.0000001) {
                    cnt++;
                } else {
                    cnt = 0;
                }
                if (cnt >= 100)
                    break;

            } while (p.getMQ() != prevMQ);
            double localBestMQ = p.getMQ();

            if (localBestMQ > bestQuality) {
                bestQuality = localBestMQ;
                bestPartition = p;
            }
            if (HillClimbingClusterFinder.PLOT == true) {
                steps += xx;
            }
        }
        if (HillClimbingClusterFinder.PLOT == true) {
            time = System.currentTimeMillis() - time;
            saver.writeToFile(bestQuality, this.MQevals, steps, time);
            saver.closeFile();
        }

        if (bestPartition == null) {
            bestPartition = (QualityPartition) this.partGen.generateRandomPartition(processedMatrix);
            bestPartition.setMQ(this.MQCalculator.getModularizationQuality(bestPartition));
        }

        this.finalQuality = bestPartition.getMQ();

        return bestPartition;
    }

    @Override
    protected Collection<Edge> computeDependecyEdges(
            SparceMatrix<Integer> matrix) {
        int nodeNumber = matrix.getRows();
        Collection<Edge> edges = new TreeSet<>();

        for (int i = 0; i < nodeNumber; i++) {
            Set<Integer> s = matrix.getConnectedToRow(i);

            if (s != null) {
                for (Integer j : s) {
                    edges.add(new Edge(i, j, matrix.getElement(i, j)));
                }
            }
        }

        return edges;
    }

    private void getNextBestPartition(QualityPartition initialPartition) {

        ArrayList<Integer> currentMove;
        this.moveCreator.shuffleMoves();

        int clusterNumber = initialPartition.getClusterNumber();
        int nrPart = clusterNumber * this.processedMatrix.getRows();
        int moveSource = -1;
        int moveDest = -1;
        int moveElement = -1;

        int randSource;
        int randDest;
        int randElement;

        double initialMQ = initialPartition.getMQ();
        double newMQ;
        double sourceCF = 0;
        double destCF = 0;

        double bestMQ = initialMQ;

        double pendInterEdgeSource = 0, pendInterEdgeDest = 0;
        double pendIntraEdgeSource = 0, pendIntraEdgeDest = 0;

        boolean found = false;

        for (int i = 0; (i < nrPart) && ((i < (nrPart * this.neighboursPercent) / 100) || (found == false)); i++) {

            currentMove = this.moveCreator.getNextMove();
            if (currentMove == null) {
                break;
            }
            randSource = currentMove.get(0);
            randDest = currentMove.get(1);
            randElement = currentMove.get(2);

            newMQ = initialMQ - initialPartition.getClusterByIndex(randSource).getCF();
            if (randDest < clusterNumber) {

                newMQ -= initialPartition.getClusterByIndex(randDest).getCF();
                sourceCF = this.MQCalculator.getNewClusterFactor(initialPartition.getClusterByIndex(randSource),
                        null, initialPartition.getClusterByIndex(randSource).getElementByIndex(randElement));
                destCF = this.MQCalculator.getNewClusterFactor(initialPartition.getClusterByIndex(randDest),
                        initialPartition.getClusterByIndex(randSource).getElementByIndex(randElement), null);
            } else {

                QualityCluster tempDest = new QualityCluster(randDest);
                tempDest.add(initialPartition.getClusterByIndex(randSource).getElementByIndex(randElement));
                destCF = this.MQCalculator.getNewClusterFactor(tempDest, null, null);
                sourceCF = this.MQCalculator.getNewClusterFactor(initialPartition.getClusterByIndex(randSource),
                        null, initialPartition.getClusterByIndex(randSource).getElementByIndex(randElement));
            }
            newMQ = newMQ + sourceCF + destCF;

            if (HillClimbingClusterFinder.PLOT == true) {
                this.MQevals++;
            }

            boolean accept = this.optimMethod.acceptPartition(initialMQ, newMQ);

            if (accept || (newMQ - bestMQ) > 0) {

                found = true;
                bestMQ = newMQ;
                moveSource = randSource;
                moveDest = randDest;
                moveElement = randElement;
                pendIntraEdgeSource = initialPartition.getClusterByIndex(randSource).getPendingIntraEdges();
                pendInterEdgeSource = initialPartition.getClusterByIndex(randSource).getPendingInterEdges();

                if (moveDest < initialPartition.getClusterNumber()) {
                    pendIntraEdgeDest = initialPartition.getClusterByIndex(randDest).getPendingIntraEdges();
                    pendInterEdgeDest = initialPartition.getClusterByIndex(randDest).getPendingInterEdges();
                }
                if (accept) {
                    break;
                }
            }
        }

        boolean createAll = false;
        if (found) {
            createAll = this.moveElement(initialPartition, moveSource, moveDest, moveElement,
                    pendInterEdgeSource, pendInterEdgeDest, pendIntraEdgeSource, pendIntraEdgeDest);
            initialPartition.setMQ(bestMQ);
        }

        if (createAll == true) {
            this.moveCreator.createAllMoves(initialPartition);
        } else {
            this.moveCreator.createNewMoves(initialPartition, moveSource, moveDest);
        }

    }

    private boolean moveElement(QualityPartition initialPartition, int moveSource, int moveDest, int moveElement,
                                double pendInterEdgeSource, double pendInterEdgeDest, double pendIntraEdgeSource, double pendIntraEdgeDest) {

        boolean createAll = false;

        QualityElement toMove = initialPartition.getClusterByIndex(moveSource).getElementByIndex(moveElement);
        initialPartition.getClusterByIndex(moveSource).getElements().remove(toMove);

        if (moveDest < initialPartition.getClusterNumber()) {
            initialPartition.getClusterByIndex(moveDest).add(toMove);
        } else {
            QualityCluster newCluster = new QualityCluster(moveDest);
            newCluster.add(toMove);
            initialPartition.appendCluster(newCluster);
            this.MQCalculator.getNewClusterFactor(newCluster, null, null);
            pendIntraEdgeDest = newCluster.getIntraEdges();
            pendInterEdgeDest = newCluster.getInterEdges();
            createAll = true;
        }
        initialPartition.getClusterByIndex(moveSource).setCF(pendIntraEdgeSource, pendInterEdgeSource);
        initialPartition.getClusterByIndex(moveDest).setCF(pendIntraEdgeDest, pendInterEdgeDest);

        if (initialPartition.getClusterByIndex(moveSource).getSize() == 0) {
            initialPartition.getClusters().remove(initialPartition.getClusterByIndex(moveSource));
            createAll = true;
        }

        return createAll;
    }

    @Override
    public String toString() {
        String result = "Hill_SP_" + this.startPartitions + "_NP_" + this.neighboursPercent +
                this.optimMethod.toString() + "_MQ_" + String.format("%.3g%n", this.finalQuality);

        return result.trim();
    }

}
