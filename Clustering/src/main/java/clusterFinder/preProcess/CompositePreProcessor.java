package clusterFinder.preProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("WhileLoopReplaceableByForEach")
public class CompositePreProcessor implements PreProcessor {
    private List<PreProcessor> children;

    public CompositePreProcessor() {
        this.children = new ArrayList<>();
    }

    public CompositePreProcessor(List<PreProcessor> children) {
        this.children = new ArrayList<>();
        this.children.addAll(children);
    }

    public CompositePreProcessor(PreProcessor child1, PreProcessor child2) {
        this.children = new ArrayList<>();

        children.add(child1);
        children.add(child2);
    }

    public void add(PreProcessor child) {
        children.add(child);
    }

    @SuppressWarnings("WhileLoopReplaceableByForEach")
    @Override
    public Collection<Edge> process(Collection<Edge> edges, DSM initialDSM) {
        Collection<Edge> result = edges;

        for (PreProcessor aChildren : children) {
            result = aChildren.process(result, initialDSM);
        }

        return result;
    }

    public String toString() {
        final StringBuilder string = new StringBuilder();
        children
                .stream()
                .forEachOrdered(kid -> string.append(kid).append("_"));

        return string.toString();
    }
}
