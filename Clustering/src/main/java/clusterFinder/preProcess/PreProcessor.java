package clusterFinder.preProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.Collection;

public interface PreProcessor {
    public abstract Collection<Edge> process(Collection<Edge> edges, DSM initialDSM);
}
