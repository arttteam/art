package clusterFinder.preProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;
import sysmodel.edge.adjustor.Adjuster;

import java.util.Collection;

public class AdjustorPreprocessor implements PreProcessor {
    private Adjuster adjuster;

    public AdjustorPreprocessor(Adjuster adjuster) {
        this.adjuster = adjuster;
    }

    @Override
    public Collection<Edge> process(Collection<Edge> edges, DSM initialDSM) {
        for (Edge edge : edges) {
            edge.setValue(adjuster.adjust(edge, edges, initialDSM).getValue());
        }

        return edges;
    }

    public String toString() {
        return "AP_" + adjuster.toString();
    }
}
