package clusterFinder.preProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.Collection;

public class NonePreProcessor implements PreProcessor {
    @Override
    public Collection<Edge> process(Collection<Edge> edges, DSM initialDSM) {
        return edges;
    }

}
