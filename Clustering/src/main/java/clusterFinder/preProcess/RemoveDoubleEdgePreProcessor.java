package clusterFinder.preProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class RemoveDoubleEdgePreProcessor implements PreProcessor {
    @Override
    public SortedSet<Edge> process(Collection<Edge> edges, DSM initialDSM) {
        boolean isAdded = false;

        SortedSet<Edge> result = new TreeSet<>();
        Iterator<Edge> itEdge = edges.iterator();
        Iterator<Edge> itResult;

        Edge edge, resultEdge;

        while (itEdge.hasNext()) {
            edge = itEdge.next();

            itResult = result.iterator();

            while (itResult.hasNext()) {
                resultEdge = itResult.next();

                if (resultEdge.hasNodes(edge)) {
                    isAdded = true;
                }
            }

            if (!isAdded) {
                result.add(edge);
            }

            isAdded = false;
        }

        return result;
    }

    public String toString() {
        return "";
    }
}
