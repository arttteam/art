package clusterFinder.preProcess;

import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;

import java.util.*;
import java.util.stream.Collectors;

public class RemoveLibrariesPreProcessor implements PreProcessor {

    private float libraryThreshold = 1;
    private List<Integer> libraries;
    // When Auto mode is used same Object is used for multiple algorithm instances.
    // Setting this to false speeds up things, as libraries are computed only once for
    // all algorithm variations.
    // Set it to true if different systems will be tested at the same time.
    private boolean computeLibrariesAlways = false;

    public RemoveLibrariesPreProcessor(float libraryThreshold) {
        super();
        this.libraryThreshold = libraryThreshold;
    }

    public RemoveLibrariesPreProcessor(float libraryThreshold, boolean computeLibrariesAlways) {
        super();
        this.libraryThreshold = libraryThreshold;
        this.computeLibrariesAlways = computeLibrariesAlways;
    }

    @Override
    public Collection<Edge> process(Collection<Edge> edges,
                                    DSM initialDSM) {
        if ((libraries == null) || (computeLibrariesAlways)) {
            SparceMatrix<Integer> matrix = initialDSM.getDependencyMatrix();

            libraries = computeLibraries(matrix);
        }

        List<Edge> toRemove = new ArrayList<>();

        for (Edge edge : edges) {
            toRemove.addAll(libraries.stream().filter(library -> edge.hasNode(library)).map(library -> edge).collect(Collectors.toList()));
        }

        edges.removeAll(toRemove);

        return edges;
    }

    private List<Integer> computeLibraries(SparceMatrix<Integer> matrix) {
        List<Integer> auxLibraries = new LinkedList<>();
        int n = matrix.getColumns();

        for (int i = 0; i < n; i++) {
            float num = 0;
            Set<Integer> set = matrix.getConnectedToRow(i);

            if (set != null) {
                num = set.size();
            }

            if ((num / n) >= libraryThreshold) {
                auxLibraries.add(i);
            }
        }

        return auxLibraries;
    }

    public String toString() {
        return "LT_" + libraryThreshold;
    }
}
