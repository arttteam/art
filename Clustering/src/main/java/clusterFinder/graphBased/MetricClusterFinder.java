package clusterFinder.graphBased;

import clusterFinder.postProcess.PostProcessor;
import clusterFinder.preProcess.CompositePreProcessor;
import clusterFinder.preProcess.PreProcessor;
import clusterFinder.preProcess.RemoveDoubleEdgePreProcessor;
import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;
import sysmodel.edge.EdgeConstructor;
import sysmodel.metrics.Metric;

import java.util.*;
import java.util.stream.Collectors;

public class MetricClusterFinder extends GraphBasedClusterFinder {
    private Metric metric;
    private float strengthThreshold;

    public MetricClusterFinder(Metric metric, float strengthThreshold, EdgeConstructor ec,
                               PreProcessor preProcessor, PostProcessor postProcessor) {
        super(ec, new CompositePreProcessor(preProcessor, new RemoveDoubleEdgePreProcessor()), postProcessor);

        this.metric = metric;
        this.strengthThreshold = strengthThreshold;
    }

    @Override
    protected List<? extends Set<Integer>> computeClusters(DSM initialDSM,
                                                           Collection<Edge> processedEdges) {
        SparceMatrix<Integer> matrix = initialDSM.getDependencyMatrix();
        int n = matrix.getRows();
        boolean[] added = new boolean[n];

        Set<Edge> toRemove = processedEdges.stream().filter(auxEdge -> !(metric.getValue(auxEdge, processedEdges) >= strengthThreshold)).collect(Collectors.toSet());

        //remove edges that do meet metric threshold
        processedEdges.removeAll(toRemove);
        toRemove.clear();

        //create clusters from remaining edges
        List<Set<Integer>> clusters = buildCluster(processedEdges, added);
        Set<Integer> auxSet;

        //add left out nodes as single element clusters
        for (int i = 0; i < n; i++) {
            if (!added[i]) {
                auxSet = new HashSet<>();
                auxSet.add(i);
                clusters.add(auxSet);
            }
        }

        return clusters;
    }

    private List<Set<Integer>> buildCluster(Collection<Edge> edges, boolean[] added) {
        List<Set<Integer>> clusters = new ArrayList<>();
        Iterator<Set<Integer>> itClusters = clusters.iterator();
        Iterator<Integer> itSet;
        Set<Integer> auxSet, nodeASet, nodeBSet;
        Integer auxInt;

        for (Edge edge : edges) {
            nodeASet = null;
            nodeBSet = null;

            while (itClusters.hasNext()) {
                auxSet = itClusters.next();
                itSet = auxSet.iterator();

                while (itSet.hasNext()) {
                    auxInt = itSet.next();

                    if ((nodeASet == null) && (auxInt.equals(edge.getLeftNode()))) {
                        nodeASet = auxSet;
                    } else if ((nodeBSet == null) && (auxInt.equals(edge.getRightNode()))) {
                        nodeBSet = auxSet;
                    }
                }
            }

            if ((nodeASet == null) && (nodeBSet == null)) {
                auxSet = new HashSet<>();

                auxSet.add(edge.getLeftNode());
                auxSet.add(edge.getRightNode());

                clusters.add(auxSet);
            } else if (nodeBSet == null) {
                nodeASet.add(edge.getRightNode());
            } else if (nodeASet == null) {
                nodeBSet.add(edge.getLeftNode());
            } else if (nodeASet != nodeBSet) {
                nodeASet.addAll(nodeBSet);
                clusters.remove(nodeBSet);
            }

            added[edge.getLeftNode()] = true;
            added[edge.getRightNode()] = true;

            itClusters = clusters.iterator();
        }

        return clusters;
    }

    public String toString() {
        return "Metric_ST_" + strengthThreshold + "_" + super.toString();
    }
}
