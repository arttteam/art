package clusterFinder.graphBased;

import algorithm.KruskalAlgorithm;
import clusterFinder.postProcess.PostProcessor;
import clusterFinder.preProcess.PreProcessor;
import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;
import sysmodel.edge.EdgeConstructor;
import sysmodel.edge.EdgeListUtil;
import sysmodel.edge.statistic.MidValueStatistic;

import java.util.*;

public class MMSTClusterFinder extends GraphBasedClusterFinder {
    private float closenessFactor;

    public MMSTClusterFinder(float closenessFactor,
                             EdgeConstructor edgeContructor, PreProcessor preProcessor, PostProcessor postProcessor) {
        super(edgeContructor, preProcessor, postProcessor);
        this.closenessFactor = closenessFactor;
    }

    @Override
    protected List<? extends Set<Integer>> computeClusters(DSM initialDSM,
                                                           Collection<Edge> processedEdges) {
        SparceMatrix<Integer> matrix = initialDSM.getDependencyMatrix();
        int n = matrix.getRows();
        int[] added = new int[n];

        MidValueStatistic stat = new MidValueStatistic();
        KruskalAlgorithm ka = new KruskalAlgorithm(n, processedEdges);
        List<Edge> mst = ka.compute(stat);

        List<Set<Integer>> ret = new LinkedList<>();

        for (int i = 0; i < n; i++) {
            added[i] = i;
            Set<Integer> s = new HashSet<>();
            s.add(i);
            ret.add(s);
        }

        for (Edge e : mst) {
            Set<Integer> s = ret.get(added[e.getLeftNode()]);
            Set<Integer> t = ret.get(added[e.getRightNode()]);
            float value = e.getValue();

            if (value >= getClusterAvg(processedEdges, s) * closenessFactor && value >= getClusterAvg(processedEdges, t) * closenessFactor) {
                s.addAll(t);
                ret.remove(added[e.getRightNode()]);
                int x = added[e.getRightNode()];
                for (Integer k : t)
                    added[k] = added[e.getLeftNode()];
                for (Set<Integer> z : ret)
                    for (Integer k : z)
                        if (added[k] > x)
                            added[k]--;
                        else
                            break;
            }

        }

        return ret;
    }

    private float getClusterAvg(Collection<Edge> edges, Set<Integer> s) {
        float ret = 0;
        int nr = 0;

        for (Integer i : s) {
            List<Integer> t = EdgeListUtil.getIncomingNeighboursTo(i, edges);

            if (t != null)
                for (Integer j : t) {
                    Integer k;

                    if (s.contains(j) &&
                            (k = (int) EdgeListUtil.getEdgeValue(i, j, edges)) != null) {
                        ret += k;

                        nr++;
                    }
                }
        }

        if (nr == 0)
            return 0;

        return ret / nr;
    }

    @Override
    public String toString() {
        return "MMST_CF_" + closenessFactor + "_" + super.toString();
    }
}
