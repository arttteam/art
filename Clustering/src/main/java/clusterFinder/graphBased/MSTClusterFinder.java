package clusterFinder.graphBased;

import algorithm.KruskalAlgorithm;
import clusterFinder.postProcess.PostProcessor;
import clusterFinder.preProcess.PreProcessor;
import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;
import sysmodel.edge.EdgeConstructor;
import sysmodel.edge.adjustor.Adjuster;
import sysmodel.edge.statistic.MidValueStatistic;

import java.util.*;

public class MSTClusterFinder extends GraphBasedClusterFinder {

    private float strengthThreshold;
    private Adjuster midAdjuster;

    public MSTClusterFinder(float strengthThreshold, EdgeConstructor edgeConstructor,
                            PreProcessor preProcessor, Adjuster midAdjuster, PostProcessor postProcessor) {
        super(edgeConstructor, preProcessor, postProcessor);
        this.strengthThreshold = strengthThreshold;

        this.midAdjuster = midAdjuster;
    }

    @Override
    protected List<? extends Set<Integer>> computeClusters(DSM initialDSM,
                                                           Collection<Edge> processedEdges) {
        SparceMatrix<Integer> matrix = initialDSM.getDependencyMatrix();
        int n = matrix.getRows();
        int[] added = new int[n];

        //? Not MaxValue?
        MidValueStatistic stat = new MidValueStatistic();
        KruskalAlgorithm ka = new KruskalAlgorithm(n, processedEdges);
        List<Edge> mst = ka.compute(stat);

        float max = stat.getValue();
        Iterator<Edge> it = mst.iterator();

        List<Edge> toRemove = new LinkedList<>();

        Edge initialEdge, adjustedEdge;

        while (it.hasNext()) {
            initialEdge = it.next();
            adjustedEdge = midAdjuster.adjust(initialEdge, processedEdges, initialDSM);

            if (adjustedEdge.getValue() < strengthThreshold * max) {
                toRemove.add(initialEdge);
            }
        }

        mst.removeAll(toRemove);
        toRemove.clear();
        List<Set<Integer>> clusters = buildCluster(mst, added);

        for (int i = 0; i < n; i++) {
            if (added[i] == 0) {
                Set<Integer> s = new HashSet<>();
                s.add(i);
                clusters.add(s);
            }
        }

        return clusters;
    }

    private List<Set<Integer>> buildCluster(List<Edge> mst, int[] added) {
        List<Set<Integer>> clusters = new LinkedList<>();
        for (Edge e : mst) {
            int i = Math.max(added[e.getLeftNode()], added[e.getRightNode()]);
            if (i == 0) {
                Set<Integer> s = new HashSet<>();
                int size = clusters.size();
                s.add(e.getLeftNode());
                s.add(e.getRightNode());
                added[e.getLeftNode()] = added[e.getRightNode()] = size + 1;
                clusters.add(s);
            } else {
                int j = added[e.getLeftNode()] + added[e.getRightNode()] - i;
                Set<Integer> s = clusters.get(i - 1);
                if (j == 0) {
                    s.add(e.getLeftNode());
                    s.add(e.getRightNode());
                    added[e.getLeftNode()] = added[e.getRightNode()] = i;
                } else {
                    Set<Integer> t = clusters.get(j - 1);
                    t.addAll(s);
                    for (Integer k : s) {
                        added[k] = j;
                    }
                    Iterator<Set<Integer>> it = clusters.listIterator(i);
                    while (it.hasNext()) {
                        Set<Integer> z = it.next();
                        for (Integer k : z)
                            added[k]--;
                    }
                    clusters.remove(s);
                }
            }
        }
        return clusters;
    }

    public String toString() {
        return "MST_ST_" + strengthThreshold +
                "_MA_" + midAdjuster.toString() + "_" +
                super.toString();
    }
}