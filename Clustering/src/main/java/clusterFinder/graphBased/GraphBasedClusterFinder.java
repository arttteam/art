package clusterFinder.graphBased;

import clusterFinder.ClusterFinder;
import clusterFinder.postProcess.PostProcessor;
import clusterFinder.preProcess.PreProcessor;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;
import sysmodel.edge.EdgeConstructor;

import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public abstract class GraphBasedClusterFinder extends ClusterFinder {
    private EdgeConstructor edgeConstructor;

    protected GraphBasedClusterFinder(EdgeConstructor edgeConstructor,
                                      PreProcessor preProcessor, PostProcessor postProcessor) {
        super(preProcessor, postProcessor);
        this.edgeConstructor = edgeConstructor;
    }

    protected Collection<Edge> computeDependecyEdges(SparceMatrix<Integer> matrix) {
        int n = matrix.getRows();

        SortedSet<Edge> edges = new TreeSet<>();

        Edge initialEdge;

        for (int i = 0; i < n; i++) {
            Set<Integer> s = matrix.getConnectedToRow(i);

            if (s != null) {
                for (Integer j : s) {
                    initialEdge = edgeConstructor.build(i, j, matrix.getElement(i, j), matrix.getElement(j, i));

                    edges.add(initialEdge);
                }
            }
        }

        return edges;
    }
}
