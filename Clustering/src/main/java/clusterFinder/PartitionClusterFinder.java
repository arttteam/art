package clusterFinder;

import cycleFinder.CycleFinder;
import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;

import java.util.Collection;
import java.util.List;
import java.util.Set;

//Hard-coding to maintain old Partitioning capability, to be removed
public class PartitionClusterFinder extends ClusterFinder {
    private CycleFinder cycleFinder;

    public PartitionClusterFinder(CycleFinder cycleFinder) {
        super(null, null);

        this.cycleFinder = cycleFinder;
    }

    @Override
    public List<? extends Set<Integer>> findClusters(DSM dsm) {
        return cycleFinder.find(dsm.getDependencyMatrix());
    }

    @Override
    protected Collection<Edge> computeDependecyEdges(SparceMatrix<Integer> matrix) {
        return null;
    }

    @Override
    protected List<? extends Set<Integer>> computeClusters(
            DSM initialDSM, Collection<Edge> processedEdges) {
        return null;
    }
}
