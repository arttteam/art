package clusterFinder.postProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;
import sysmodel.edge.EdgeListUtil;
import sysmodel.edge.adjustor.Adjuster;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class AdoptionPostProcessor implements PostProcessor {
    private Adjuster adjuster;

    public AdoptionPostProcessor(Adjuster adjuster) {
        this.adjuster = adjuster;
    }

    @Override
    public List<? extends Set<Integer>> process(List<? extends Set<Integer>> clusters,
                                                DSM initialDSM, Collection<Edge> processedEdges) {
        List<Set<Integer>> toRemove = new LinkedList<>();
        List<Integer> neighbours;
        Integer element;
        int to;
        float value, newValue;

        for (Set<Integer> set1 : clusters) {
            if (set1.size() == 1) {
                element = set1.iterator().next();

                to = -1;
                value = 0;
                neighbours = EdgeListUtil.getNeighboursTo(element, processedEdges);

                if (neighbours != null) {
                    for (Integer k : neighbours) {
                        if (extraCondition(k, clusters)) {
                            newValue = adjuster.adjust(new Edge(element, k, 1), processedEdges, initialDSM).getValue();
                            if (newValue > value) {
                                value = newValue;
                                to = k;
                            }
                        }
                    }
                }

                if (to > 0) {
                    for (Set<Integer> set2 : clusters) {
                        if (set2.contains(to)) {
                            set2.add(element);

                            set1.remove(element);

                            break;
                        }
                    }

                    toRemove.add(set1);
                }
            }
        }

        clusters.removeAll(toRemove);

        toRemove.clear();

        return clusters;
    }

    protected boolean extraCondition(Integer neighbour, List<? extends Set<Integer>> clusters) {
        return true;
    }

    protected String getAdjustorToString() {
        return adjuster.toString();
    }

    public String toString() {
        return "RA_AD_" + adjuster.toString();
    }
}
