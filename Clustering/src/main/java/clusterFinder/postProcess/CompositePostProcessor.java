package clusterFinder.postProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;
import util.SaveClusters;

import java.util.*;
import java.util.stream.Collectors;

public class CompositePostProcessor implements PostProcessor {
    private final List<PostProcessor> children;

    public CompositePostProcessor() {
        this.children = new ArrayList<>();
    }

    public CompositePostProcessor(List<PostProcessor> children) {
        this.children = children.stream().collect(Collectors.toList());
    }

    public CompositePostProcessor(PostProcessor child) {
        this.children = Arrays.asList(child);
    }

    public CompositePostProcessor(PostProcessor child1, PostProcessor child2) {
        this.children = new ArrayList<>();

        children.add(child1);
        children.add(child2);
    }

    public void add(PostProcessor child) {
        children.add(child);
    }

    @Override
    public List<? extends Set<Integer>> process(
            List<? extends Set<Integer>> clusters,
            DSM initialDSM, Collection<Edge> processedEdges) {

        for (PostProcessor aChildren : children) {
            clusters = aChildren.process(clusters, initialDSM, processedEdges);
        }
        SaveClusters.cycles = clusters;
        return clusters;
    }

    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();

        children.forEach(stringBuilder.append("_")::append);

        return stringBuilder.toString();
    }

}
