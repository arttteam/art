package clusterFinder.postProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;
import sysmodel.edge.adjustor.Adjuster;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class ModifiedAdoptionPostProcessor extends AdoptionPostProcessor {
    private int parentSize;
    private int nrOfRuns;

    public ModifiedAdoptionPostProcessor(Adjuster adjuster, int parentSize, int nrOfRuns) {

        super(adjuster);
        this.parentSize = parentSize;
        this.nrOfRuns = nrOfRuns;
    }

    @Override
    public List<? extends Set<Integer>> process(List<? extends Set<Integer>> clusters,
                                                DSM initialDSM, Collection<Edge> processedEdges) {
        List<? extends Set<Integer>> auxClusters = clusters;

        for (int i = 0; i < nrOfRuns; i++) {
            auxClusters = super.process(auxClusters, initialDSM, processedEdges);
        }

        return auxClusters;
    }

    @Override
    protected boolean extraCondition(Integer neighbour, List<? extends Set<Integer>> clusters) {
        return isSuitableParent(neighbour, clusters);
    }

    private boolean isSuitableParent(Integer parent, List<? extends Set<Integer>> clusters) {
        for (Set<Integer> set : clusters) {
            if (set.contains(parent)) {
                if (set.size() > parentSize) {
                    return true;
                }

                return false;
            }
        }

        return false;
    }

    public String toString() {
        return "MA_" + getAdjustorToString() + "_PS_" + parentSize + "_T_" + nrOfRuns;
    }
}
