package clusterFinder.postProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface PostProcessor {
    public abstract List<? extends Set<Integer>> process(List<? extends Set<Integer>> clusters,
                                                         DSM initialDSM, Collection<Edge> processedEdges);
}
