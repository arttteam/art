package clusterFinder.postProcess;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class NonePostProcessor implements PostProcessor {
    @Override
    public List<? extends Set<Integer>> process(List<? extends Set<Integer>> clusters,
                                                DSM initialDSM, Collection<Edge> processedEdges) {
        return clusters;
    }

    public String toString() {
        return "";
    }
}
