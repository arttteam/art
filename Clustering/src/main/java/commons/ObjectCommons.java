package commons;

/**
 * Created by ThatKidFlo on 18/08/16.
 */
public final class ObjectCommons {

    public static <T> boolean isNil(T subject) {
        return subject == null;
    }

    public static <T> boolean isNotNil(T subject) {
        return subject != null;
    }
}
