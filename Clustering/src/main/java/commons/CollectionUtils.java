package commons;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by ThatKidFlo on 18/08/16.
 */
public class CollectionUtils {

    public static <K, V> Optional<V> maybeGet(Map<K, V> map, K key) {
        return Optional.ofNullable(map.get(key));
    }

    public static <A, B, Z, K, V> Map<K, V> zipToMap(List<A> first,
                                                     List<B> second,
                                                     BiFunction<A, B, Z> zipper,
                                                     Function<Z, K> keyMapper,
                                                     Function<Z, V> valueMapper) {
        final int size = Math.min(first.size(), second.size());

        final List<Z> zipped = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            zipped.add(zipper.apply(first.get(i), second.get(i)));
        }

        return zipped.stream().collect(Collectors.toMap(keyMapper, valueMapper));
    }
}