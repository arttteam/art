package commons;

/**
 * Created by ThatKidFlo on 20/08/16.
 */
public class DataStructures {

    public static class Tuple<A, B> {
        private final A first;
        private final B second;

        private Tuple(A first, B second) {
            this.first = first;
            this.second = second;
        }

        public static <A, B> Tuple<A, B> create(A first, B second) {
            return new Tuple<>(first, second);
        }

        public A getFirst() {
            return this.first;
        }

        public B getSecond() {
            return this.second;
        }
    }
}
