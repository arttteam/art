/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dataFormat;

import clusterFinder.hillclimbing.ModularizationQualityCalculator;

import java.util.Collection;

/**
 * @author raptor
 */
public class QualityCluster extends Cluster {

    private double CF = 0;
    private double intraEdges = 0;
    private double interEdges = 0;

    private double pendingIntraEdges = 0;
    private double pendingInterEdges = 0;

    public QualityCluster(int tag) {

        super(tag);
    }

    public QualityCluster(int tag, Collection<Element> elements) {

        super(tag, elements);
    }

    public QualityElement getElementByIndex(int index) {

        return (QualityElement) this.getElements().get(index);
    }

    public double getIntraEdges() {

        return this.intraEdges;
    }

    public double getInterEdges() {

        return this.interEdges;
    }

    public double getCF() {

        return this.CF;
    }

    public void setCF(double intraEdges, double interEdges) {

        this.intraEdges = intraEdges;
        this.interEdges = interEdges;

        this.CF = ModularizationQualityCalculator.calculateCF(intraEdges, interEdges);
    }

    public void setPendingEdges(double intraEdges, double interEdges) {

        this.pendingInterEdges = interEdges;
        this.pendingIntraEdges = intraEdges;
    }

    public double getPendingIntraEdges() {

        return this.pendingIntraEdges;
    }

    public double getPendingInterEdges() {

        return this.pendingInterEdges;
    }

    @Override
    public int hashCode() {

        int hash = 3;
        hash = 53 * hash + (this.getElements() != null ? this.getElements().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object o) {

        if (o instanceof QualityCluster) {

            for (int i = 0; i < this.getSize(); i++) {
                if (!((QualityCluster) o).getElements().contains(this.getElementByIndex(i))) {
                    return false;
                }
            }
            for (int i = 0; i < ((QualityCluster) o).getSize(); i++) {
                if (!this.getElements().contains(((QualityCluster) o).getElementByIndex(i))) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
