/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dataFormat;

/**
 * @author raptor
 */
public class QualityElement extends Element {

    public QualityElement(String classname, int index) {

        super(classname, index);
    }

    @Override
    public boolean equals(Object o) {

        if (o instanceof QualityElement) {

            if (((QualityElement) o).classname.equals(this.classname) && ((QualityElement) o).index == this.index) {
                return true;
            }
        }
        return false;

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + this.index;
        return hash;
    }
}
