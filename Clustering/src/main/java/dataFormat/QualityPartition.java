/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dataFormat;

import java.util.Collection;

/**
 * @author raptor
 */
public class QualityPartition extends Partition {

    private double MQ = 0;

    public QualityPartition() {

        super();
    }

    public QualityPartition(Collection<Cluster> clusters) {

        super(clusters);
    }

    public QualityCluster getClusterByIndex(int index) {

        return (QualityCluster) this.getClusters().get(index);
    }

    public void setMQ(double MQ) {

        this.MQ = MQ;
    }

    public double getMQ() {

        return this.MQ;
    }

    @Override
    public int hashCode() {

        int hash = 3;
        hash = 29 * hash + (this.getClusters() != null ? this.getClusters().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object o) {

        if (o instanceof QualityPartition) {

            for (int i = 0; i < this.getClusterNumber(); i++) {
                if (!((QualityPartition) o).getClusters().contains(this.getClusterByIndex(i))) {
                    return false;
                }
            }
            for (int i = 0; i < ((QualityPartition) o).getClusterNumber(); i++) {
                if (!this.getClusters().contains(((QualityPartition) o).getClusterByIndex(i))) {
                    return false;
                }
            }

        } else {
            return false;
        }
        return true;
    }
}
