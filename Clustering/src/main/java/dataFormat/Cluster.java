package dataFormat;

import java.util.*;
import java.util.stream.Collectors;


public class Cluster {

    private List<Element> elements;
    private int tag;

    public Cluster(int tag) {
        this.tag = tag;
        this.elements = new ArrayList<>();
    }

    public Cluster(int tag, Collection<Element> elements) {
        this(tag);
        this.elements.addAll(elements);
    }

    public void add(Element e) {
        this.elements.add(e);
    }

    public int getSize() {
        return this.elements.size();
    }

    public List<Element> getElements() {
        return this.elements;
    }

    public int getTag() {
        return this.tag;
    }

    public Set<Integer> toSet() {
        return elements
                .stream()
                .map(element -> element.index)
                .collect(Collectors.toSet());
    }
}
