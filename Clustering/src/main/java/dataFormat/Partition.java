/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dataFormat;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author mihai
 */
public class Partition {

    private ArrayList<Cluster> partition;
    public int classes;

    public Partition() {
        partition = new ArrayList<>();
        this.classes = 0;
    }

    public Partition(Collection<Cluster> clusters) {
        this();
        this.partition.addAll(clusters);
    }

    public void appendCluster(Cluster c) {
        partition.add(c);
        this.classes += c.getSize();
    }

    public ArrayList<Cluster> getClusters() {
        return this.partition;
    }

    public int getClusterNumber() {
        return partition.size();
    }

    public List<Set<Integer>> toListSet() {
        return partition
                .stream()
                .map(Cluster::toSet)
                .collect(Collectors.toList());
    }

    public String toString() {
        String sir = "";
        for (Cluster c : partition) {
            sir += "\n";
            for (Object e : c.getElements()) {
                sir += e.toString();
            }
        }
        return sir;
    }
}
