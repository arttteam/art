/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dataFormat;

/**
 * @author mihai
 */
public class Element {
    public String classname;
    public int index;

    public Element(String classname, int index) {
        this.classname = classname;
        this.index = index;
    }

    public String getClassName() {
        return this.classname;
    }

    public String toString() {
        return "\n[" + classname + ":" + index + "]";
    }
}
