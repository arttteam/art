package algorithm;

import sysmodel.edge.Edge;
import sysmodel.edge.statistic.EdgeStatistic;

import java.util.*;

public class KruskalAlgorithm implements Algorithm {
    private ArrayList<List<Integer>> sublists;
    private int n;
    private Collection<Edge> edges;

    public KruskalAlgorithm(int n, Collection<Edge> edges) {
        this.n = n;
        this.edges = edges;
    }

    private int findIndex(Integer i) {
        int j = 0;
        for (List<Integer> s : sublists) {
            if (s.contains(i))
                return j;
            j++;
        }
        return -1;
    }

    private void merge(int i, int j) {
        List<Integer> s = sublists.get(j);
        sublists.get(i).addAll(s);
        sublists.remove(j);
        s.clear();
    }

    private void buildSublists() {
        sublists = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            List<Integer> l = new LinkedList<>();
            l.add(i);
            sublists.add(l);
        }
    }

    public List<Edge> compute(EdgeStatistic s) {
        int m = n - 1;
        buildSublists();
        List<Edge> ret = new LinkedList<>();
        Iterator<Edge> it = edges.iterator();
        while (m > 0 && it.hasNext()) {
            Edge first = it.next();
            int i = findIndex(first.getLeftNode());
            int j = findIndex(first.getRightNode());
            if (i != j) {
                merge(i, j);
                ret.add(first);
                s.add(first);
                m--;
            }
        }
        return ret;
    }
}


