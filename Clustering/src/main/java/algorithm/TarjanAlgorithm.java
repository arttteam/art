package algorithm;

import sysmodel.SparceMatrix;

import java.util.*;


public final class TarjanAlgorithm implements Algorithm {
    private Stack<Integer> stack = new Stack<>();
    private SparceMatrix<Integer> m;
    private int[] nindex;
    private int[] nlowlink;
    private int index;

    public TarjanAlgorithm(SparceMatrix<Integer> m) {
        this.m = m;
        int size = m.getRows();
        nindex = new int[size];
        nlowlink = new int[size];
        for (int i = 0; i < size; i++) {
            nindex[i] = -1;
            nlowlink[i] = -1;
        }
        index = 0;
    }

    public List<? extends Set<Integer>> compute() {
        LinkedList<Set<Integer>> SCC = new LinkedList<>();
        int size = m.getColumns();
        int i = 0;
        while (i < size) {
            if (nindex[i] == -1) {
                compute(i, SCC);
            }
            i++;
        }
        return SCC;
    }

    private void compute(int v, LinkedList<Set<Integer>> SCC) {
        nindex[v] = index;
        nlowlink[v] = index;
        index++;
        stack.push(v);
        Set<Integer> s = m.getConnectedToColumns(v);
        if (s != null)
            for (Integer n : s) {
                if (nindex[n] == -1) {
                    compute(n, SCC);
                    nlowlink[v] = Math.min(nlowlink[v], nlowlink[n]);
                } else if (stack.contains(n)) {
                    nlowlink[v] = Math.min(nlowlink[v], nindex[n]);
                }
            }

        if (nlowlink[v] == nindex[v]) {
            Integer n;
            Set<Integer> component = new HashSet<>();
            do {
                n = stack.pop();
                component.add(n);
            } while (n != v);
            if (!SCC.contains(component))
                SCC.add(component);
        }
    }
}
