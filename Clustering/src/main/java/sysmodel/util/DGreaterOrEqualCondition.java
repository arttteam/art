package sysmodel.util;

public class DGreaterOrEqualCondition implements DoubleCondition {
    @Override
    public boolean meetsCondition(double firstValue, double secondValue) {
        if (firstValue >= secondValue)
            return true;
        return false;
    }

}
