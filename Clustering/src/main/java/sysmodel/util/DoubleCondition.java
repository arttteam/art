package sysmodel.util;

public interface DoubleCondition {
    public abstract boolean meetsCondition(double firstValue, double secondValue);
}
