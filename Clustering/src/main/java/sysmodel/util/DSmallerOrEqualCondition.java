package sysmodel.util;

public class DSmallerOrEqualCondition implements DoubleCondition {
    @Override
    public boolean meetsCondition(double firstValue, double secondValue) {
        if (firstValue <= secondValue)
            return true;
        return false;
    }
}
