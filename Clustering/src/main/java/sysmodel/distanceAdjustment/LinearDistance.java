package sysmodel.distanceAdjustment;


public class LinearDistance implements Distance {

    @Override
    public float compute(float x1, float x2) {
        return 1 - Math.abs(x1 - x2);
    }

    public String toString() {
        return "L";
    }
}
