package sysmodel.distanceAdjustment;

import java.util.*;


public abstract class DistanceFactory {
    public static final String EXPONENTIAL = "exponential";
    public static final String LINEAR = "linear";
    public static final String NONE = "none";

    private static Map<String, Distance> map = new HashMap<>();

    static {
        map.put("exponential", new ExponentialDistance());
        map.put("none", new ConstantValueDistance(1.0f));
        map.put("linear", new LinearDistance());
    }

    public static Distance getByName(String name) {
        return map.get(name);
    }

    public static List<Distance> getByName(Collection<String> values) {
        if (values == null) {
            return null;
        }

        List<Distance> distances = new ArrayList<>();
        Distance auxDistance;

        for (String value : values) {
            auxDistance = DistanceFactory.getByName(value);

            if (auxDistance == null) {
                return null;
            }

            distances.add(auxDistance);
        }

        return distances;
    }
}
