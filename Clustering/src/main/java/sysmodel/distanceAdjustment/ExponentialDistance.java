package sysmodel.distanceAdjustment;


public class ExponentialDistance implements Distance {

    public float compute(float x1, float x2) {
        double d = Math.abs(x1 - x2);
        return (float) Math.exp(-2 * d);
    }

    public String toString() {
        return "E";
    }
}
