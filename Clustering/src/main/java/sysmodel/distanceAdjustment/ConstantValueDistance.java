package sysmodel.distanceAdjustment;


public class ConstantValueDistance implements Distance {
    private float value;

    public ConstantValueDistance(float value) {
        this.value = value;
    }

    @Override
    public float compute(float x1, float x2) {
        return value;
    }

    public String toString() {
        return "C";
    }
}
