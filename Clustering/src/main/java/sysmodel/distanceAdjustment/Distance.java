package sysmodel.distanceAdjustment;

public interface Distance {
    float compute(float x1, float x2);
}
