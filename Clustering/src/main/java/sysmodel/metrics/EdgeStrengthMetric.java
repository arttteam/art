package sysmodel.metrics;

import sysmodel.edge.Edge;
import sysmodel.edge.EdgeListUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EdgeStrengthMetric implements Metric {

    public EdgeStrengthMetric() {
    }

    public float getValue(Edge edge, Collection<Edge> edges) {
        List<Integer> nodeANeighbours = EdgeListUtil.getNeighboursTo(edge.getLeftNode(), edges);
        List<Integer> nodeBNeighbours = EdgeListUtil.getNeighboursTo(edge.getRightNode(), edges);

        nodeANeighbours.remove(Integer.valueOf(edge.getRightNode()));
        nodeBNeighbours.remove(Integer.valueOf(edge.getLeftNode()));

        List<Integer> intersect = findIntersect(nodeANeighbours, nodeBNeighbours);

        nodeANeighbours.removeAll(intersect);
        nodeBNeighbours.removeAll(intersect);

        float fourCycles = s(nodeANeighbours, intersect, edges) + s(nodeBNeighbours, intersect, edges) +
                s(nodeANeighbours, nodeBNeighbours, edges) + s(intersect, edges);

        float sizeSum = (intersect.size() + nodeANeighbours.size() + nodeBNeighbours.size());

        float threeCycles;

        if (sizeSum > 0) {
            threeCycles = (float) intersect.size() /
                    (float) (intersect.size() + nodeANeighbours.size() + nodeBNeighbours.size());
        } else {
            threeCycles = 0;
        }

        return (fourCycles + threeCycles) / 5;
    }

//	public boolean test(float thresholdValue, Edge edge, Collection<Edge> edges)
//	{
//		if(getValue(edge, edges) >= thresholdValue)
//		{
//			return true;
//		}
//		
//		return false;
//	}

    private float s(List<Integer> u, List<Integer> v, Collection<Edge> edges) {
        if ((u.size() > 0) && (v.size() > 0)) {
            return (float) EdgeListUtil.numberOfConnectingEdges(u, v, edges, false) / (float) (u.size() * v.size());
        } else {
            return 0;
        }
    }

    //function defined in doc
    private float s(List<Integer> u, Collection<Edge> edges) {
        int binomialCoefficient = binomialCoefficient(u.size());

        if (binomialCoefficient > 0) {
            return (float) EdgeListUtil.numberOfConnectingEdges(u, edges, false) / (float) binomialCoefficient;
        } else {
            return 0;
        }
    }


    private List<Integer> findIntersect(List<Integer> nodeANeighbours, List<Integer> nodeBNeighbours) {
        List<Integer> intersect = new ArrayList<>();

        for (Integer nodeANeighbour : nodeANeighbours) {
            for (Integer nodeBNeighbour : nodeBNeighbours) {
                if (nodeANeighbour.equals(nodeBNeighbour)) {
                    intersect.add(nodeANeighbour);

                    break;
                }
            }
        }

        return intersect;
    }

    private int binomialCoefficient(int number) {
        if (number > 0) {
            return (number * (number - 1)) / 2;
        } else
            return 0;
    }
}
