package sysmodel.metrics;

import sysmodel.edge.Edge;

import java.util.Collection;

public interface Metric {
    public abstract float getValue(Edge edge, Collection<Edge> edges);
}
