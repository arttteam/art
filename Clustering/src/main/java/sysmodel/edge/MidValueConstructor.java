package sysmodel.edge;

public class MidValueConstructor implements EdgeConstructor {

    @Override
    public Edge build(int nodeA, int nodeB, Integer e1, Integer e2) {
        int value = (e1 + getSecond(e2)) / 2;

        return new Edge(nodeA, nodeB, value);
    }

    private int getSecond(Integer e2) {
        if (e2 == null) {
            return 0;
        }

        return e2;
    }
}
