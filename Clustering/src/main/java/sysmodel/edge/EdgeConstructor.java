package sysmodel.edge;

public interface EdgeConstructor {
    Edge build(int nodeA, int nodeB, Integer e1, Integer e2);
}
