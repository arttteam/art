package sysmodel.edge;

public final class Edge implements Comparable<Edge>, Cloneable {
    public int leftNode, rightNode;

    private float value;

    public Edge(int leftNode, int rightNode, float value) {
        this.leftNode = leftNode;
        this.rightNode = rightNode;
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getLeftNode() {
        return leftNode;
    }

    public int getRightNode() {
        return rightNode;
    }

    public boolean hasNode(int node) {
        return ((leftNode == node) || (rightNode == node));
    }

    public boolean hasNodes(Edge edge) {
        return hasNodes(edge.leftNode, edge.rightNode);
    }

    public boolean hasNodes(int firstNode, int secondNode) {
        return (((this.leftNode == firstNode) && (this.rightNode == secondNode)) ||
                ((this.leftNode == secondNode) && (this.rightNode == firstNode)));
    }

    public boolean hasSameNodes(int leftNode, int rightNode) {
        return ((this.leftNode == leftNode) && (this.rightNode == rightNode));
    }

    public String toString() {
        return "(" + leftNode + "," + rightNode + "," + value + ")";
    }

    @Override
    public int compareTo(Edge edge) {
        float c = this.value - edge.value;
        if (c == 0) {
            int d = this.leftNode - edge.leftNode;

            if (d == 0) {
                return this.rightNode - edge.rightNode;
            }

            return d;
        }
        if (c > 0) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Edge) {
            Edge edge = (Edge) o;

            if ((edge.leftNode == leftNode) && (edge.rightNode == rightNode) && (edge.value == value)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 1;

        hash = hash * 17 + leftNode;
        hash = hash * 31 + rightNode;
        hash = hash * 53 + Float.valueOf(value).hashCode();

        return hash;
    }

    @Override
    public Edge clone() throws CloneNotSupportedException {
        Edge clone = (Edge) super.clone();

        clone.value = value;

        return clone;
    }

    public Edge reverse() {
        return new Edge(rightNode, leftNode, value);
    }
}