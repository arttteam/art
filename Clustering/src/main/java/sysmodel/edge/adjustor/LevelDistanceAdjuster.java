package sysmodel.edge.adjustor;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.Collection;

public abstract class LevelDistanceAdjuster implements Adjuster {

    @Override
    public abstract Edge adjust(Edge edge, Collection<Edge> processedEdges,
                                DSM initialDSM);

    @Override
    public String toString() {
        return "LD_";
    }
}
