package sysmodel.edge.adjustor;

import cycleFinder.BinaryCycleFinder;
import cycleFinder.CycleFinder;
import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.edge.Edge;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public abstract class RegularLevelDistanceAdjuster extends LevelDistanceAdjuster {
    private int[] levels;
    private int size;
    // When Auto mode is used same Object is used for multiple algorithm instances.
    // Setting this to false speeds up things, as levels are computed only once for
    // all algorithm variations. This does not apply for MST Mid Adjuster, new instance
    // is created by each algorithm.
    // Set it to true if different systems will be tested at the same time.
    private boolean computeLevelsAlways = false;

    public RegularLevelDistanceAdjuster(boolean computeLevelsAlways) {
        this.computeLevelsAlways = computeLevelsAlways;
    }

    public Edge adjust(Edge edge, Collection<Edge> edges, DSM initialDSM) {
        if ((levels == null) || (computeLevelsAlways)) {
            SparceMatrix<Integer> matrix = initialDSM.getDependencyMatrix();
            //System.out.println("intra");
            CycleFinder b = new BinaryCycleFinder();
            List<? extends Set<Integer>> partitions = b.find(matrix);

            size = partitions.size();
            levels = computeLevels(partitions, matrix.getRows());
            ;
        }
        Edge adujstedEdge = new Edge(edge.getLeftNode(), edge.getRightNode(),
                edge.getValue() * computeDistance(
                        ((float) levels[edge.getLeftNode()]) / size,
                        ((float) levels[edge.getRightNode()]) / size));

        return adujstedEdge;
    }

    public abstract float computeDistance(float x1, float x2);

    private int[] computeLevels(List<? extends Set<Integer>> partitions, int n) {
        int[] levels = new int[n];
        int j = 0;
        for (Set<Integer> s : partitions) {
            for (Integer i : s) {
                levels[i] = j;
            }

            j++;
        }

        return levels;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
