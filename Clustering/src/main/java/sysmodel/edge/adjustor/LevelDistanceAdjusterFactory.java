package sysmodel.edge.adjustor;

import commons.CollectionUtils;
import commons.DataStructures;

import java.util.*;

public class LevelDistanceAdjusterFactory {
    public static final String EXPONENTIAL = "exponential";
    public static final String LINEAR = "linear";
    public static final String NONE = "none";

    private static Map<String, LevelDistanceAdjuster> map;

    public static void reset() {
        map = CollectionUtils.zipToMap(
                Arrays.asList(EXPONENTIAL, NONE, LINEAR),
                Arrays.asList(new ExpoLevelDistanceAdjuster(false),
                             new NoneLevelDistanceAdjuster(),
                             new LinearLevelDistanceAdjuster(false)),
                DataStructures.Tuple::create,
                DataStructures.Tuple::getFirst,
                DataStructures.Tuple::getSecond
        );
    }

    public static LevelDistanceAdjuster getByName(String name) {
        if (map == null)
            reset();

        return map.get(name);
    }

    public static Collection<LevelDistanceAdjuster> getByName(Collection<String> values) {
        if (values == null) {
            return null;
        }

        List<LevelDistanceAdjuster> adjusters = new ArrayList<>();
        LevelDistanceAdjuster auxAdjusters;

        for (String value : values) {
            auxAdjusters = LevelDistanceAdjusterFactory.getByName(value);

            if (auxAdjusters == null) {
                return null;
            }

            adjusters.add(auxAdjusters);
        }

        return adjusters;
    }
}
