package sysmodel.edge.adjustor;

import sysmodel.DSM;
import sysmodel.edge.Edge;
import sysmodel.metrics.EdgeStrengthMetric;

import java.util.Collection;

public class GeometricAdjuster implements Adjuster {
    private static final float metricIntervalStart = 0;
    private static final float metricIntervalFinish = 1;
    private static final float scaledIntervalStart = 0;
    private static final float scaledIntervalFinish = 1;

    private float confidenceLevel;
    private EdgeStrengthMetric strengthMetric;

    public GeometricAdjuster(float confidenceLevel) {
        this.confidenceLevel = confidenceLevel;

        strengthMetric = new EdgeStrengthMetric();
    }


    public Edge adjust(Edge edge, Collection<Edge> edges,
                       DSM initialDSM) {
        if (confidenceLevel > 0) {
            Edge result;
            float metricValue;

            metricValue = strengthMetric.getValue(edge, edges);

            metricValue = scaleValue(metricIntervalStart, metricIntervalFinish,
                    (1 - confidenceLevel) * (scaledIntervalFinish - scaledIntervalStart),
                    scaledIntervalFinish, metricValue);

            result = new Edge(edge.getLeftNode(), edge.getRightNode(), edge.getValue() * metricValue);

            return result;
        }

        return edge;
    }


    private float scaleValue(float initialStartValue, float initialFinishValue,
                             float afterStartValue, float afterFinishValue,
                             float value) {
        float intervalScale = (initialFinishValue - initialStartValue) /
                (afterFinishValue - afterStartValue);

        return value / intervalScale + afterStartValue;
    }

    public String toString() {
        return "GA_CL_" + confidenceLevel;
    }
}
