package sysmodel.edge.adjustor;

public class LinearLevelDistanceAdjuster extends RegularLevelDistanceAdjuster {
    public LinearLevelDistanceAdjuster(boolean computeLevelsAlways) {
        super(computeLevelsAlways);
    }

    @Override
    public float computeDistance(float x1, float x2) {
        return 1 - Math.abs(x1 - x2);
    }

    public String toString() {
        return super.toString() + "L";
    }
}
