package sysmodel.edge.adjustor;

public class ExpoLevelDistanceAdjuster extends RegularLevelDistanceAdjuster {
    public ExpoLevelDistanceAdjuster(boolean computeLevelsAlways) {
        super(computeLevelsAlways);
    }

    @Override
    public float computeDistance(float x1, float x2) {
        double d = Math.abs(x1 - x2);
        return (float) Math.exp(-2 * d);
    }

    public String toString() {
        return "E";
    }
}
