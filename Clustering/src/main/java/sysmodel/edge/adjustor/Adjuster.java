package sysmodel.edge.adjustor;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.Collection;

public interface Adjuster {
    Edge adjust(Edge edge, Collection<Edge> processedEdges, DSM initialDSM);
}
