package sysmodel.edge.adjustor;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class CompositeAdjuster implements Adjuster {
    private Collection<Adjuster> adjusters;

    public CompositeAdjuster() {
        adjusters = new ArrayList<>();
    }

    public CompositeAdjuster(Adjuster first, Adjuster second) {
        this();
        adjusters.add(first);
        adjusters.add(second);
    }

    public CompositeAdjuster(Collection<Adjuster> adjusters) {
        this.adjusters = adjusters;
    }

    public void addAdjustor(Adjuster adjuster) {
        adjusters.add(adjuster);
    }

    @Override
    public Edge adjust(Edge edge, Collection<Edge> edges,
                       DSM initialDSM) {
        Edge result = edge;

        for (Adjuster adjuster : adjusters) {
            result = adjuster.adjust(result, edges, initialDSM);
        }

        return result;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        adjusters
                .stream()
                .forEachOrdered(result::append);

        return result.toString();
    }
}
