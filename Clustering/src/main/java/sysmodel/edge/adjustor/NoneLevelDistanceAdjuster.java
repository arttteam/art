package sysmodel.edge.adjustor;

import sysmodel.DSM;
import sysmodel.edge.Edge;

import java.util.Collection;

public class NoneLevelDistanceAdjuster extends LevelDistanceAdjuster {
    @Override
    public Edge adjust(Edge edge, Collection<Edge> processedEdges,
                       DSM initialDSM) {
        return edge;
    }

    public String toString() {
        return super.toString() + "C";
    }
}
