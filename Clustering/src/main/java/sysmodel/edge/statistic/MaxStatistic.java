package sysmodel.edge.statistic;

import sysmodel.edge.Edge;

public class MaxStatistic implements EdgeStatistic {
    private float value = 0;

    @Override
    public void add(Edge e) {
        if (e.getValue() > value) {
            value = e.getValue();
        }
    }

    public float getValue() {
        return value;
    }
}
