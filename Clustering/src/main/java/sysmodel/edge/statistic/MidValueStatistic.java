package sysmodel.edge.statistic;

import sysmodel.edge.Edge;

public class MidValueStatistic implements EdgeStatistic {
    private float value = 0;
    private int nr = 0;

    @Override
    public void add(Edge e) {
        value += e.getValue();
        nr++;
    }

    public float getValue() {
        return value / nr;
    }
}
