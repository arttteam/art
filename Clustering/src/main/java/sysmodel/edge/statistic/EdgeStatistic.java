package sysmodel.edge.statistic;

import sysmodel.edge.Edge;

public interface EdgeStatistic {
    void add(Edge e);

    float getValue();
}
