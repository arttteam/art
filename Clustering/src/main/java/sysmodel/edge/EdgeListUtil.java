package sysmodel.edge;

import sysmodel.edge.statistic.EdgeStatistic;

import java.util.*;

public class EdgeListUtil {
    public static Collection<Edge> cloneEdges(Collection<Edge> edges) throws CloneNotSupportedException {
        final List<Edge> newEdges = new ArrayList<>();

        for (Edge edge : edges) {
            newEdges.add(edge.clone());
        }

        return new TreeSet<>(newEdges);
    }

    public static Map<Integer, Collection<Edge>> getEdgeToNodeMap(Collection<Edge> edges,
                                                                  EdgeStatistic statistic) {
        Map<Integer, Collection<Edge>> result = new HashMap<>();

        Iterator<Edge> itEdges = edges.iterator();
        Edge auxEdge;
        Collection<Edge> auxEdges;

        while (itEdges.hasNext()) {
            auxEdge = itEdges.next();
            auxEdges = result.get(auxEdge.getLeftNode());

            if (auxEdges == null) {
                auxEdges = new ArrayList<>();
                result.put(auxEdge.getLeftNode(), auxEdges);
            }

            auxEdges.add(auxEdge);
            statistic.add(auxEdge);
        }

        return result;
    }

    public static float getEdgeValue(int leftNode, int rightNode, Collection<Edge> edges) {
        return edges
                .stream()
                .filter(edge -> edge.hasSameNodes(leftNode, rightNode))
                .findFirst()
                .map(Edge::getValue)
                .orElseGet(() -> -1.0f);
    }

    public static List<Integer> getNeighboursTo(int node, Collection<Edge> edges) {
        List<Integer> result = new ArrayList<>();

        for (Edge edge : edges) {
            if ((edge.getLeftNode() == node) && (!result.contains(edge.getRightNode()))) {
                result.add(edge.getRightNode());
            } else if ((edge.getRightNode() == node) && (!result.contains(edge.getLeftNode()))) {
                result.add(edge.getLeftNode());
            }
        }

        return result;
    }

    public static List<Integer> getIncomingNeighboursTo(int node, Collection<Edge> edges) {
        final List<Integer> result = new ArrayList<>();

        edges
            .stream()
            .filter(edge -> (edge.getLeftNode() == node) && (!result.contains(edge.getRightNode())))
            .map(Edge::getRightNode)
            .forEachOrdered(result::add);

        return result;
    }

    public static List<Integer> getOutgoingNeighboursTo(int node, Collection<Edge> edges) {
        final List<Integer> result = new ArrayList<>();

        edges
            .stream()
            .filter(edge -> (edge.getRightNode() == node) && (!result.contains(edge.getLeftNode())))
            .map(Edge::getLeftNode)
            .forEachOrdered(result::add);

        return result;
    }

    public static int numberOfConnectingEdges(List<Integer> list,
                                              Collection<Edge> edges,
                                              boolean allowDoubleEdges) {
        return numberOfConnectingEdges(list, list, edges, allowDoubleEdges);
    }

    public static int numberOfConnectingEdges(Collection<Integer> firstList,
                                              Collection<Integer> secondList,
                                              Collection<Edge> edges,
                                              boolean allowDoubleEdges) {
        final List<Edge> result = new ArrayList<>();

        for (Integer firstListElement : firstList) {
            for (Integer secondListElement : secondList) {
                edges
                    .stream()
                    .filter(edge -> edge.hasNodes(firstListElement, secondListElement))
                    .filter(edge -> (!result.contains(edge)) &&
                            (allowDoubleEdges || (!result.contains(edge.reverse()))))
                    .forEachOrdered(result::add);
            }
        }

        return result.size();
    }
}
