package cycleFinder;

import sysmodel.SparceMatrix;

import java.util.List;
import java.util.Set;


public interface CycleFinder {
    List<? extends Set<Integer>> find(SparceMatrix<Integer> m);
}
