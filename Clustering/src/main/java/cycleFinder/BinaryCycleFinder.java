package cycleFinder;

import algorithm.TarjanAlgorithm;
import sysmodel.SparceMatrix;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class BinaryCycleFinder implements CycleFinder {
    private Set<Integer> addedSet;

    public BinaryCycleFinder() {
        addedSet = new HashSet<>();
    }

    public List<Set<Integer>> find(SparceMatrix<Integer> m) {
        TarjanAlgorithm t = new TarjanAlgorithm(m);
        List<? extends Set<Integer>> unsorted = t.compute();
        int size = unsorted.size();
        boolean[] added = new boolean[size];
        List<Set<Integer>> first = new LinkedList<>();
        List<Set<Integer>> last = new LinkedList<>();
        boolean canContinue = true;
        while (canContinue) {
            canContinue = false;
            int i = 0;
            for (Set<Integer> s : unsorted) {
                if (!added[i]) {
                    if (row(s, first, m) || columns(s, last, m)) {
                        added[i] = true;
                        canContinue = true;
                    }
                }
                i++;
            }
        }
        first.addAll(last);
        addedSet.clear();
        return first;
    }

    private boolean columns(Set<Integer> toAdd, List<Set<Integer>> last, SparceMatrix<Integer> m) {
        for (Integer i : toAdd) {
            Set<Integer> set = m.getConnectedToColumns(i);
            if (set == null) {
                last.add(0, toAdd);
                addedSet.addAll(toAdd);
                return true;
            }
            for (Integer j : set)
                if (!toAdd.contains(j) && !addedSet.contains(j))
                    return false;
        }
        last.add(0, toAdd);
        addedSet.addAll(toAdd);
        return true;
    }

    private boolean row(Set<Integer> toAdd, List<Set<Integer>> first, SparceMatrix<Integer> m) {
        for (Integer i : toAdd) {
            Set<Integer> set = m.getConnectedToRow(i);
            if (set == null) {
                first.add(toAdd);
                addedSet.addAll(toAdd);
                return true;
            }
            for (Integer j : set)
                if (!toAdd.contains(j) && !addedSet.contains(j))
                    return false;
        }
        first.add(toAdd);
        addedSet.addAll(toAdd);
        return true;
    }
}