package cycleFinder;

import algorithm.TarjanAlgorithm;
import sysmodel.SparceMatrix;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class BinaryCycleFinder2 implements CycleFinder {
    private Set<Integer> addedSet;

    public BinaryCycleFinder2() {
        addedSet = new HashSet<>();
    }

    public List<Set<Integer>> find(SparceMatrix<Integer> m) {
        TarjanAlgorithm t = new TarjanAlgorithm(m);
        List<? extends Set<Integer>> unsorted = t.compute();
        int size = unsorted.size();
        boolean[] added = new boolean[size];
        List<Set<Integer>> first = new LinkedList<>();
        List<Set<Integer>> last = new LinkedList<>();
        boolean canContinue = true;
        List<Integer> aux = new LinkedList<>();
        while (canContinue) {
            canContinue = false;
            aux.clear();
            int i = 0;
            for (Set<Integer> s : unsorted) {
                if (!added[i]) {
                    if (row(s, first, m) || columns(s, last, m)) {
                        aux.addAll(s);
                        added[i] = true;
                    }
                    canContinue = true;
                }
                i++;
            }
            addedSet.addAll(aux);
        }
        first.addAll(last);
        addedSet.clear();
        return first;
    }

    private boolean columns(Set<Integer> toAdd, List<Set<Integer>> last, SparceMatrix<Integer> m) {
        for (Integer i : toAdd) {
            Set<Integer> set = m.getConnectedToColumns(i);
            if (set == null) {
                last.add(0, toAdd);
                return true;
            }
            for (Integer j : set)
                if (!toAdd.contains(j) && !addedSet.contains(j))
                    return false;
        }
        last.add(0, toAdd);
        return true;
    }

    private boolean row(Set<Integer> toAdd, List<Set<Integer>> first, SparceMatrix<Integer> m) {
        for (Integer i : toAdd) {
            Set<Integer> set = m.getConnectedToRow(i);
            if (set == null) {
                first.add(toAdd);
                return true;
            }
            for (Integer j : set)
                if (!toAdd.contains(j) && !addedSet.contains(j))
                    return false;
        }
        first.add(toAdd);
        return true;
    }
}