package gui;

import sysmodel.DSM;
import util.ClusterStatEngine;
import util.StatEngine;

import java.util.List;
import java.util.Set;

public class ClusterBuilder implements StatEngineBuilder {

    @Override
    public StatEngine build(List<? extends Set<Integer>> list,
                            DSM d) {
        return new ClusterStatEngine(list, d);
    }

}
