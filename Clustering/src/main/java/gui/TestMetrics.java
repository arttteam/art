package gui;

import metrics.mojo.MojoAlgorithm;
import metrics.precision_recall.Precision_RecallAlgorithm;
import metrics.util.MetricPartition;
import util.OpenClusters;
import util.Partition;
import util.XMLParser;

import java.io.File;

public class TestMetrics {


    /* the Main program that does everything */
    public static void main(final String[] args) throws Exception {

		/*
         * Select a sourceFileName = the name of the cluster result file to be analysed - it
		 * is searched in the outputs folder.
		 */

        //String sourceFileName="jhotdraw.jar-MMST_CF_0.3_AP_E___RA_AD_LD_L.xml";
        String sourceFileName = "minimalART.jar-MMST_CF_0.6_AP_E___RA_AD_LD_C.xml";
		
		
		
		/*
		 * Select a referenceFileName = the name of the reference file (the "ground truth architecture")
	     * is searched in the outputs folder.
		 */
        //String referenceFileName="ref_jhotdraw.xml";
        String referenceFileName = "ref_MinimalART.xml";


        File file = new File("outputs//" + sourceFileName);
        Partition p = OpenClusters.open(file);
        MetricPartition mp = new MetricPartition(p);
        mp.algorithmPerformed = XMLParser.getHeader().algorithmPerformed;

        File ref = new File("inputs//" + referenceFileName);
        Partition pref = OpenClusters.open(ref);
        MetricPartition mpref = new MetricPartition(pref);

        MojoAlgorithm mojo = new MojoAlgorithm(mpref, mp);
        int mojoAB = mojo.calculate();
        int n = mojo.getPartitionBSize();
        float f = (float) mojoAB / n;
        f = ((float) 1 - f) * 100;


        Precision_RecallAlgorithm pralg = new Precision_RecallAlgorithm(mpref, mp);
        float precision = pralg.calculatePrecision(mpref, mp);
        float recall = pralg.calculateRecall(mp, mpref);
        float rez = (precision + recall) / 2;

        System.out.println(mp.algorithmPerformed + " on " + sourceFileName + ":");
        System.out.println("     MoJo=" + f + ", prec=" + precision + ", recall=" + recall);
    }

}