package gui;

import sysmodel.DSM;
import util.StatEngine;

import java.util.List;
import java.util.Set;

public interface StatEngineBuilder {
    StatEngine build(List<? extends Set<Integer>> list, DSM matrix);
}
