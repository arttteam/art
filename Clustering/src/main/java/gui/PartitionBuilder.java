package gui;

import sysmodel.DSM;
import util.PartitionStatEngine;
import util.StatEngine;

import java.util.List;
import java.util.Set;

public class PartitionBuilder implements StatEngineBuilder {

    @Override
    public StatEngine build(List<? extends Set<Integer>> list,
                            DSM d) {
        return new PartitionStatEngine(list, d.getDependencyMatrix());
    }

}
