package gui;

import classloader.*;
import clusterFinder.ClusterFinder;
import clusterFinder.graphBased.MMSTClusterFinder;
import clusterFinder.postProcess.AdoptionPostProcessor;
import clusterFinder.postProcess.CompositePostProcessor;
import clusterFinder.postProcess.PostProcessor;
import clusterFinder.preProcess.AdjustorPreprocessor;
import clusterFinder.preProcess.CompositePreProcessor;
import clusterFinder.preProcess.PreProcessor;
import clusterFinder.preProcess.RemoveLibrariesPreProcessor;
import dependencyfinder.classdependencymodel.DependencyModel;
import dependencyfinder.util.*;
import sysmodel.DSM;
import sysmodel.SystemModel;
import sysmodel.edge.MidValueConstructor;
import sysmodel.edge.adjustor.Adjuster;
import sysmodel.edge.adjustor.GeometricAdjuster;
import sysmodel.edge.adjustor.LevelDistanceAdjusterFactory;
import util.SaveClusters;
import util.SaveDSM;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainTestClustering {

    /*
     * the name of the file(s) to be analysed and used to build the system
     * model; it is expected to be a *.jar, *.class or folder
     */
    private static String sourceFileName;

    /* the reverse engineered system model representation */
    private static SystemModel theSystemModel;

    /* the DSM built from the system model */
    private static DSM theDSM;

    /*
     * several different clusterfinders (using various pre- and post-processors
     * and clustering algorithms
     */
    private static List<ClusterFinder> clusterFinders = new ArrayList<>();

    /* the Main program that does everything */
    public static void main(final String[] args) throws Exception {

		/*
         * Select a sourceFileName = the name of the input to be analysed - it
		 * is searched in the inputs folder.
		 * 
		 * Following files are in the inputs folder - uncomment one:
		 */

        // sourceFileName=new String("ant.jar");
        // sourceFileName=new String("argouml.jar");
        // sourceFileName = new String("jhotdraw.jar");
        // sourceFileName=new String("jedit.jar");
        // sourceFileName = new String("wro4j-core-1.6.3.jar");
        sourceFileName = new String("minimalART.jar");

        buildSystemModel();

        computeDSM();

        configureClusterFinders();

        runClusterings();
    }

    private static void buildSystemModel() throws Exception {
        System.out.println("start reading input ...");

        File[] ss = {new File("inputs//" + sourceFileName)};

        long beginning = System.currentTimeMillis();
        InputHandler ih = new JarHandler(
                new DirHandler(new ClassFileHandler(null), new JarHandler(new ClassFileHandler(null))));

        theSystemModel = new SystemModel();

        int i = 0;
        for (File f : ss) {
            List<DependencyModel> deps;
            try {
                deps = ih.handle(f);
                i = deps.size();
                for (DependencyModel dm : deps)
                    theSystemModel.addElement(dm);
            } catch (UnsupportedInputException | NoSuchFileException e) {
            }

        }
        if (i == 0)
            throw new Exception();

        System.out.println(
                "input read and system model built in " + ((double) System.currentTimeMillis() - beginning) / 1000);

    }

    private static void buildConfig(String string) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(string));
            Initializer i = new SingleValueInitializer(
                    new SingleValueInitializer(
                            new SingleValueInitializer(
                                    new TwoValueInitializer(
                                            new TwoValueInitializer(
                                                    new TwoValueInitializer(
                                                            new TwoValueInitializer(
                                                                    new SingleValueInitializer(
                                                                            new SingleValueInitializer(
                                                                                    new SingleValueInitializer(
                                                                                            new SingleValueInitializer(
                                                                                                    null,
                                                                                                    "instantiation",
                                                                                                    new InstantiatedInvoker()),
                                                                                            "member_access",
                                                                                            new MemberAccessInvoker()),
                                                                                    "type_binding",
                                                                                    new TypeBindingInvoker()),
                                                                            "return", new ReturnInvoker()),
                                                                    "static_invocation", new StaticInvoker()),
                                                            "parameter", new ParamInvoker()),
                                                    "local_variable", new LocalVarInvoker()),
                                            "member", new MemberInvoker()),
                                    "implementation", new ImplementationInvoker()),
                            "inheritance", new InheritanceInvoker()),
                    "cast", new CastInvoker());
            String line = null;
            while ((line = in.readLine()) != null) {
                String[] parts = line.split("  *= *");
                if (parts.length == 2)
                    i.handle(parts[0], parts[1]);
            }
            in.close();
        } catch (IOException e) {
        }
    }

    private static void computeDSM() {

        // buildConfig("cfg.txt"); // this is optional, use it only if you want
        // to change some of the default values for dependency strengths

        long beginning = System.currentTimeMillis();

        theDSM = theSystemModel.computeDSM();

        System.out.println("DSM created in " + ((double) System.currentTimeMillis() - beginning) / 1000);

    }

    private static void configureClusterFinders() {

		/*
		 * Every clusterfinder may use a list of preprocessors.
		 * 
		 * Currently we have following preprocessor types:
		 * 
		 * - RemoveLibrariesPreProcessor : removes classes that are libraries
		 * (are used by more than a libraryThreshold percentage of all the
		 * classes
		 * 
		 * - AdjustorPreprocesor : adjusts edge weights according to the layer
		 * distance between the classes. Adjustement can be made proportional
		 * with the distance by a linear or exponential function
		 * 
		 * - GeometricAdjuster : adjusts edge weights according to a kind of
		 * indirect coupling
		 */
        List<PreProcessor> preProcessList = new ArrayList<>();

		/*
		 * Every clusterfinder may use a list of postprocessors.
		 * 
		 * Currently we have following postprocessor types:
		 * 
		 * - AdoptionPostProcessor: assigns "orphaned" classes to nearest
		 * cluster. It may use a layer distance adjustor.
		 */
        List<PostProcessor> postProcessList = new ArrayList<>();

		/*
		 * *********** The PreProcessors and their configuration ***************
		 */

        float libraryTreshold = -1; // parameter for the
        // RemoveLibrariesPreProcessor
        if (libraryTreshold > 0) {
            preProcessList.add(new RemoveLibrariesPreProcessor(libraryTreshold));
        }

        LevelDistanceAdjusterFactory.reset(); // for layer distance adjustment
        String levelDistAdj = "exponential"; // can be linear, exponential or
        // none
        preProcessList.add(new AdjustorPreprocessor(LevelDistanceAdjusterFactory.getByName(levelDistAdj)));

        int geometricConfidenceLevel = -1; // parameter for the "geometric
        // adjustment"
        if (geometricConfidenceLevel > 0) {
            preProcessList.add(new AdjustorPreprocessor(new GeometricAdjuster(geometricConfidenceLevel)));
        }

        // a composite preprocessor is created from the list. This composite is
        // used by the clusterfinder
        CompositePreProcessor cpre = new CompositePreProcessor(preProcessList);

		/*
		 * ******** The PostProcessors and their configuration ***************
		 */

        String adoptionAdjustment = "none";
        Adjuster adoptionAdjuster = LevelDistanceAdjusterFactory.getByName(adoptionAdjustment);

        PostProcessor postProcessor = new AdoptionPostProcessor(adoptionAdjuster);

        postProcessList.add(postProcessor);

        CompositePostProcessor cpost = new CompositePostProcessor(postProcessList);

        float closenessFactor;

        closenessFactor = (float) 0.3;
        ClusterFinder cf1 = new MMSTClusterFinder(closenessFactor, new MidValueConstructor(), cpre, cpost);
        clusterFinders.add(cf1);

        closenessFactor = (float) 0.4;
        ClusterFinder cf2 = new MMSTClusterFinder(closenessFactor, new MidValueConstructor(), cpre, cpost);
        clusterFinders.add(cf2);

        closenessFactor = (float) 0.5;
        ClusterFinder cf3 = new MMSTClusterFinder(closenessFactor, new MidValueConstructor(), cpre, cpost);
        clusterFinders.add(cf3);

        closenessFactor = (float) 0.6;
        ClusterFinder cf4 = new MMSTClusterFinder(closenessFactor, new MidValueConstructor(), cpre, cpost);
        clusterFinders.add(cf4);

        closenessFactor = (float) 0.7;
        ClusterFinder cf5 = new MMSTClusterFinder(closenessFactor, new MidValueConstructor(), cpre, cpost);
        clusterFinders.add(cf5);

    }

    private static void runClusterings() {
        System.out.println(clusterFinders.size() + " algorithms to run : \n");

        ClusterFinder clusterFinder;
        String clusterFinderName;

        for (int i = 0; i < clusterFinders.size(); i++) {
            long beginning = System.currentTimeMillis();

            clusterFinder = clusterFinders.get(i);
            clusterFinderName = String.valueOf(i + 1).concat(" : ".concat(clusterFinder.toString()));

            List<? extends Set<Integer>> clusters = clusterFinder.findClusters(theDSM);

            clusterFinderName = String.valueOf(i + 1).concat(" : ".concat(clusterFinder.toString()));

            System.out.println(clusterFinderName + " successfully completed in : "
                    + ((double) System.currentTimeMillis() - beginning) / 1000);

            StatEngineBuilder seb = new ClusterBuilder();
            String statistics = seb.build(clusters, theDSM).start();

            System.out.println("statistics: ");
            System.out.println(statistics);

            saveClustering(clusterFinder, clusters);

        }

        System.out.println("\nFINISHED");
    }

    private static void saveClustering(ClusterFinder clusterFinder, List<? extends Set<Integer>> clusters) {
        try {

            SaveClusters.cycles = clusters;

            new SaveDSM(new File("outputs" + "/" + sourceFileName + "-" + clusterFinder.toString() + ".xml"), theDSM,
                    clusterFinder, clusterFinder.toString());

        } catch (Exception e) {
            System.out.println("failed to save  " + e.toString());
        }
    }

}
