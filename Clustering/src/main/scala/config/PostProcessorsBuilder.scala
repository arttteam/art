package config

import clusterFinder.postProcess.{AdoptionPostProcessor, CompositePostProcessor, PostProcessor}
import sysmodel.edge.adjustor._

import scala.collection.JavaConversions._
import scala.collection.mutable

/**
  * Created by ThatKidFlo on 11/09/16.
  */
class PostProcessorsBuilder private(filename: String) extends ProcessorsConfigBuilder[CompositePostProcessor](filename) {

  override def build: CompositePostProcessor = {
    val postProcessors = tokenizeInputFile.map {
      case (_, name) => LevelDistanceAdjusterFactory.getByName(name)
      case _ => new NoneLevelDistanceAdjuster()
    }.foldRight(mutable.ListBuffer[PostProcessor]()) { (adjuster, acc) => acc += new AdoptionPostProcessor(adjuster) }
      .toList
    postProcessors.foreach(println)
    new CompositePostProcessor(postProcessors)
  }
}

object PostProcessorsBuilder {
  def fromSource(filename: String): PostProcessorsBuilder = new PostProcessorsBuilder(filename)

  def main(args: Array[String]) {
    val postProcessors = PostProcessorsBuilder.fromSource("Clustering/src/main/resources/post.conf").build
    val preProcessors = PreProcessorsBuilder.fromSource("Clustering/src/main/resources/pre.conf").build
    val hc = new HillClimbingBuilder("Clustering/src/main/resources/HC.csv", preProcessors, postProcessors).build
    hc.foreach(println)
  }
}
