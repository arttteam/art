package config

import clusterFinder.hillclimbing.HillClimbingClusterFinder
import clusterFinder.postProcess.CompositePostProcessor
import clusterFinder.preProcess.CompositePreProcessor

import scala.util.Try

/**
  * Created by ThatKidFlo on 24/09/16.
  */
class HillClimbingBuilder(filename: String,
                          val preprocessors: CompositePreProcessor,
                          val postProcessors: CompositePostProcessor) extends ClusteringAlgorithmBuilder[HillClimbingClusterFinder, List[(Int, Double, Double, Double)]](filename) {

  override def build: List[HillClimbingClusterFinder] = {
    tokenizeInputFile.map { case (sp, np, cf, st) =>
      new HillClimbingClusterFinder(preprocessors, postProcessors, sp, np, cf, st)
    }
  }

  override def tokenizeInputFile: List[(Int, Double, Double, Double)] = {
    genericFileTokenizer(_.startsWith("#"))(",") {
      case Array(startPart, neighbourPercent, coolFactor, startTemp, _*) =>
        Try {
          (startPart.toInt, neighbourPercent.toDouble, coolFactor.toDouble, startTemp.toDouble)
        }
    } {_.filter(_.isSuccess).map(_.get).toList}
  }
}
