package config

import clusterFinder.ClusterFinder

/**
  * Created by ThatKidFlo on 24/09/16.
  */
abstract class ClusteringAlgorithmBuilder[CF <: ClusterFinder, T](filename: String) extends GenericConfigurationBuilder[List[CF], T](filename) {
  override def build: List[CF]
}
