package config

import clusterFinder.graphBased.MMSTClusterFinder

/**
  * Created by ThatKidFlo on 24/09/16.
  */
class MMSTBuilder(filename: String) extends ClusteringAlgorithmBuilder[MMSTClusterFinder, AnyVal](filename) {
  override def build: List[MMSTClusterFinder] = ???

  override def tokenizeInputFile: AnyVal = ???
}
