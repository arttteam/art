package config

/**
  * Created by ThatKidFlo on 24/09/16.
  */
abstract class GenericConfigurationBuilder[B, T](val filename: String) {
  def build: B

  def genericFileTokenizer[S, T](commentsPredicate: String => Boolean)
                                (splittingToken: String)
                                (tokenizerFunction: Array[String] => T)
                                (structuringFunction: Iterator[T] => S): S = {
    structuringFunction(
      scala.io.Source.fromFile(filename)
        .getLines
        .filterNot(commentsPredicate)
        .map(_.split(splittingToken))
        .map(tokenizerFunction))
  }

  def tokenizeInputFile: T
}
