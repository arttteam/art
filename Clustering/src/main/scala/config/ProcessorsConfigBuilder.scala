package config

/**
  * Created by ThatKidFlo on 24/09/16.
  */
abstract class ProcessorsConfigBuilder[B](filename: String) extends GenericConfigurationBuilder[B, Map[String, String]](filename) {

  override def tokenizeInputFile: Map[String, String] = {
    genericFileTokenizer(_.startsWith("#"))("=")(lineArray => lineArray(0) -> lineArray(1).split(" ")(0))(_.toMap)
  }
}
