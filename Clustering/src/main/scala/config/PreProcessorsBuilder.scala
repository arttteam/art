package config

import clusterFinder.preProcess.{AdjustorPreprocessor, CompositePreProcessor, PreProcessor, RemoveLibrariesPreProcessor}
import sysmodel.edge.adjustor.{GeometricAdjuster, LevelDistanceAdjusterFactory}

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.util.Try

/**
  * Created by ThatKidFlo on 24/09/16.
  */
class PreProcessorsBuilder private(filename: String) extends ProcessorsConfigBuilder[CompositePreProcessor](filename) {
  override def build: CompositePreProcessor = {
    val preProcessors = tokenizeInputFile.filter {
      case ("level", name) => true
      case (_, maybeNumber) => Try(maybeNumber.toFloat).toOption.exists(_ > 0)
    }.map {
      case ("level", name) => new AdjustorPreprocessor(LevelDistanceAdjusterFactory.getByName(name))
      case ("library", threshold) => new RemoveLibrariesPreProcessor(threshold.toFloat)
      case ("geometric", threshold) => new AdjustorPreprocessor(new GeometricAdjuster(threshold.toFloat))
    }.foldRight(mutable.ListBuffer[PreProcessor]()) { (adjuster, acc) => acc += adjuster }
      .toList
    preProcessors.foreach(println)
    new CompositePreProcessor(preProcessors)
  }
}

object PreProcessorsBuilder {
  def fromSource(filename: String) = new PreProcessorsBuilder(filename)
}