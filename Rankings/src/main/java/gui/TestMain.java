package gui;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

import classloader.ClassFileHandler;
import classloader.DirHandler;
import classloader.InputHandler;
import classloader.JarHandler;
import classloader.NoSuchFileException;
import classloader.UnsupportedInputException;
import dependencyfinder.classdependencymodel.DependencyModel;
import dependencyfinder.classdependencymodel.DependencyStrengthFactory;
import dependencyfinder.util.*;

import inputOutput.OutputWriter;
import inputOutput.Reader;
import ranking.Ranking;
import ranking.strategy.FuzzyRulesRankingStrategy;
import ranking.strategy.SimpleRankingStrategy;
import sysmodel.DSM;
import sysmodel.SparceMatrix;
import sysmodel.SystemModel;
import metrics.Metric;
import metrics.Precision;
import metrics.Recall;
import ranking.propertyValueCalculators.DefaultClassRankingPropertiesAdder;
import ranking.propertyValueCalculators.hits.HitsCalculator;
import ranking.propertyValueCalculators.hits.HitsValue;
import ranking.propertyValueCalculators.pagerank.PageRankCalculator;
import ranking.ClassRankingProperties;

public class TestMain {

	public static void main(final String[] args) throws Exception {

        final String PATH_PREFIX = "Rankings//refSols//";
		//  the name of the jar to be analysed - it is in the inputs folder
		String name=new String("ant.jar");
		String refSolPath = "ant.jar.txt";
//		 String name = "ApacheJMeter_core.jar";
//         String refSolPath = "ApacheJMeter_core.jar.txt";
//       String name = new String("argouml.jar");
//       String refSolPath = "argouml.jar.txt";
//		 String name = new String("jedit.jar");
//       String refSolPath = "jedit.jar.txt";
//		 String name = new String("jhotdraw.jar");
//       String refSolPath = "jhotdraw.jar.txt";
//		 String name = new String("wro4j-core-1.6.3.jar");
//       String refSolPath = "wro4j-core-1.6.3.jar.txt";
		//	String name = new String("rt.jar");


		System.out.println("start...");

		// good config for depend strengths
		buildConfig("enase.txt");  // initializes dep strengths with values used in ENASE paper experiments

		// not so good config for depend strengths - these are equal with the default values
		//buildConfig("old-default.txt"); // not so good values for dep strengths


		File[] ss = { new File("Rankings//inputs//" + name) };

		long beginning = System.currentTimeMillis();


		InputHandler ih = new JarHandler(
				new DirHandler(new ClassFileHandler(null), new JarHandler(new ClassFileHandler(null))));

		SystemModel sm = new SystemModel();

		int i = 0;
		for (File f : ss) {
			List<DependencyModel> deps;
			try {
				deps = ih.handle(f);
				i = deps.size();
				for (DependencyModel dm : deps)
					sm.addElement(dm);
			} catch (UnsupportedInputException e) {
			} catch (NoSuchFileException e) {
			}

		}
		if (i == 0)
			throw new Exception();

		System.out.println("input read in " + ((double) System.currentTimeMillis() - beginning) / 1000);

		beginning = System.currentTimeMillis();



		DSM d = sm.computeDSM();

		d.collapseInnerClasses();

		//todo test here
		//load reference solution
//        String refSolPath = "E:\\Facultate\\Licenta\\Projects\\OnlyPageRank\\refSols\\ant.jar.txt";
		Reader r = new Reader();
		List<String> referenceSolution = r.readReferenceSolution(PATH_PREFIX + refSolPath);

        Ranking ranking = new Ranking(name, referenceSolution);
        boolean directed = true;

		HitsCalculator hitsCalculator = new HitsCalculator(d, 20);
		hitsCalculator.computeWeightedHubsAndAuthorities();
		ranking.setClassList(hitsCalculator.getResultAsRankingEntryList());

		PageRankCalculator prCalculator = new PageRankCalculator(d, !directed, 2, 50);
		prCalculator.computeWeightedPageRank();
		prCalculator.addResultToRanking(ranking.getClassList());

		DefaultClassRankingPropertiesAdder defaultCalculator = new DefaultClassRankingPropertiesAdder(d, directed, null);
		defaultCalculator.addResultToRanking(ranking.getClassList());

        int threshold = 30;
        ranking.setStrategy(new SimpleRankingStrategy(ClassRankingProperties.HITS_AUTHORITY));
        ranking.rank();
		OutputWriter.outputRanking(ranking);

		Metric precisionAUTH = new Precision(ranking, threshold);
		precisionAUTH.compute();
		Metric recallAUTH = new Recall(ranking, threshold);
		recallAUTH.compute();

		ranking.setStrategy(new SimpleRankingStrategy(ClassRankingProperties.HITS_HUB));
		ranking.rank();
		OutputWriter.outputRanking(ranking);

		Metric precisionHUB = new Precision(ranking, threshold);
		precisionHUB.compute();
		Metric recallHUB = new Recall(ranking, threshold);
		recallHUB.compute();

        ranking.setStrategy(new SimpleRankingStrategy(ClassRankingProperties.PAGERANK_UNDIRECTED));
        ranking.rank();
		OutputWriter.outputRanking(ranking);

		Metric precisionPR = new Precision(ranking, threshold);
		precisionPR.compute();
		Metric recallPR = new Recall(ranking, threshold);
		recallPR.compute();

        /** Test fuzzy rules **/
        List<ClassRankingProperties> variables = Arrays.asList(
                ClassRankingProperties.WEIGHT_IN, ClassRankingProperties.WEIGHT_OUT,
                ClassRankingProperties.SIZE, ClassRankingProperties.PAGERANK_UNDIRECTED);
                // ClassRankingProperties.HITS_AUTHORITY, ClassRankingProperties.HITS_HUB
        String path = "D:\\School\\Licenta\\Projects\\ARTTeam\\Rankings\\src\\main\\java\\jFuzzyLogicTest\\ranking4in.fcl";
        ranking.setStrategy(new FuzzyRulesRankingStrategy(variables, path));
        ranking.rank();
        System.out.println("Did fuzzy ranking");
		// print normalized values => get ranges for auth/hub
        OutputWriter.outputRanking(ranking, true);

        Metric precisionFuzzy = new Precision(ranking, threshold);
        precisionFuzzy.compute();
        Metric recallFuzzy = new Recall(ranking, threshold);
        recallFuzzy.compute();


        /** -------------------- METRICS -------------------- **/
//		OutputWriter.outputMetrics(name, precisionAUTH, recallAUTH, precisionHUB, recallHUB, precisionPR, recallPR);
        //todo alta varianta ar fi sa stochez valorile metricilor in ranking

        Map<String, List<Metric>> metricsMap = new HashMap<>();
        metricsMap.put(Precision.getNameStatic(), Arrays.asList(precisionAUTH, precisionHUB, precisionPR, precisionFuzzy));
        metricsMap.put(Recall.getNameStatic(), Arrays.asList(recallAUTH, recallHUB, recallPR, recallFuzzy));
		OutputWriter.outputMetrics2(name, metricsMap);

//		System.out.println("DSM created in " + ((double) System.currentTimeMillis() - beginning) / 1000);

		/*System.out.println("\n \nDo PageRank on Directed graph");

		doPR(name, d, true, -1);

		int F = 1;
		System.out.println("\n \nDo PageRank on Undirected graph created with fraction F=" + F);
		doPR(name, d, false, F);

		F = 2;
		System.out.println("\n \nDo PageRank on Undirected graph created with fraction F=" + F);
		doPR(name, d, false, F);

		F = 4;
		System.out.println("\n \nDo PageRank on Undirected graph created with fraction F=" + F);
		doPR(name, d, false, F);*/

		/*System.out.println("\n\nDoing HITS on Directed graph, ordering by authority");
		doHits(name, d, "authority");*/

		/*System.out.println("\n\n\nDoing HITS on Directed graph, ordering by hub");
		doHits(name, d, "hub");*/
	}

	/*private static void doPR(String name, DSM dsm, boolean directed, int F) throws FileNotFoundException {
		SparceMatrix<Integer> mat;

		if (!directed) {
			mat = dsm.getDependencyMatrix().createUndirected(F);
		} else {
			mat = dsm.getDependencyMatrix();
		}

		PageRankCalculator prc = new PageRankCalculator(mat, 50);

		Map<Integer, Double> m2 = prc.getResult();

		List<Entry<Integer, Double>> sortedEntries = new ArrayList<Entry<Integer, Double>>(m2.entrySet());

		Collections.sort(sortedEntries, new Comparator<Entry<Integer, Double>>() {
			@Override
			public int compare(Entry<Integer, Double> e1, Entry<Integer, Double> e2) {
				return e2.getValue().compareTo(e1.getValue());
			}
		});

		PrintWriter writer = new PrintWriter("outputs//" + name + F + ".txt");

		System.out.println("\n\n Top decreasing PageRank : \n\n");
		System.out.printf("%-60s \t%s\t%s\t%s\t%s\t%s\t%s\t%s \n", "Classname", "NrF", "NrM", "ToC", "ToR", "Win",
				"Wout", "PR");
		writer.printf("%-40s \t%s\t%s\t%s\t%s\t%s\t%s\t%s \n", "Classname", "NrF", "NrM", "ToC", "ToR", "Win", "Wout",
				"PR");
		// System.out.println("Classname "+"\t"+"NrFields \t NrMethods \t ToR \t
		// ToC \t Win \t Wout \t PageRank \n");
		for (int i = 0; i < mat.getNumberOfNodes() * 1; i++) {

			int classi = sortedEntries.get(i).getKey();
			int ToR = 0;
			if (mat.getConnectedToRow(classi) != null)
				ToR = mat.getConnectedToRow(classi).size();

			int ToC = 0;
			if (mat.getConnectedToColumns(classi) != null)
				ToC = mat.getConnectedToColumns(classi).size();

			System.out.printf("%-60s \t %5d \t %5d \t %5d \t %5d %5d \t %5d \t %f \n",
					dsm.elementAtFull(classi).getName(), // classname
					dsm.elementAtFull(classi).getNrFields(), // number of fields
					dsm.elementAtFull(classi).getNrMethods(), // number of
																// methods
					ToR, // number of classes on row
					ToC, // number of classes on column
					mat.inWeight(classi), // weight incoming deps
					mat.outWeight(classi), // weight outgoing deps
					sortedEntries.get(i).getValue()); // pagerank value

			writer.printf("%-40s \t %5d \t %5d \t %5d \t %5d %5d \t %5d \t %f \n", dsm.elementAtFull(classi).getName(), // classname
					dsm.elementAtFull(classi).getNrFields(), // number of fields
					dsm.elementAtFull(classi).getNrMethods(), // number of
																// methods
					ToR, // number of classes on row
					ToC, // number of classes on column
					mat.inWeight(classi), // weight incoming deps
					mat.outWeight(classi), // weight outgoing deps
					sortedEntries.get(i).getValue()); // pagerank value

		}
		writer.close();
	}*/

	// orderBy = "hub" or "authority"
	private static void doHits(String name, DSM dsm, String orderBy) throws FileNotFoundException{
		SparceMatrix<Integer> mat = dsm.getDependencyMatrix();

		HitsCalculator hc = new HitsCalculator(dsm, 15);

		Map<Integer, HitsValue> values = hc.computeWeightedHubsAndAuthorities();

		List<Entry<Integer, HitsValue>> sortedEntries = new ArrayList<Entry<Integer, HitsValue>>(values.entrySet());

		Comparator<Entry<Integer, HitsValue>> comparator;
		switch(orderBy){
			case "authority":
				comparator = new Comparator<Entry<Integer, HitsValue>>() {
					@Override
					public int compare(Entry<Integer, HitsValue> e1, Entry<Integer, HitsValue> e2) {
						return e2.getValue().getAuthority().compareTo(e1.getValue().getAuthority());
					}};
				break;
			case "hub":
				comparator = new Comparator<Entry<Integer, HitsValue>>() {
					@Override
					public int compare(Entry<Integer, HitsValue> e1, Entry<Integer, HitsValue> e2) {
						return e2.getValue().getHub().compareTo(e1.getValue().getHub());
					}};
				break;
			default:
				throw new RuntimeException("Cannot create comparator!");
		}

		Collections.sort(sortedEntries,comparator);

		PrintWriter writer = new PrintWriter("outputs//" + name + "WeightedHits_" + orderBy + "_" + hc.getIterations() + ".txt");

		System.out.println("\n\n Top decreasing " + orderBy + " : \n\n");

		System.out.printf("%-60s \t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s \n", "Classname", "NrF", "NrM", "ToC", "ToR", "Win",
				"Wout", "Authority", "Hub");
		writer.printf("%-40s \t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s \n", "Classname", "NrF", "NrM", "ToC", "ToR", "Win", "Wout",
				"Authority", "Hub");
		// System.out.println("Classname "+"\t"+"NrFields \t NrMethods \t ToR \t
		// ToC \t Win \t Wout \t PageRank \n");
		for (int i = 0; i < mat.getNumberOfNodes() * 1; i++) {

			int classi = sortedEntries.get(i).getKey();
			int ToR = 0;
			if (mat.getConnectedToRow(classi) != null)
				ToR = mat.getConnectedToRow(classi).size();

			int ToC = 0;
			if (mat.getConnectedToColumns(classi) != null)
				ToC = mat.getConnectedToColumns(classi).size();

			System.out.printf("%-60s \t %5d \t %5d \t %5d \t %5d %5d \t %5d \t %f \t %f \n",
					dsm.elementAtFull(classi).getName(), // classname
					dsm.elementAtFull(classi).getNrFields(), // number of fields
					dsm.elementAtFull(classi).getNrMethods(), // number of
					// methods
					ToR, // number of classes on row
					ToC, // number of classes on column
					mat.inWeight(classi), // weight incoming deps
					mat.outWeight(classi), // weight outgoing deps
					sortedEntries.get(i).getValue().getAuthority(), // authority value
					sortedEntries.get(i).getValue().getHub()); // hub value

			writer.printf("%-40s \t %5d \t %5d \t %5d \t %5d %5d \t %5d \t %f \t %f \n", dsm.elementAtFull(classi).getName(), // classname
					dsm.elementAtFull(classi).getNrFields(), // number of fields
					dsm.elementAtFull(classi).getNrMethods(), // number of
					// methods
					ToR, // number of classes on row
					ToC, // number of classes on column
					mat.inWeight(classi), // weight incoming deps
					mat.outWeight(classi), // weight outgoing deps
					sortedEntries.get(i).getValue().getAuthority(), // authority value
					sortedEntries.get(i).getValue().getHub()); // hub value

		}
		writer.close();
	}

	private static void buildConfig(String file) throws Exception {
		// initialize dependency strengths with values from config file
		BufferedReader in = new BufferedReader(new FileReader(file));
		Initializer i = new SingleValueInitializer(
				new SingleValueInitializer(
						new SingleValueInitializer(
								new TwoValueInitializer(
										new TwoValueInitializer(
												new TwoValueInitializer(
														new TwoValueInitializer(
																new SingleValueInitializer(
																		new SingleValueInitializer(
																				new SingleValueInitializer(
																						new SingleValueInitializer(
																								null,
																								"instantiation",
																								new InstantiatedInvoker()),
																						"member_access",
																						new MemberAccessInvoker()),
																				"type_binding",
																				new TypeBindingInvoker()),
																		"return", new ReturnInvoker()),
																"static_invocation", new StaticInvoker()),
														"parameter", new ParamInvoker()),
												"local_variable", new LocalVarInvoker()),
										"member", new MemberInvoker()),
								"implementation", new ImplementationInvoker()),
						"inheritance", new InheritanceInvoker()),
				"cast", new CastInvoker());
		String line = null;
		while ((line = in.readLine()) != null) {
			String[] parts = line.split("  *= *");
			if (parts.length == 2) {
				System.out.println("Input handle parts "+parts[0]+" "+parts[1]);
				i.handle(parts[0], parts[1]);
			}
		}
		in.close();

	}

}
