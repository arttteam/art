package ranking;

/**
 * Created by Mihaela Ilin on 8/27/2016.
 */
public enum ClassRankingProperties {

    PAGERANK_DIRECTED("pagerank_directed", "PR_dir"),
    PAGERANK_UNDIRECTED("pagerank_undirected", "PR_undir"),
    HITS_AUTHORITY("hits_auth", "Authority"),
    HITS_HUB("hits_hub", "Hub"),
    FIELD_NR("fields", "NrF"),
    METHOD_NR("methods", "NrM"),
    SIZE("size", "Size"),
    CONN_TO_ROW("ToR"),
    CONN_TO_COL("ToC"),
    WEIGHT_IN("Win"),
    WEIGHT_OUT("Wout"),
    FUZZY_DECISION("fuzzy_decision", "Fuzzy_decision");

    private String name;
    private String description;

    ClassRankingProperties(String name){
        this.name = name;
    }

    ClassRankingProperties(String name, String description){
        this.name = name;
        this.description = description;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description == null ? name : description;
    }

    public static ClassRankingProperties[] getRankingPropertiesOrderForPrinting(){
        return new ClassRankingProperties[]{ FIELD_NR, METHOD_NR, SIZE, CONN_TO_ROW, CONN_TO_COL, WEIGHT_IN, WEIGHT_OUT, PAGERANK_DIRECTED, PAGERANK_UNDIRECTED, HITS_AUTHORITY, HITS_HUB, FUZZY_DECISION};
    }
}
