package ranking;

import ranking.strategy.RankingStrategy;

import java.util.List;

/**
 * Created by Mihaela Ilin on 9/14/2016.
 */

//todo Cum se rezolva partea de fractie? (Se poate rula pagerank undirected cu fractii diferite)
//fractie: 1,2,4
public class Ranking {

    private List<RankingEntry> classList;
    private List<String> referenceSolution;

    // name of jar file that has been analyzed
    private String jarName;

    //refers to the entire graph as being directed or not;
    //not the graph used for Pagerank
    private boolean undirected;
    private Integer fraction;

    //the strategy by which the list has been sorted
    private RankingStrategy strategy;

    public Ranking(String jarName, List<String> referenceSolution) {
        this.jarName = jarName;
        this.referenceSolution = referenceSolution;
    }

    public void rank(){
        if(classList != null && classList.size() > 0){
            strategy.rank(classList);
        }
    }


    public String getStrategyDescription(){
        String description = strategy.getDescription();
        if(undirected == true){
            description += "_F" + fraction;
        }
        return description;
    }

    public List<RankingEntry> getClassList() {
        return classList;
    }

    public String getJarName() {
        return jarName;
    }

    public void setClassList(List<RankingEntry> classList) {
        this.classList = classList;
    }

    public void setStrategy(RankingStrategy strategy) {
        this.strategy = strategy;
    }

    public List<String> getReferenceSolution() {
        return referenceSolution;
    }

    public void setUndirected(boolean undirected) {
        this.undirected = undirected;
    }

    public void setFraction(Integer fraction) {
        this.fraction = fraction;
    }
}
