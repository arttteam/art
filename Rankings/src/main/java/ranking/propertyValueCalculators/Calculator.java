package ranking.propertyValueCalculators;

import ranking.RankingEntry;

import java.util.List;

/**
 * Created by Mihaela Ilin on 8/30/2016.
 */
public interface Calculator {

    void addResultToRanking(List<RankingEntry> ranking);

}
