package ranking.strategy;

import ranking.RankingEntry;

import java.util.List;

/**
 * Created by Mihaela Ilin on 9/14/2016.
 */

public abstract class RankingStrategy {

    private String description;

    /**
     * Sorts the given list in-place.
     */
    public abstract void rank(List<RankingEntry> ranking);

    protected void setDescription(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
