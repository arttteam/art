package metrics;

import ranking.Ranking;
import ranking.RankingEntry;

import java.util.List;

/**
 * Created by Mihaela Ilin on 8/17/2016.
 */
public abstract class Metric {

    private Ranking ranking;
    private int threshold;

    private Double value;
    private String strategyDescription;

    public Metric(Ranking ranking, int threshold) {
        this.ranking = ranking;
        this.threshold = threshold;
        strategyDescription = ranking.getStrategyDescription();
    }

    public void compute(){
        List<String> referenceSolution = ranking.getReferenceSolution();
        if(ranking != null && referenceSolution != null){
            List<RankingEntry> classList = ranking.getClassList();
            if(threshold <= 0){
                throw new RuntimeException("Threshold must be positive!");
            } else if(threshold >= classList.size()){
                threshold = classList.size() - 1;
            }

            int foundClasses = 0;

            for(int i=0; i<=threshold; i++){
                if(referenceSolution.contains(classList.get(i).getClassName())){
                    foundClasses++;
                }
            }

            value = doCalculations(foundClasses);
        }
    }

//    public abstract Metrics getEnumValue();

    protected abstract Double doCalculations(int foundClasses);

    public abstract String getName();

    protected int getThreshold() {
        return threshold;
    }

    public Double getValue() {
        return value;
    }

    public Ranking getRanking() {
        return ranking;
    }

    protected int getReferenceSolutionSize(){
        if(ranking.getReferenceSolution() != null){
            return ranking.getReferenceSolution().size();
        }
        return 0;
    }

    public String getStrategyDescription() {
        return strategyDescription;
    }
}
