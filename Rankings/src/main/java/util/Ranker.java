package util;

import ranking.ClassRankingProperties;
import ranking.RankingEntry;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Mihaela Ilin on 8/17/2016.
 */
public class Ranker {

    /*
     * @param classOrder : contains the indeces of the classes in the order given by the ranking.
     * @param dsm : dependency structure matrix.
     * @return list containing fully qualified class names
     */
    /*public static List<String> getRankingAsClassList(List<RankingEntry> classOrder, DSM dsm){
        List<String> classList = new ArrayList<>();
        for(int i=0; i<classOrder.size(); i++){
            classList.add(dsm.elementAtFull(classOrder.get(i).getClassNumber()).getName());
        }
        return classList;
    }*/

    /**
     * Sorts the given list according to the specified algorithm values.
     */
    // algoritmul trebuie wrapped intr-o strategy
    // trebuie sa se poata rankui dupa mai multe valori
    public static void rankByAlgorithmResultValue(List<RankingEntry> ranking, final ClassRankingProperties algorithm){
        if(ranking != null && ranking.size() > 0){
            // If the first entry has a value set for this algorithm, than all entries must have one.
            if(ranking.get(0).getClassRankingPropertyValue(algorithm) != null){
                Comparator<RankingEntry> comparator = new Comparator<RankingEntry>(){
                    @Override
                    public int compare(RankingEntry o1, RankingEntry o2) {
                        return o2.getClassRankingPropertyValue(algorithm).compareTo(o1.getClassRankingPropertyValue(algorithm));
                    }
                };
                Collections.sort(ranking, comparator);
            }
        }
    }

    /*public static void rank(List<RankingEntry> ranking, RankingStrategy rankingStrategy){
        if(ranking != null && ranking.size() > 0){
            rankingStrategy.rank(ranking);
        }
    }*/
}
