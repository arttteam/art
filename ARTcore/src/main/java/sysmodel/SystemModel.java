package sysmodel;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import dependencyfinder.classdependencymodel.DependencyModel;

public class SystemModel {
	private Set<String> elements;
	private Map<String, ClassAttributesEntry> infoMap;
	private Map<String, Map<String, Integer>> depMap;
	private String[] indexMap = null;

	public SystemModel() {
		elements = new TreeSet<String>();
		infoMap = new HashMap<String, ClassAttributesEntry>();
		depMap = new HashMap<String, Map<String, Integer>>();

	}

	public void addElement(DependencyModel model) {
		String name = model.getClassName();
		elements.add(name);
		infoMap.put(name, new ClassAttributesEntry(name, model.getNrMethods(), model.getNrFields()));

		Map<String, Integer> classDeps = model.computeModel();
		depMap.put(name, classDeps);
		this.indexMap = null;

	}

	public DSM computeDSM() {
		buildIndexMap();
		int added = elements.size();
		SparceMatrix<Integer> toRet = new SparceMatrix<Integer>(added, added);
		int i = 0;
		Iterator<String> it = elements.iterator();
		while (it.hasNext()) {
			String currentElement = it.next();
			Map<String, Integer> m = this.depMap.get(currentElement);
			Set<Entry<String, Integer>> entries = m.entrySet();
			Iterator<Entry<String, Integer>> itt = entries.iterator();
			StringBinSearch alg = new StringBinSearch();
			while (itt.hasNext()) {
				Entry<String, Integer> currentDep = itt.next();
				int j = alg.indexOf(indexMap, currentDep.getKey());
				if (j >= 0) {
					Integer dep = toRet.getElement(j, i);
					if (dep != null)
						dep += currentDep.getValue();
					else
						dep = currentDep.getValue();
					toRet.putElement(j, i, dep);
				}
			}
			i++;
		}
		return new DSM(toRet, indexMap, infoMap);
	}

	private void buildIndexMap() {
		int len = elements.size();
		this.indexMap = new String[len];
		elements.toArray(this.indexMap);
		System.out.println("elements " + elements);
		
	}

	public String elementAt(int i) {
		if (this.indexMap == null)
			throw new IllegalStateException();
		return this.indexMap[i];
	}
	
	public String elementAtFull(int i) {
		if (this.indexMap == null)
			throw new IllegalStateException();
		return this.indexMap[i] + " " + this.infoMap.get(indexMap[i]);
	}
	}
