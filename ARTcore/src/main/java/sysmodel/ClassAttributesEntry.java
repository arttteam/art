package sysmodel;

public class ClassAttributesEntry {
	private int nrMethods;
	private int nrFields;
	private String name;

	public ClassAttributesEntry(String n, int nrM, int nrF) {
		name=n;
		nrMethods = nrM;
		nrFields = nrF;
	}

	public String getName(){
		return name;
	}
	
	public int getNrMethods() {
		return nrMethods;
	}

	public int getNrFields() {
		return nrFields;
	}
}
