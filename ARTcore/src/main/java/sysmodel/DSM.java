package sysmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DSM {

	private final SparceMatrix<Integer> dependencyMatrix;
	private final String[] indexMap;
	private final Map<String, ClassAttributesEntry> infoIndexMap;

	public DSM(SparceMatrix<Integer> dependencyMatrix, String[] indexMap, Map<String, ClassAttributesEntry> infoMap) {
		this.dependencyMatrix = dependencyMatrix;
		this.indexMap = indexMap;
		this.infoIndexMap = infoMap;

		System.out.println("Dependency matrix is:");
		System.out.println(dependencyMatrix.toString());
		System.out.println("No of classes is " + dependencyMatrix.getNumberOfNodes());

	}

	public void collapseInnerClasses() {
		for (int i = 0; i < dependencyMatrix.getNumberOfNodes() * 1; i++) {
			int dolarindex = elementAt(i).indexOf('$');
			if (dolarindex >= 0) {
				// System.out.print("$$$$$$$" +elementAt(i));
				String containerName = elementAt(i).substring(0, dolarindex);
				for (int j = 0; j < dependencyMatrix.getNumberOfNodes(); j++) {
					if (elementAt(j).equals(containerName)) {
						System.out.println("found container");
						/*
						 * if (containerName.equals(
						 * "org.apache.tools.ant.helper.ProjectHelperImpl")
						 * ||containerName.equals(
						 * "org.apache.tools.ant.helper.ProjectHelper2"))
						 */dependencyMatrix.mergeIntoContainer(i, j);
					}
				}
			}
		}

		System.out.println("Dependency matrix is:");
		System.out.println(dependencyMatrix.toString());

	}

	public String toString() {
		return dependencyMatrix.toString();
	}

	public String elementAt(int i) {
		return indexMap[i]; // + infoIndexMap.get(indexMap[i]);
	}

	public ClassAttributesEntry elementAtFull(int i) {
		return infoIndexMap.get(indexMap[i]);
		}

	public SparceMatrix<Integer> getDependencyMatrix() {
		return this.dependencyMatrix;
	}

	public PackageNames getPackageNames() {
		List<String> packageNames = new ArrayList<>();
		String packageName;

		for (int i = 0; i < indexMap.length; i++) {
			packageName = indexMap[i].substring(0, indexMap[i].lastIndexOf('.'));
			if (!packageNames.contains(packageName)) {
				packageNames.add(packageName);
			}
		}

		return new PackageNames(packageNames);
	}
}
