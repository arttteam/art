package dependencyfinder.classdependencymodel;

import java.util.Map;

public interface DependencyModel {
	String[] getClassFullName();
	String getClassName();
        int getNrFields(); //added for size
        int getNrMethods(); //added for size 
	void setName(String name);
	Map<String,Integer> computeModel();
}
